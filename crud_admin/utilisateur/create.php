<?php
include '../admin/fonction_include.php';
$pdo = pdo_connect_mysql();
$msg = '';
$valid = true;
// Check if POST data is not empty
if (!empty($_POST)) {
    $nomError = ''; $prenomError=''; $loginError=''; $passwordError =''; $emailError ='';  // on recupère nos valeurs 

    // Post data not empty insert a new record
    // Set-up the variables that are going to be inserted, we must check if the POST variables exist if not we can default them to blank
    $id = isset($_POST['id']) && !empty($_POST['id']) && $_POST['id'] != 'auto' ? $_POST['id'] : NULL;
    // Check if POST variable "name" exists, if not default the value to blank, basically the same for all variables
//    $nom = isset($_POST['nom']) ? $_POST['nom'] : '';
//    $prenom = isset($_POST['prenom']) ? $_POST['prenom'] : '';
//    $login = isset($_POST['login']) ? $_POST['login'] : '';
//    $password = md5(isset($_POST['password']) ? $_POST['password'] : '');
//    $email = isset($_POST['email']) ? $_POST['email'] : '';
    $nom = htmlentities(trim($_POST['nom'])); 
    $prenom=htmlentities(trim($_POST['prenom'])); 
    $login = htmlentities(trim($_POST['login'])); 
    $password= md5(htmlentities(trim($_POST['password']))); 
    $email = htmlentities(trim($_POST['email']));
    $created = isset($_POST['created']) ? $_POST['created'] : gmdate('Y-m-d H:i:s');
    $etat = htmlentities(trim($_POST['etat']));
    //TEST DE VALEURS NULL
    if(empty($nom)) { 
    $nomError = ' est requis!'; 
     $valid = false; }
     else if (!preg_match("/^[a-zA-Z ]*$/",$nom)) {
         $nomError = "Seules les lettres et les espaces blancs sont autorisés"; 
         
     } 
     if(empty($prenom)){
         $prenomError =' est requis!'; 
         $valid= false; 
         
     }
         else if (!preg_match("/^[a-zA-Z ]*$/",$prenom)) {
             $prenomError = "Seules les lettres et les espaces blancs sont autorisés"; 
             
         } 
         
    if(empty($login)){
         $loginError =' est requis!'; 
         $valid= false; 
         
     }
         else if (!preg_match("/^[a-zA-Z ]*$/",$login)) {
             $loginError = "Seules les lettres et les espaces blancs sont autorisés"; 
             
         }
          
    if(empty($password)){
         $passwordError =' est requis!'; 
         $valid= false; 
         
     }
         else if (!preg_match("/^[a-zA-Z ]*$/",$password)) {
             $passwordError = "Seules les lettres et les espaces blancs sont autorisés"; 
             
         }
    if (empty($email)) { 
                 $emailError = ' est requis';
             $valid = false;
        } else if ( !filter_var($email,FILTER_VALIDATE_EMAIL) ) {
            $emailError = 'Veuillez saisir un email valide'; 
            $valid = false; } 

    
    
    // Insert new record into the contacts table
            
              $sql = "SELECT * FROM utilisateur WHERE US_LOGIN= ? AND US_PASSWORD=? ";
        $q = $pdo->prepare($sql);
        $q->execute(array( $_SESSION['login'] ,  $_SESSION['password'] ));
        $data = $q->fetch(PDO::FETCH_ASSOC);
        
            if($valid && !data){
                  $stmt = $pdo->prepare('INSERT INTO utilisateur(US_NOM,US_PRENOM,US_LOGIN,US_PASSWORD,US_EMAIL,US_DATECREATE,US_ETAT) VALUES (?, ?, ?, ?,?,?,?)');
    $stmt->execute([$nom,$prenom,$login,$password,$email,$created,$etat]);
    // Output message
    $msg = 'Utilisateur Enregistré avec succès'; 
            } else {
                if($data){
                   $msg = 'login existe déjà';   
                } else {
                     $msg = 'Veuillez saisir tous les champs'; 
                }
                
            }
 
}
?><?=template_header('Create')?>

<div class="content update">
     <?php if ($msg): ?>
    <p class="help-inline"><?=$msg?></p>
    <?php endif; ?>
	<h2>Création utilisateur</h2>
    <form  action="create.php" method="post">
        <!--<label for="id">ID</label>-->
         <div class="ligne">
            <p class="premier">
        <label for="nom">Nom <?php if (!empty($nomError)): ?>
                              <span class="help-inline"><?php echo ':'.$nomError;?></span>
                            <?php endif; ?></label>
        <input type="text" name="nom" placeholder="Toure" id="nom" required="">

         <label for="prenom">Prénom <?php if(!empty($prenomError)):?>
                            <span class="help-inline"><?php echo ':'.$prenomError ;?></span>
                            <?php endif;?></label>
         <input type="text" name="prenom" placeholder="Oumou" id="prenom" required="">
        
          <label for="login">login  <?php if (!empty($loginError)): ?>
                                <span class="help-inline"><?php echo ':'.$loginError;?></span>
                            <?php endif;?></label>
        <input type="text" name="login" placeholder="test" id="login">
        <label for="email">Email<?php if (!empty($emailError)): ?>
                                <span class="help-inline"><?php echo ':'.$emailError;?></span>
                            <?php endif;?></label>
        <input type="email" name="email" placeholder="johndoe@example.com" id="email" required="">
        <label for="password">Mot de passe</label>
         <input type="password" name="password" placeholder="*****" id="password">    
          <label >Activer</label>
        <input style="margin-left: -27%;" type="radio" name="etat" value="Y" id="Y" checked="checked"/>
        <label >Desactiver</label>
         <input style="margin-left: -27%;" type="radio" name="etat" value="N" id="N" />
            </p></div>
        <input type="submit" value="Valider">
        <a href="liste.php"><input type="button" value="Annuler"></a>
    </form>
   
</div>

<?=template_footer()?>