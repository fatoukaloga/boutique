<?php
include '../admin/fonction_include.php';
$pdo = pdo_connect_mysql();
$msg = '';
// Check that the contact ID exists
if (isset($_GET['id'])) {
    // Select the record that is going to be deleted
    $stmt = $pdo->prepare('SELECT * FROM utilisateur WHERE US_ID = ?');
    $stmt->execute([$_GET['id']]);
    $contact = $stmt->fetch(PDO::FETCH_ASSOC);
    if (!$contact) {
        exit('Contact doesn\'t exist with that ID!');
    }
    // Make sure the user confirms beore deletion
    if (isset($_GET['confirm'])) {
        if ($_GET['confirm'] == 'yes') {
            // User clicked the "Yes" button, delete record
            $stmt = $pdo->prepare('DELETE FROM utilisateur WHERE US_ID = ?');
            $stmt->execute([$_GET['id']]);
            $msg = 'Utilisateur supprimé avec succès!';
        } else {
            // User clicked the "No" button, redirect them back to the read page
            header('Location: liste.php');
            exit;
        }
    }
} else {
    exit('identifiant non specifique!');
}
?>
<?=template_header('Delete')?>

<div class="content delete">
	<h2>Suppression d'utilisateur #<?=$contact['US_NOM']?></h2>
    <?php if ($msg): ?>
    <p><?=$msg?></p>
    <?php else: ?>
	<p>Etes vous sur de vouloir supprimer #<?=$contact['US_NOM']?>?</p>
    <div class="yesno">
        <a href="delete.php?id=<?=$contact['US_ID']?>&confirm=Oui">Yes</a>
        <a href="delete.php?id=<?=$contact['US_ID']?>&confirm=Non">No</a>
    </div>
    <?php endif; ?>
</div>

<?=template_footer()?>