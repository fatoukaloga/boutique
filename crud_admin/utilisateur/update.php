<?php
include '../admin/fonction_include.php';
$pdo = pdo_connect_mysql();
$msg = '';
// Check if the contact id exists, for example update.php?id=1 will get the contact with the id of 1
if (isset($_GET['id'])) {
    if (!empty($_POST)) {
        // This part is similar to the create.php, but instead we update a record and not insert
        $id = isset($_POST['id']) ? $_POST['id'] : NULL;
        $nom = isset($_POST['nom']) ? $_POST['nom'] : '';
        $prenom = isset($_POST['prenom']) ? $_POST['prenom'] : '';
        $login = isset($_POST['login']) ? $_POST['login'] : '';
        $password = isset($_POST['password']) ? md5(htmlentities(trim($_POST['password']))) : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';    
        $created = isset($_POST['created']) ? $_POST['created'] : gmdate('Y-m-d H:i:s');
        // Update the record
        $stmt = $pdo->prepare('UPDATE utilisateur SET US_ID = ?,US_NOM = ?,US_PRENOM = ?,US_LOGIN = ?,US_PASSWORD = ?,US_EMAIL = ?,US_DATECREATE = ? WHERE US_ID = ?');
        $stmt->execute([$_GET['id'],$nom,$prenom,$login,$password,$email,$created, $_GET['id']]);
        $msg = 'Mise à jour faite avec succès';
    }
    // Get the contact from the contacts table
    $stmt = $pdo->prepare('SELECT * FROM utilisateur WHERE US_ID = ?');
 
    $stmt->execute([$_GET['id']]);
    $contact = $stmt->fetch(PDO::FETCH_ASSOC);
    if (!$contact) {
        exit('l\'utilisateur n\'existe pas');
    }
} else {
    exit('Identifiant n\'est pas specifié');
}
?>
<?=template_header('liste')?>

<div class="content update">
     <?php if ($msg): ?>
    <p><?=$msg?></p>
    <?php endif; ?>
	<h2>Modification de #<?=$contact['US_NOM']?></h2>
    <form action="update.php?id=<?=$contact['US_ID']?>" method="post">
        <div class="ligne">
        <p class="premier">
        <label for="nom">Nom</label>
        <input type="text" name="nom" placeholder="John Doe" value="<?=$contact['US_NOM']?>" id="nom">
        <label for="prenom">Prénom</label>
        <input type="text" name="prenom" placeholder="John Doe" value="<?=$contact['US_PRENOM']?>" id="prenom">
        <label for="login">Login</label>
        <input type="text" name="login" placeholder="LOGIN" value="<?=$contact['US_LOGIN']?>" id="login">
        <label for="email">Email</label>
        <input type="text" name="email" placeholder="johndoe@example.com" value="<?=$contact['US_EMAIL']?>" id="email">
        <label for="password">Mot de passe </label>
        <input type="password" name="password" placeholder="*****" id="password">
        </p></div>
        <input type="submit" value="Valider">
        <a href="liste.php"><input type="button" value="Annuler"></a>
    </form>
   
</div>

<?=template_footer()?>