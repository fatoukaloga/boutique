<?php
//session_start();
include '../../db_local.php';

function template_header($title) {
echo <<<EOT
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>$title</title>
		<link href="../css_admin/style_test.css" rel="stylesheet" type="text/css">
                <link href="../css_admin/form_css.css" rel="stylesheet" type="text/css">
                <link href="../../styles/bootstrap-4.1.3/bootstrap.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
                <link rel="stylesheet" href="../admin/fontawesome-free-5.9.0-web/css/v4-shims.min.css">
		<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
   </head>
	<body>
    <nav class="navtop">
    	<div>
         <a href="../admin/adminUser.php"><h1>MesDenrées</h1></a>
         <a href="../admin/adminUser.php"><i class="fas fa-home" style='color: black;'></i>Accueil</a>
         <a href="../localite/liste.php"><i class="fas fa-map-marker" style='color: rgb(80,190,135);'></i>Localite</a>
         <a href="../boutique/liste.php"><i class="fas fa-shopping-cart" style='color: rgb(255,220,0);'></i>Boutique</a>
         <a href="../produit/liste.php"><i class="fa fa-product-hunt" style='color: rgb(145,100,205);'></i>Produit</a>
         <a href="../boutique_produit/liste.php"><i class="fas fa-address-book" style='color: red;'></i>Produits/Boutique</a>
         <a href="../boutique_livreurs/liste.php"><i class="fas fa-taxi" style='color: salmon;'></i>Livreurs_Vendeurs/Boutique</a>      
         <a href="../utilisateur/liste.php"><i class="fas fa-wrench" style='color: turquoise;'></i>Administration</a>
         <a href="../../logout_1.php" title="Déconnexion"><i class="fas fa-power-off" style='color: tomato;'></i>Déconnexion</a>

    	</div>
    </nav>
EOT;
}
function template_footer() {
    
echo <<<EOT
    
    </body>
</html>
EOT;
}
?>
