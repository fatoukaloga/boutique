<?php
include '../admin/fonction_include.php';
$pdo=pdo_connect_mysql();
?>
<?php
if (!empty($_POST)) {
    $produit = $_POST['produit'];
    $boutique = $_POST['boutique'];
    $prix= $_POST['prix'];
    $unite = $_POST['unite'];
    $stock= $_POST['stock'];
    $engros = $_POST['engros'];
    $endetail = $_POST['endetail'];
    $req = 'INSERT INTO boutique_produits(BO_ID,PR_ID,PR_PRIX,PR_UNITE,PR_QUANTITE,PR_PRIXGROS,PR_PRIXDETAIL'
            . ') VALUES (?,?,?,?,?,?,?)';

    $stmt = $pdo->prepare($req);
    $stmt->execute([$boutique, $produit, $prix, $unite, $stock,$engros,$endetail]);
    header('Location: liste.php');}
?>
<?= template_header('Create')?>

<div class="content update">
    <h2>Ajout Produit à une boutique</h2>
    <form method="post" action="create.php" >
        <div class="ligne">
            <p class="premier">
                <label>Produit : </label></br>
                <select name="produit" id="produit">
                    <?php
                    $reponse = $pdo->query('SELECT * FROM produits where PR_ETAT=\'Y\' ');
                    while ($donnees = $reponse->fetch()) {
                        ?>
                        <option value="<?php echo $donnees['PR_ID'];?>"> <?php echo $donnees['PR_LIBELLE'];?></option>
                        <?php
                    }
                    ?>
                </select></br></br>
                <label>Boutique : </label></br>
                <select name="boutique" id="boutique">
                    <?php
                    $reponse = $pdo->query('SELECT * FROM boutique where BO_ETAT=\'Y\' ');
                    while ($donnees = $reponse->fetch()) {
                        ?>
                        <option value="<?php echo $donnees['BO_ID'];?>"> <?php echo $donnees['BO_LIBELLE'];?></option>
                        <?php
                    }
                    ?>
                </select></br></br>
                <label>Prix/FCFA : </label></br>
                <input type="number" id="prix" name="prix" required=""></br></br>
                <label>Unité : </label></br>
                <input type="text" id="unite" name="unite" required=""></br></br>
                               
                <label>Quantité en Stock: </label></br>
                <input type="number" id="stock" name="stock" step="0.000001"></br></br>
                <label>Prix en Gros : </label></br>
                <input type="number" id="engros" name="engros"></br></br>
                <label>Prix en Détail : </label></br>
                <input type="number" id="endetail" name="endetail"></br></br>
                       


            </p>

        </div>
     
            <input type="submit" value="Ajouter" >
            <a href="liste.php">
                    <input type="button" value="Annuler"></a>
        

    </form>
</div>
<?= template_footer() ?>