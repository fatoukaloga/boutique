<?php
include '../admin/fonction_include.php';
$pdo = pdo_connect_mysql();
$msg = '';
// Check that the contact ID exists
if (isset($_GET['id'])) {
    // Select the record that is going to be deleted
    $stmt = $pdo->prepare('SELECT * FROM boutique bo,produits pr,boutique_produits bp where bo.BO_ID=bp.BO_ID and pr.PR_ID=bp.PR_ID AND pr.PR_ETAT=\'Y\' and bo.BO_ETAT=\'Y\' and BP_ID = ?');
    $stmt->execute([$_GET['id']]);
    $produitb = $stmt->fetch(PDO::FETCH_ASSOC);
    if (!$produitb) {
        exit('le produit n\'existe pas!');
    }
    // Make sure the user confirms beore deletion
    if (isset($_GET['confirm'])) {
        if ($_GET['confirm'] == 'yes') {
            // User clicked the "Yes" button, delete record
            $stmt = $pdo->prepare('DELETE FROM boutique_produits WHERE BP_ID = ?');
            $stmt->execute([$_GET['id']]);
            $msg = 'Suppression faite avec succès!';
            header('Location: liste.php');
        } else {
            // User clicked the "No" button, redirect them back to the read page
            header('boutique_produit: liste.php');
            exit;
        }
    }
} else {
    exit('identifiant non specifique!');
}
?>
<?=template_header('Suppression produit de la boutique')?>

<div class="content delete">
	<h2>Suppression Produit #<?=$produitb['PR_LIBELLE']?> de <?=$produitb['BO_LIBELLE']?></h2>
    <?php if ($msg): ?>
    <p><?=$msg?></p>
    <?php else: ?>
	<p>Etes vous sur de vouloir supprimer #<?=$produitb['PR_LIBELLE']?>?</p>
    <div class="yesno">
        <a href="delete.php?id=<?=$produitb['BP_ID']?>&confirm=yes">Yes</a>
        <a href="liste.php">Non</a>
    </div>
    <?php endif; ?>
</div>

<?=template_footer()?>