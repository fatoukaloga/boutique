<?php
include '../admin/fonction_include.php';

// Connect to MySQL database
$pdo = pdo_connect_mysql();
// Get the page via GET request (URL param: page), if non exists default the page to 1
$page = isset($_GET['page']) && is_numeric($_GET['page']) ? (int)$_GET['page'] : 1;
// Number of records to show on each page
$records_per_page = 15;
$num_boutiques=0;
$num_boutiquest=0;
$boutiques_produitt=null;
$boutiques_produit=null;
$rechercher='';
// Prepare the SQL statement and get records from our contacts table, LIMIT will determine the page
if(isset($_POST['rechercher']) && $_POST['rechercher']!=''){
$rechercher          = $_POST['rechercher'];
$stmt = $pdo->prepare('SELECT * FROM boutique bo,produits pr,boutique_produits bp where bo.BO_ID=bp.BO_ID and pr.PR_ID=bp.PR_ID AND pr.PR_ETAT=\'Y\' and bo.BO_ETAT=\'Y\' and (bo.BO_LIBELLE LIKE \'%'.$rechercher.'%\' or pr.PR_LIBELLE LIKE \'%'.$rechercher.'%\' ) ORDER BY bo.BO_ID LIMIT :current_page, :record_per_page');
$stmt->bindValue(':current_page', ($page-1)*$records_per_page, PDO::PARAM_INT);
$stmt->bindValue(':record_per_page', $records_per_page, PDO::PARAM_INT);
$stmt->execute();
// Fetch the records so we can display them in our template.
$boutiques_produitt = $stmt->fetchAll(PDO::FETCH_ASSOC);
$num_boutiquest = $pdo->query('SELECT count(*) FROM boutique bo,produits pr,boutique_produits bp where bo.BO_ID=bp.BO_ID and pr.PR_ID=bp.PR_ID AND pr.PR_ETAT=\'Y\' and bo.BO_ETAT=\'Y\' and (bo.BO_LIBELLE LIKE \'%'.$rechercher.'%\' or pr.PR_LIBELLE LIKE \'%'.$rechercher.'%\' )')->fetchColumn();
$num_boutiques=0;
//echo 'select'. $num_boutiquest.'non trouver::'.$num_boutiques;
} else {
$stmt = $pdo->prepare('SELECT * FROM boutique bo,produits pr,boutique_produits bp where bo.BO_ID=bp.BO_ID and pr.PR_ID=bp.PR_ID AND pr.PR_ETAT=\'Y\' and bo.BO_ETAT=\'Y\' ORDER BY bo.BO_ID LIMIT :current_page, :record_per_page');
$stmt->bindValue(':current_page', ($page-1)*$records_per_page, PDO::PARAM_INT);
$stmt->bindValue(':record_per_page', $records_per_page, PDO::PARAM_INT);
$stmt->execute();
// Fetch the records so we can display them in our template.
$boutiques_produit = $stmt->fetchAll(PDO::FETCH_ASSOC);
$num_boutiques = $pdo->query('SELECT COUNT(*) FROM boutique_produits ')->fetchColumn();
$num_boutiquest=0;
//echo 'non select'.$num_boutiques.'trouver::'.$num_boutiquest;
}


// Get the total number of contacts, this is so we can determine whether there should be a next and previous button

?>

<?=template_header('liste_boutiques')?>
<?php if ($boutiques_produit!=null): ?>
<div class="content read">
	<h2>Liste des Produits par Boutique</h2>
	<a href="create.php" class="create-contact">Ajouter Produit/Boutique</a>
        <form action="liste.php" method="Post" style="margin-left: 50%;display: inline-block;">
            <input type="text" name="rechercher" size="20" placeholder="">
            <input type="submit" value="Rechercher" style="
    text-decoration: none;
    background-color:  #32a367;
    color: white;
    border: none;
    padding: 4px;
}">
        </form>
	<table>
 
        <thead>
            <tr>
               
                <td>Produit</td>
                <td>Boutique</td>
                <td>Prix/FCFA</td>
                <td>Unité</td>
                <td>Qunatité Stock</td>
                <td>Prix en Gros</td>
                <td>Prix en Détail</td>
                <td></td>
            </tr>
        </thead>
        <tbody>
            
            <?php foreach ($boutiques_produit as $boutique): ?>
            <tr>
                <td><?=$boutique['PR_LIBELLE']?></td>
                <td><?=$boutique['BO_LIBELLE']?></td>
                <td><?=$boutique['PR_PRIX']?> </td>
                <td><?=$boutique['PR_UNITE']?></td>
                <td><?=$boutique['PR_QUANTITE']?> </td>
                <td><?=$boutique['PR_PRIXGROS']?> </td>
                <td><?=$boutique['PR_PRIXDETAIL']?> </td>
                
                    <td class="actions" style="width: 10%;">
                        <a href="update.php?id=<?=$boutique['BP_ID']?>" class="edit"><i class="fas fa-pen fa-xs"></i></a>
                    <a href="delete.php?id=<?=$boutique['BP_ID']?>" class="trash"><i class="fas fa-trash fa-xs"></i></a>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
	<div class="pagination">
		<?php if ($page > 1): ?>
		<a href="liste.php?page=<?=$page-1?>"><i class="fas fa-angle-double-left fa-sm"></i></a>
		<?php endif; ?>
		<?php if (($page*$records_per_page < $num_boutiques) && ($num_boutiques>0)): ?>
		<a href="liste.php?page=<?=$page+1?>"><i class="fas fa-angle-double-right fa-sm"></i></a>
		<?php endif; ?>
        </div>
</div>
<?php endif; ?>
<?php if ($boutiques_produitt!=null): ?>
<div class="content read">
	<h2>Liste des Produits par Boutique</h2>
	<a href="create.php" class="create-contact">Ajouter Produit/Boutique</a>
        <form action="liste.php" method="Post" style="margin-left: 50%;display: inline-block;">
            <input type="text" name="rechercher" size="20" placeholder="">
            <input type="submit" value="Rechercher" style="
    text-decoration: none;
    background-color:  #32a367;
    color: white;
    border: none;
    padding: 4px;
}">
        </form>
	<table>
 
        <thead>
            <tr>
               
                <td>Produit</td>
                <td>Boutique</td>
                <td>Prix/FCFA</td>
                <td>Unité</td>
                <td>Qunatité Stock</td>
                <td>Prix en Gros</td>
                <td>Prix en Détail</td>
                <td></td>
            </tr>
        </thead>
        <tbody>
            
            <?php foreach ($boutiques_produitt as $boutique): ?>
            <tr>
                <td><?=$boutique['PR_LIBELLE']?></td>
                <td><?=$boutique['BO_LIBELLE']?></td>
                <td><?=$boutique['PR_PRIX']?> </td>
                <td><?=$boutique['PR_UNITE']?></td>
                <td><?=$boutique['PR_QUANTITE']?> </td>
                <td><?=$boutique['PR_PRIXGROS']?> </td>
                <td><?=$boutique['PR_PRIXDETAIL']?> </td>
                
                    <td class="actions" style="width: 10%;">
                        <a href="update.php?id=<?=$boutique['BP_ID']?>" class="edit"><i class="fas fa-pen fa-xs"></i></a>
                    <a href="delete.php?id=<?=$boutique['BP_ID']?>" class="trash"><i class="fas fa-trash fa-xs"></i></a>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
	<div class="pagination">
		<?php if ($page > 1): ?>
		<a href="liste.php?page=<?=$page-1?>"><i class="fas fa-angle-double-left fa-sm"></i></a>
		<?php endif; ?>
		<?php if (($page*$records_per_page < $num_boutiquest) && ($num_boutiquest>0)): ?>
		<a href="liste.php?page=<?=$page+1?>"><i class="fas fa-angle-double-right fa-sm"></i></a>
		<?php endif; ?>
        </div>
</div>
<?php endif; ?>
<?=template_footer()?>