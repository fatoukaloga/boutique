<?php
include '../admin/fonction_include.php';
$pdo = pdo_connect_mysql();
$msg = '';
// Check if the contact id exists, for example update.php?id=1 will get the contact with the id of 1
if (isset($_GET['id'])) {
    $stmt1 = $pdo->prepare('SELECT * FROM boutique bo,produits pr,boutique_produits bp where bo.BO_ID=bp.BO_ID and pr.PR_ID=bp.PR_ID AND pr.PR_ETAT=\'Y\' and bo.BO_ETAT=\'Y\' and BP_ID = ?');
    $stmt1->execute([$_GET['id']]);
    $boutique_p = $stmt1->fetch(PDO::FETCH_ASSOC);
    if (!empty($_POST)) {
        // This part is similar to the create.php, but instead we update a record and not insert
        
        $produit          = $_POST['produit'];
        $boutique           = $_POST['boutique'];
        $prix           = $_POST['prix'];
        $prix           = doubleval($_POST['prix']);
        $unite           = $_POST['unite'];
        $stock          = doubleval($_POST['stock']);
        $engros          = doubleval($_POST['engros']);
        $endetail           =doubleval($_POST['endetail']);
          
        // Update the record
        $stmt2 = $pdo->prepare('UPDATE boutique_produits SET BP_ID = ?,BO_ID = ?, PR_ID= ?, PR_PRIX= ?, PR_UNITE= ?, PR_QUANTITE=?,PR_PRIXGROS= ?,PR_PRIXDETAIL= ?'
                . ' WHERE BP_ID = ?');
        $stmt2->execute([$_GET['id'],$boutique,$produit,$prix,$unite,$stock,$engros,$endetail,$_GET['id']]);
        $msg = 'Mise à jour faite avec succès';
    }
    // Get the contact from the contacts table
    if ($boutique_p) {
        $stmtBTYPE = $pdo->prepare('SELECT * FROM boutique WHERE BO_ID = ?');
        $stmtBTYPE->execute([$boutique_p['BO_ID']]);
        $BoutiqueDefault = $stmtBTYPE->fetch(PDO::FETCH_ASSOC);
        $stmtp = $pdo->prepare('SELECT * FROM produits WHERE PR_ID = ?');
        $stmtp->execute([$boutique_p['PR_ID']]);
        $produitDefault = $stmtp->fetch(PDO::FETCH_ASSOC);
    }
    if (!$boutique_p) {
        exit('le produit n\'existe pas');
    }
} else {
    exit('Identifiant n\'est pas specifié');
}
?>
<?= template_header('Modification_produit_boutique') ?>

<div class="content update">

    <?php if ($msg): ?>
        <p><?= $msg ?></p>
    <?php endif; ?>
    <h2>Modification Produit #<?= $boutique_p['PR_LIBELLE'] ?> de <?= $boutique_p['BO_LIBELLE'] ?></h2>

    <form action="update.php?id=<?= $boutique_p['BP_ID'] ?>" method="post">
        <div class="ligne">
            <p class="premier">
                
                <label>Produit : </label></br>
             <select name="produit" id="produit"><br/><br/>
                    <option value="<?php echo $produitDefault['PR_ID']; ?>" selected="selected" > <?php echo $produitDefault['PR_LIBELLE']; ?></option>

                    <?php
                    $reponse = $pdo->query('SELECT * FROM produits where PR_ETAT="Y" and PR_ID !=' . $produitDefault['PR_ID']);
                    while ($donnees = $reponse->fetch()) {
                        ?>
                        <option value="<?php echo $donnees['PR_ID']; ?>"> <?php echo $donnees['PR_LIBELLE']; ?></option><br/>
                        <?php
                    }
                    ?>
                </select><br/>
            <label>Boutique : </label></br>
             <select name="boutique" id="boutique"><br/><br/>
                    <option value="<?php echo $BoutiqueDefault['BO_ID']; ?>" selected="selected" > <?php echo $BoutiqueDefault['BO_LIBELLE']; ?></option>

                    <?php
                    $reponse = $pdo->query('SELECT * FROM boutique where BO_ETAT="Y" and BO_ID !=' . $BoutiqueDefault['BO_ID']);
                    while ($donnees = $reponse->fetch()) {
                        ?>
                        <option value="<?php echo $donnees['BO_ID']; ?>"> <?php echo $donnees['BO_LIBELLE']; ?></option><br/>
                        <?php
                    }
                    ?>
                </select><br/>
             <label>Prix : </label></br>
             <input type="text" id="prix" name="prix"  value="<?=$boutique_p['PR_PRIX']?>"></br></br> 
             <label>Unite/FCFA : </label></br>
             <input type="text" id="unite" name="unite" value="<?=$boutique_p['PR_UNITE']?>" ></br></br>
              <label>Quantite de Stock: </label></br>
              <input type="text" id="stock" name="stock" value="<?=$boutique_p['PR_QUANTITE']?>"></br></br>
             <label>Prix En Gros/FCFA : </label></br>
             <input type="text" id="engros" name="engros" value="<?=$boutique_p['PR_PRIXGROS']?>"></br></br>
            <label>Pris en Détail/FCFA : </label></br>
            <input type="text" id="endetail" name="endetail" value="<?=$boutique_p['PR_PRIXDETAIL']?>"></br></br>
                                 
            </p></div>
        
            <input type="submit" value="Modifier" >
            <a href="liste.php">
            <input type="button" value="Annuler"></a>
    </form>


</div>

<?=
template_footer()?>