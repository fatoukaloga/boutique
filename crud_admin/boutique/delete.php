<?php
include '../admin/fonction_include.php';
$pdo = pdo_connect_mysql();
$msg = '';
// Check that the contact ID exists
if (isset($_GET['id'])) {
    // Select the record that is going to be deleted
    $stmt = $pdo->prepare('SELECT * FROM boutique WHERE BO_ID = ?');
    $stmt->execute([$_GET['id']]);
    $boutique = $stmt->fetch(PDO::FETCH_ASSOC);
    if (!$boutique) {
        exit('la boutique n\'existe pas!');
    }
    // Make sure the user confirms beore deletion
    if (isset($_GET['confirm'])) {
        if ($_GET['confirm'] == 'yes') {
            // User clicked the "Yes" button, delete record
            $stmt = $pdo->prepare('DELETE FROM boutique WHERE BO_ID = ?');
            $stmt->execute([$_GET['id']]);
            $msg = 'Suppression faite avec succès!';
        } else {
            // User clicked the "No" button, redirect them back to the read page
            header('boutique: liste.php');
            exit;
        }
    }
} else {
    exit('identifiant non specifique!');
}
?>
<?=template_header('Suppression boutique')?>

<div class="content delete">
	<h2>Suppression de #<?=$boutique['BO_LIBELLE']?></h2>
    <?php if ($msg): ?>
    <p><?=$msg?></p>
    <?php else: ?>
	<p>Etes vous sur de vouloir supprimer #<?=$boutique['BO_LIBELLE']?>?</p>
    <div class="yesno">
        <a href="delete.php?id=<?=$boutique['BO_ID']?>&confirm=yes">Yes</a>
        <a href="liste.php">Non</a>
    </div>
    <?php endif; ?>
</div>

<?=template_footer()?>