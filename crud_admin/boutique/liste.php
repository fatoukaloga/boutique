<?php
session_start();
if(isset($_SESSION['login'])){

include '../admin/fonction_include.php';

// Connect to MySQL database
$pdo = pdo_connect_mysql();
// Get the page via GET request (URL param: page), if non exists default the page to 1
$page = isset($_GET['page']) && is_numeric($_GET['page']) ? (int)$_GET['page'] : 1;
// Number of records to show on each page
$records_per_page = 10;
// Prepare the SQL statement and get records from our contacts table, LIMIT will determine the page
$stmt = $pdo->prepare('SELECT * FROM boutique bo,boutique_type bt,localite lo where bo.BO_TYPE=bt.BT_ID and bo.LO_ID=lo.LO_ID AND bo.BO_ETAT=\'Y\' ORDER BY BO_ID LIMIT :current_page, :record_per_page');
$stmt->bindValue(':current_page', ($page-1)*$records_per_page, PDO::PARAM_INT);
$stmt->bindValue(':record_per_page', $records_per_page, PDO::PARAM_INT);
$stmt->execute();
// Fetch the records so we can display them in our template.
$boutiques = $stmt->fetchAll(PDO::FETCH_ASSOC);
// Get the total number of contacts, this is so we can determine whether there should be a next and previous button
$num_boutiques = $pdo->query('SELECT COUNT(*) FROM boutique where BO_ETAT=\'Y\'')->fetchColumn();

?>
<?=template_header('liste_boutiques')?>

<div class="content read">
	<h2>Liste des Boutiques</h2>
	<a href="create.php" class="create-contact">Ajouter Boutique</a>
<!--         <a href="exporter.php" class="create-contact">Exporte en xls</a>-->
	<table>
        <thead>
            <tr>
               
                <td>Nom</td>
                <td>Quartier</td>
                <td>Contact</td>
                <td>Numero Contact</td>
                <td>Type</td>
                <td></td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($boutiques as $boutique): ?>
            <tr>
                <td><?=$boutique['BO_LIBELLE']?></td>
                <td><?=$boutique['BO_QUARTIER']?> </td>
                <td><?=$boutique['BO_CONTACT']?></td>
                <td><?=$boutique['BO_NUMERO_TELEPHONE']?> </td>
                <td><?=$boutique['BT_LIBELLE']?> </td>
                
                
                    <td class="actions" style="width: 10%;">
                        <a href="update.php?id=<?=$boutique['BO_ID']?>" class="edit"><i class="fas fa-pen fa-xs"></i></a>
                    <a href="delete.php?id=<?=$boutique['BO_ID']?>" class="trash"><i class="fas fa-trash fa-xs"></i></a>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
	<div class="pagination">
		<?php if ($page > 1): ?>
		<a href="liste.php?page=<?=$page-1?>"><i class="fas fa-angle-double-left fa-sm"></i></a>
		<?php endif; ?>
		<?php if ($page*$records_per_page < $num_boutiques): ?>
		<a href="liste.php?page=<?=$page+1?>"><i class="fas fa-angle-double-right fa-sm"></i></a>
		<?php endif; ?>
	</div>
</div>

<?=template_footer()?>
<?php } else {
echo '<p>Vous n\'etes pas connecté</p>';
 
}?>
