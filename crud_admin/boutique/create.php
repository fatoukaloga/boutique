<?php
include '../admin/fonction_include.php';
$pdo = pdo_connect_mysql();
?>
<?php
$msg='';
if (!empty($_POST)) {
    $libelle = $_POST['libelle'];
    $quartier = $_POST['quartier'];
    $long = $_POST['long'];
    $lat = $_POST['lat'];
    $houvert = $_POST['houvert'];
    $hferme = $_POST['hferme'];
    $email = $_POST['email'];
    $site = $_POST['site'];
    $etat = $_POST['etat'];
    $contact = $_POST['contact'];
    $phone = $_POST['phone'];
    $commune = $_POST['commune'];
    $type = $_POST['type'];
    $sql = "SELECT * FROM utilisateur WHERE US_LOGIN= ? AND US_PASSWORD=? ";
        $q = $pdo->prepare($sql);
        $q->execute(array( $_SESSION['login'] ,  $_SESSION['password'] ));
        $data = $q->fetch(PDO::FETCH_ASSOC);
        $iduser=1;
        if($data){
            $iduser=$data['US_ID'];
        } else {
           $iduser=1; 
        }
    $req = 'INSERT INTO boutique (LO_ID,BO_LIBELLE,BO_ETAT,BO_QUARTIER,BO_LONGITUDE,BO_LATITUDE,BO_HEURE_OUVERTURE,BO_HEURE_FERMETURE,'
            . 'BO_NUMERO_TELEPHONE,BO_EMAIL,BO_SITEWEB,BO_CONTACT,US_CREATE,BO_TYPE) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
            echo $iduser;
                        

//           echo $commune.':'.$type.':::'.$houvert.'::'.$hferme;
    $stmt = $pdo->prepare($req);
    $stmt->execute([$commune, $libelle, $etat, $quartier, $long, $lat, $houvert, $hferme, $phone,
        $email, $site, $contact, $iduser, $type]);
    $msg = 'création faite avec succès';
}
?>
<?= template_header('Create') ?>

<div class="content update">
    <?php if ($msg): ?>
        <p><?= $msg ?></p>
    <?php endif; ?>
<h2>Création de Boutique</h2>
    <form method="post" action="create.php" >
        <div class="ligne">
            <p class="premier">
                <label>Nom : </label><input type="text" id="libelle" name="libelle" required=""></br></br>
                <label>Quartier : </label></br>
                <input type="text" id="quartier" name="quartier"></br></br>
                <label>Longitude : </label></br>
                <input type="number" id="long" name="long" step="0.00000000000001"></br></br>
                <label>Latitude : </label></br>
                <input type="number" id="lat" name="lat" step="0.00000000000001"></br></br>
                <label>Heure Ouverture : </label></br>
                <input type="time" id="houvert" name="houvert"></br></br>
                <label>Heure Fermeture : </label></br>
                <input type="time" id="hferme" name="hferme"></br></br>
                <label>Email : </label></br>
                <input type="email" id="email" name="email"></br></br>
                <label>Site Web : </label></br>
                <input type="text" id="site" name="site"></br></br>
                <label>Personne à Contacter : </label></br>
                <input type="text" id="contact" name="contact"></br></br>
                <label>Numéro de la personne : </label></br>
                <input type="tel" id="phone" name="phone"></br></br>

                <label for="commune">Région/Commune : </label><br />
                <select name="commune" id="commune">

                    <?php
                    $reponse = $pdo->query('SELECT * FROM localite where LO_ETAT=\'Y\' ');
                    while ($donnees = $reponse->fetch()) {
                        ?>
                        <option value="<?php echo $donnees['LO_ID']; ?>"> <?php echo $donnees['LO_LIBELLE']; ?></option>
                        <?php
                    }
                    ?>
                </select><br/>

                <label for="type">Type Boutique : </label><br />
                <select name="type" id="type">

<?php
$reponse = $pdo->query('SELECT * FROM boutique_type');
while ($donnees = $reponse->fetch()) {
    ?>
                        <option value="<?php echo $donnees['BT_ID']; ?>"> <?php echo $donnees['BT_LIBELLE']; ?></option>
                        <?php
                    }
                    ?>
                </select><br/>

                <label >Activer</label>
                <input style="margin-left: -28%;" type="radio" name="etat" value="Y" id="Y" checked="checked"/>
                <label >Desactiver</label>
                <input style="margin-left: -27%;" type="radio" name="etat" value="N" id="N" />
               
            </p>

        </div>
        <input type="submit" value="Ajouter" >
    <a href="liste.php">
                <input type="button" value="Annuler"></a>

    </form>
</div>
<?= template_footer() ?>

