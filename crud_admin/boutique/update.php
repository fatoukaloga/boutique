<?php
include '../admin/fonction_include.php';
$pdo = pdo_connect_mysql();
$msg = '';
// Check if the contact id exists, for example update.php?id=1 will get the contact with the id of 1
if (isset($_GET['id'])) {
    $stmt1 = $pdo->prepare('SELECT * FROM boutique WHERE BO_ID = ?');
    $stmt1->execute([$_GET['id']]);
    $boutique = $stmt1->fetch(PDO::FETCH_ASSOC);
    $row = $stmt1->fetchColumn(PDO::FETCH_ASSOC);
    
    if (!empty($_POST)) {
        // This part is similar to the create.php, but instead we update a record and not insert
        $libelle = $_POST['libelle'];
        $quartier = $_POST['quartier'];
        $long = doubleval($_POST['long']);
        $lat = doubleval($_POST['lat']);
        $houvert = $_POST['houvert'];
        $hferme = $_POST['hferme'];
        $email = $_POST['email'];
        $site = $_POST['site'];
        $etat = $_POST['etat'];
        $contact = $_POST['contact'];
        $phone = $_POST['phone'];
        $commune = $_POST['commune'];
        $type = $_POST['type'];
        // Update the record
        $stmt2 = $pdo->prepare('UPDATE boutique SET BO_ID = ?, LO_ID= ?, BO_LIBELLE= ?, BO_ETAT= ?, BO_QUARTIER=?, BO_LONGITUDE= ?, BO_LATITUDE= ?, BO_HEURE_OUVERTURE= ?,BO_HEURE_FERMETURE= ?,'
                . ' BO_NUMERO_TELEPHONE= ?, BO_EMAIL= ?, BO_SITEWEB= ?,BO_CONTACT= ?, US_CREATE= ?,BO_TYPE= ? WHERE BO_ID = ?');
        $stmt2->execute([$_GET['id'], $commune, $libelle, $etat, $quartier, $long, $lat, $houvert, $hferme, $phone,
            $email, $site, $contact, 1, $type, $_GET['id']]);

        $msg = 'Mise à jour faite avec succès';
        
    }
    // Get the contact from the contacts table

    if ($boutique) {
        $stmtBTYPE = $pdo->prepare('SELECT * FROM boutique_type WHERE BT_ID = ?');
        $stmtBTYPE->execute([$boutique['BO_TYPE']]);
        $BTYPEDefault = $stmtBTYPE->fetch(PDO::FETCH_ASSOC);
        $stmtlocalite = $pdo->prepare('SELECT * FROM localite WHERE LO_ID = ?');
        $stmtlocalite->execute([$boutique['LO_ID']]);
        $localiteDefault = $stmtlocalite->fetch(PDO::FETCH_ASSOC);
    }
    if (!$boutique) {
        exit('la boutique n\'existe pas');
    }
} else {
    exit('Identifiant n\'est pas specifié');
}
?>
<?= template_header('Modification_boutique') ?>

<div class="content update">
    <?php if ($msg): ?>
        <p><?= $msg ?></p>
    <?php endif; ?>
    <h2>Modification de #<?= $boutique['BO_LIBELLE'] ?></h2>
    <form action="update.php?id=<?= $boutique['BO_ID'] ?>" method="post">
        <div class="ligne">
            <p class="premier">
                <label>Nom : </label><input type="text" id="libelle" name="libelle" required="" value="<?= $boutique['BO_LIBELLE'] ?>" ></br></br>
                <label>Quartier : </label></br>
                <input type="text" id="quartier" name="quartier" value="<?= $boutique['BO_QUARTIER'] ?>"></br></br>
                <label>Longitude : </label></br>
                <input type="text" id="long" name="long" value="<?= $boutique['BO_LONGITUDE'] ?>" ></br></br> 
                <label>Latitude : </label></br>
                <input type="text" id="lat" name="lat" value="<?= $boutique['BO_LATITUDE'] ?>"></br></br>
                <label>Heure Ouverture : </label></br>
                <input type="text" id="houvert" name="houvert" value="<?= $boutique['BO_HEURE_OUVERTURE'] ?>"></br></br>
                <label>Heure Fermeture : </label></br>

                <input type="text" id="hferme" name="hferme" value="<?= $boutique['BO_HEURE_FERMETURE'] ?>"></br></br>
                <label>Email : </label></br>
                <input type="email" id="email" name="email" value="<?= $boutique['BO_EMAIL'] ?>"></br></br>
                <label>Site Web : </label></br>
                <input type="text" id="site" name="site" value="<?= $boutique['BO_SITEWEB'] ?>"></br></br>
                <label>Personne à Contacter : </label></br>
                <input type="text" id="contact" name="contact" value="<?= $boutique['BO_CONTACT'] ?>"></br></br>
                <label>Numéro de la personne : </label></br>
                <input type="tel" id="phone" name="phone" value="<?= $boutique['BO_NUMERO_TELEPHONE'] ?>"></br></br>

                <label for="type">Région/Commune : </label><br/>
                <select name="commune" id="commune"><br/><br/>
                    <option value="<?php echo $localiteDefault['LO_ID']; ?>" selected="selected" > <?php echo $localiteDefault['LO_LIBELLE']; ?></option>

                    <?php
                    $reponse = $pdo->query('SELECT * FROM localite where LO_ETAT="Y" and LO_ID !=' . $localiteDefault['LO_ID']);
                    while ($donnees = $reponse->fetch()) {
                        ?>
                        <option value="<?php echo $donnees['LO_ID']; ?>"> <?php echo $donnees['LO_LIBELLE']; ?></option><br/>
                        <?php
                    }
                    ?>
                </select><br/>


                <label for="type">Type : </label><br/>
                <select name="type" id="type"><br/><br/>
                    <option value="<?php echo $BTYPEDefault['BT_ID']; ?>" selected="selected" > <?php echo $BTYPEDefault['BT_LIBELLE']; ?></option>

                    <?php
                    $reponse = $pdo->query('SELECT * FROM boutique_type where BT_ID !=' . $BTYPEDefault['BT_ID']);
                    while ($donnees = $reponse->fetch()) {
                        ?>
                        <option value="<?php echo $donnees['BT_ID']; ?>"> <?php echo $donnees['BT_LIBELLE']; ?></option><br/>
                        <?php
                    }
                    ?>
                </select><br/>

                <label >Activer</label>
                <input style="margin-left: -28%;" type="radio" name="etat" value="Y" id="Y" checked="checked"/>
                <label >Desactiver</label>
                <input style="margin-left: -27%;" type="radio" name="etat" value="N" id="N" />

            </p></div>
     
            <a href="liste.php"><input type="submit" value="Modifier" ></a>
            <a href="liste.php">
                    <input type="button" value="Annuler"></a>
        
    </form>


</div>

<?=
template_footer()?>