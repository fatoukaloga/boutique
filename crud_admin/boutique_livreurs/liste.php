<?php
include '../admin/fonction_include.php';

// Connect to MySQL database
$pdo = pdo_connect_mysql();
// Get the page via GET request (URL param: page), if non exists default the page to 1
$page = isset($_GET['page']) && is_numeric($_GET['page']) ? (int)$_GET['page'] : 1;
// Number of records to show on each page
$records_per_page = 10;
// Prepare the SQL statement and get records from our contacts table, LIMIT will determine the page
$stmt = $pdo->prepare('SELECT * FROM boutique bo,livreur_vendeur lv where bo.BO_ID=lv.BO_ID and bo.BO_ETAT=\'Y\' ORDER BY bo.BO_ID LIMIT :current_page, :record_per_page');
$stmt->bindValue(':current_page', ($page-1)*$records_per_page, PDO::PARAM_INT);
$stmt->bindValue(':record_per_page', $records_per_page, PDO::PARAM_INT);
$stmt->execute();
// Fetch the records so we can display them in our template.
$boutiques_livreur = $stmt->fetchAll(PDO::FETCH_ASSOC);
// Get the total number of contacts, this is so we can determine whether there should be a next and previous button
$num_boutiques = $pdo->query('SELECT COUNT(*) FROM livreur_vendeur ')->fetchColumn();
?>

<?=template_header('liste_liveurs_vendeurs')?>

<div class="content read">
	<h2>Liste des Livreurs/Vendeurs par Boutique</h2>
	<a href="create.php" class="create-contact">Ajouter Livreur_Vendeur/Boutique</a>
<!--        <form action="liste.php" method="Post">
            <input type="text" name="rechercher" size="21" placeholder="">
            <input type="submit" value="Rechercher">
        </form>-->
	<table>
 
        <thead>
            <tr>
               
                <td>Nom</td>
                <td>Prénom</td>
                <td>Téléphone</td>
                <td>Boutique</td>
<!--                <td>Type</td>-->
                
                <td></td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($boutiques_livreur as $livreur): ?>
            <tr>
                <td><?=$livreur['LV_NOM']?></td>
                <td><?=$livreur['LV_PRENOM']?></td>
                <td><?=$livreur['LV_TELEPHONE']?> </td>
                <td><?=$livreur['BO_LIBELLE']?></td>
<!--                <td><?=$boutique['LV_TYPE']?> </td>-->
                
                
                
                    <td class="actions" style="width: 10%;">
                        <a href="update.php?id=<?=$livreur['LV_ID']?>" class="edit"><i class="fas fa-pen fa-xs"></i></a>
                    <a href="delete.php?id=<?=$livreur['LV_ID']?>" class="trash"><i class="fas fa-trash fa-xs"></i></a>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
	<div class="pagination">
		<?php if ($page > 1): ?>
		<a href="liste.php?page=<?=$page-1?>"><i class="fas fa-angle-double-left fa-sm"></i></a>
		<?php endif; ?>
		<?php if ($page*$records_per_page < $num_boutiques): ?>
		<a href="liste.php?page=<?=$page+1?>"><i class="fas fa-angle-double-right fa-sm"></i></a>
		<?php endif; ?>
	</div>
</div>

<?=template_footer()?>