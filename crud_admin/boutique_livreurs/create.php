<?php
include '../admin/fonction_include.php';
$pdo=pdo_connect_mysql();
?>
<?php
if (!empty($_POST)) {
    $nom = $_POST['nom'];
    $prenom = $_POST['prenom'];
    $telephone= $_POST['telephone'];
    $type = $_POST['type'];
    $boutique= $_POST['boutique'];
    $etat = $_POST['etat'];
    $req = 'INSERT INTO livreur_vendeur(LV_NOM,LV_PRENOM,LV_TELEPHONE,LV_ETAT,LV_TYPE,BO_ID'
            . ') VALUES (?,?,?,?,?,?)';

    $stmt = $pdo->prepare($req);
    $stmt->execute([$nom, $prenom, $telephone, $etat,$type,$boutique]);
    header('Location: liste.php');}
?>
<?= template_header('Create')?>

<div class="content update">
    <h2>Création Livreur/Vendeur</h2>
    <form method="post" action="create.php" >
        <div class="ligne">
            <p class="premier">
                <label>Nom : </label></br>
                <input type="text" id="nom" name="nom" required=""></br></br>
                <label>Prénom: </label></br>
                <input type="text" id="prenom" name="prenom" required=""></br></br> 
               <label>Téléphone : </label></br>
               <input type="text" id="telephone" name="telephone" required=""></br></br>
                        
                <label>Type : </label></br>
                <select name="type" id="type">
                <option value="L"> Livreur</option>
                <option value="V"> Vendeur</option>   
                </select></br></br>
                <label>Boutique : </label></br>                                
                <select name="boutique" id="boutique">
                    <?php
                    $reponse = $pdo->query('SELECT * FROM boutique where BO_ETAT=\'Y\' ');
                    while ($donnees = $reponse->fetch()) {
                        ?>
                        <option value="<?php echo $donnees['BO_ID'];?>"> <?php echo $donnees['BO_LIBELLE'];?></option>
                        <?php
                    }
                    ?>
                </select></br></br>
                <label >Activer</label>
                <input style="margin-left: -28%;" type="radio" name="etat" value="Y" id="Y" checked="checked"/>
                <label >Desactiver</label>
                <input style="margin-left: -27%;" type="radio" name="etat" value="N" id="N" />

            </p>

        </div>
        <input type="submit" value="Ajouter" >
            <a href="liste.php">
                <input type="button" value="Annuler"></a>

    </form>
</div>
<?= template_footer() ?>