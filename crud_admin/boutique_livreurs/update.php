<?php
include '../admin/fonction_include.php';
$pdo = pdo_connect_mysql();
$msg = '';
// Check if the contact id exists, for example update.php?id=1 will get the contact with the id of 1
if (isset($_GET['id'])) {
    $stmt1 = $pdo->prepare('SELECT * FROM boutique bo,livreur_vendeur lv where bo.BO_ID=lv.BO_ID and bo.BO_ETAT=\'Y\' and lv.LV_ID = ?');
    $stmt1->execute([$_GET['id']]);
    $boutique_L = $stmt1->fetch(PDO::FETCH_ASSOC);
    if (!empty($_POST)) {
        // This part is similar to the create.php, but instead we update a record and not insert
        
        $nom          = $_POST['nom'];
        $prenom           = $_POST['prenom'];
        $telephone           = $_POST['telephone'];
        $etat           =$_POST['etat'];
        $type           = $_POST['type'];
        $boutique          =$_POST['boutique'];
        
        
        // Update the record
        $stmt2 = $pdo->prepare('UPDATE livreur_vendeur SET '
                . 'LV_ID = ?,LV_NOM= ?, LV_PRENOM= ?, LV_TELEPHONE= ?, LV_ETAT=?,'
                . 'LV_TYPE= ?,'. ' BO_ID = ? WHERE LV_ID = ?');
        $stmt2->execute([$_GET['id'],$nom,$prenom,$telephone,$etat,$type,$boutique,$_GET['id']]);
        $msg = 'Mise à jour faite avec succès';
        header('Location: liste.php');
    }
    // Get the contact from the contacts table

    if ($boutique_L) {
        $stmtBTYPE = $pdo->prepare('SELECT * FROM boutique WHERE BO_ID = ?');
        $stmtBTYPE->execute([$boutique_L['BO_ID']]);
        $BoutiqueDefault = $stmtBTYPE->fetch(PDO::FETCH_ASSOC);
       }
    if (!$boutique_L) {
        exit('livreur ou vendeur n\'existe pas');
    }
} else {
    exit('Identifiant n\'est pas specifié');
}
?>
<?= template_header('Modification_livreur_vendeur') ?>

<div class="content update">

    <?php if ($msg): ?>
        <p><?= $msg ?></p>
    <?php endif; ?>
    <h2>Modification Livreur #<?= $boutique_L['LV_NOM'] ?> <?= $boutique_L['LV_PRENOM'] ?></h2>

    <form action="update.php?id=<?= $boutique_L['LV_ID'] ?>" method="post">
        <div class="ligne">
            <p class="premier">
             
             <label>Nom : </label></br>
             <input type="text" id="nom" name="nom"  value="<?=$boutique_L['LV_NOM']?>"></br></br> 
             <label>PRENOM : </label></br>
             <input type="text" id="prenom" name="prenom" value="<?=$boutique_L['LV_PRENOM']?>" ></br></br>
              <label>TELEPHONE: </label></br>
              <input type="text" id="telephone" name="telephone" value="<?=$boutique_L['LV_TELEPHONE']?>"></br></br>
                
              <label>Type : </label></br>
              <select name="type" id="type"><br/><br/>
                  <?php if ($boutique_L['LV_TYPE'] == 'L') { ?>
                      <option value="<?= $boutique_L['LV_TYPE'] ?>" selected="selected" > Livreur</option>
                      <option value="V"> Vendeur</option>
                  <?php } ?>
                  <?php if ($boutique_L['LV_TYPE'] == 'V') { ?>
                      <option value="<?= $boutique_L['LV_TYPE'] ?>" selected="selected" > Vendeur</option>
                      <option value="L" > Livreur</option>
                  <?php } ?>

              </select><br/>
              <label>Boutique : </label></br>
              <select name="boutique" id="boutique"><br/><br/>
                  <option value="<?php echo $BoutiqueDefault['BO_ID']; ?>" selected="selected" > <?php echo $BoutiqueDefault['BO_LIBELLE']; ?></option>
                  <?php
                  $reponse = $pdo->query('SELECT * FROM boutique where BO_ETAT="Y" and BO_ID !=' . $BoutiqueDefault['BO_ID']);
                  while ($donnees = $reponse->fetch()) {
                      ?>
                      <option value="<?php echo $donnees['BO_ID']; ?>"> <?php echo $donnees['BO_LIBELLE']; ?></option><br/>
                      <?php
                  }
                  ?>
              </select><br/>
                <label >Activer</label>
                <input style="margin-left: -28%;" type="radio" name="etat" value="Y" id="Y" checked="checked"/>
                <label >Desactiver</label>
                <input style="margin-left: -27%;" type="radio" name="etat" value="N" id="N" />
            </p></div>
       
            <input type="submit" value="Modifier" >
            <a href="liste.php">
            <input type="button" value="Annuler"></a>
     
    </form>


</div>

<?=
template_footer()?>