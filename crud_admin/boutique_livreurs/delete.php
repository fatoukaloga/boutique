<?php
include '../admin/fonction_include.php';
$pdo = pdo_connect_mysql();
$msg = '';
// Check that the contact ID exists
if (isset($_GET['id'])) {
    // Select the record that is going to be deleted
    $stmt = $pdo->prepare('SELECT * FROM boutique bo,livreur_vendeur lv where bo.BO_ID=lv.BO_ID and bo.BO_ETAT=\'Y\' and LV_ID = ?');
    $stmt->execute([$_GET['id']]);
    $livreur = $stmt->fetch(PDO::FETCH_ASSOC);
    if (!$livreur) {
        exit('livreur ou vendeur n\'existe pas!');
    }
    // Make sure the user confirms beore deletion
    if (isset($_GET['confirm'])) {
        if ($_GET['confirm'] == 'yes') {
            // User clicked the "Yes" button, delete record
            $stmt = $pdo->prepare('DELETE FROM livreur_vendeur WHERE LV_ID = ?');
            $stmt->execute([$_GET['id']]);
            $msg = 'Suppression faite avec succès!';
            header('Location: liste.php');
        } else {
            // User clicked the "No" button, redirect them back to the read page
            header('livreur_vendeur: liste.php');
            exit;
        }
    }
} else {
    exit('identifiant non specifique!');
}
?>
<?=template_header('Suppression livreur de la boutique')?>

<div class="content delete">
	<h2>Suppression Livreur/Vendeur #<?=$livreur['LV_PRENOM']?> de <?=$livreur['BO_LIBELLE']?></h2>
    <?php if ($msg): ?>
    <p><?=$msg?></p>
    <?php else: ?>
	<p>Etes vous sur de vouloir supprimer #<?=$livreur['LV_PRENOM']?>?</p>
    <div class="yesno">
        <a href="delete.php?id=<?=$livreur['LV_ID']?>&confirm=yes">Yes</a>
        <a href="liste.php">Non</a>
    </div>
    <?php endif; ?>
</div>

<?=template_footer()?>