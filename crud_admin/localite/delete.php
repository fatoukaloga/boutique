<?php
include '../admin/fonction_include.php';
$pdo = pdo_connect_mysql();
$msg = '';
// Check that the contact ID exists
if (isset($_GET['id'])) {
    // Select the record that is going to be deleted
    $stmt = $pdo->prepare('SELECT * FROM localite WHERE LO_ID = ?');
    $stmt->execute([$_GET['id']]);
    $contact = $stmt->fetch(PDO::FETCH_ASSOC);
    if (!$contact) {
        exit('la localite n\'existe pas!');
    }
    // Make sure the user confirms beore deletion
    if (isset($_GET['confirm'])) {
        if ($_GET['confirm'] == 'yes') {
            // User clicked the "Yes" button, delete record
            $stmt = $pdo->prepare('DELETE FROM localite WHERE LO_ID = ?');
            $stmt->execute([$_GET['id']]);
            $msg = 'Commune supprimée avec succès!';
        } else {
            // User clicked the "No" button, redirect them back to the read page
            header('localite: liste.php');
            exit;
        }
    }
} else {
    exit('identifiant non specifique!');
}
?>
<?=template_header('Suppression Commune')?>

<div class="content delete">
	<h2>Suppression de la #<?=$contact['LO_LIBELLE']?></h2>
    <?php if ($msg): ?>
    <p><?=$msg?></p>
    <?php else: ?>
	<p>Etes vous sur de vouloir supprimer #<?=$contact['LO_LIBELLE']?>?</p>
    <div class="yesno">
        <a href="delete.php?id=<?=$contact['LO_ID']?>&confirm=yes">Yes</a>
        <a href="delete.php?id=<?=$contact['LO_ID']?>&confirm=no">Non</a>
    </div>
    <?php endif; ?>
</div>

<?=template_footer()?>