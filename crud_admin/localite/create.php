<?php
include '../admin/fonction_include.php';
$pdo = pdo_connect_mysql();
$msg = '';
$valid = true;
// Check if POST data is not empty
if (!empty($_POST)) {
    $libelleError = '';
    $codeError = '';
    $etatError = '';   // on recupère nos valeurs 
    // Post data not empty insert a new record
    // Set-up the variables that are going to be inserted, we must check if the POST variables exist if not we can default them to blank
    $id = isset($_POST['id']) && !empty($_POST['id']) && $_POST['id'] != 'auto' ? $_POST['id'] : NULL;
    $code = htmlentities(trim($_POST['code']));
    $libelle = htmlentities(trim($_POST['libelle']));
    $etat = htmlentities(trim($_POST['etat']));
 $region = htmlentities(trim($_POST['region']));
    //TEST DE VALEURS NULL
    if (empty($code)) {
        $codeError = ' est requis!';
        $valid = false;
    }

    if (empty($libelle)) {
        $libelleError = ' est requis!';
        $valid = false;
    } else if (!preg_match("/^[a-zA-Z ]*$/", $libelle)) {
        $libelleError = "Seules les lettres et les espaces blancs sont autorisés";
    }

    if (empty($etat)) {
        $etatError = ' est requis!';
        $valid = false;
    } else if (!preg_match("/^[a-zA-Z ]*$/", $etat)) {
        $etatError = "Seules les lettres et les espaces blancs sont autorisés";
    }


    // Insert new record into the contacts table
    if ($valid) {
        $stmt = $pdo->prepare('INSERT INTO localite(LO_CODE,LO_LIBELLE,LO_ETAT,LO_PARENT) VALUES (?, ?, ?,?)');
        $stmt->execute([$code, $libelle, $etat, $region]);
        // Output message
        $msg = 'Enregistré avec succès';
    } else {
        $msg = 'Veuillez saisir tous les champs';
    }
}
?><?= template_header('Creation Localite') ?>

<div class="content update">
    <?php if ($msg): ?>
        <p><?= $msg ?></p>
<?php endif; ?>
    <h2>Création Localite</h2>
    <form action="create.php" method="post">

        <div class="ligne">
            <p class="premier">
                <label for="form-code">Code <?php if (!empty($codeError)): ?>
                        <span class="help-inline"><?php echo ':' . $codeError; ?></span>
                <?php endif; ?></label>
                <input type="text" name="code" placeholder="CXXX" id="form-code" required="">
                <label for="form-libelle">Libellé <?php if (!empty($libelleError)): ?>
                        <span class="help-inline"><?php echo ':' . $libelleError; ?></span>
                <?php endif; ?></label>
                <input type="text" name="libelle" placeholder="commune" id="form-libelle">
                
                <label for="region">Region : </label><br />
                <select name="region" id="region">

                    <?php
                    $reponse = $pdo->query('SELECT * FROM localite where LO_ETAT=\'Y\' and  LO_PARENT=0 ');
                    while ($donnees = $reponse->fetch()) {
                        ?>
                     <option value="<?php echo $donnees['LO_ID']; ?>"> 
                    <?php echo $donnees['LO_LIBELLE']; ?>
                     </option>
                    <?php
                    }
                    ?>
                </select><br/>
                
                <label >Activer</label>
                <input style="margin-left: -28%;" type="radio" name="etat" value="Y" id="Y" checked="checked"/>
                <label >Desactiver</label>
                <input style="margin-left: -27%;" type="radio" name="etat" value="N" id="N" />
                          
            </p>
        </div>
        <input type="submit" value="Ajouter" >
        <a href="liste.php"> <input type="button" value="Annuler"></a>
    </form>

</div>
    <?=template_footer()?>