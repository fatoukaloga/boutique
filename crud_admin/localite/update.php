<?php
include '../admin/fonction_include.php';
$pdo = pdo_connect_mysql();
$msg = '';
// Check if the contact id exists, for example update.php?id=1 will get the contact with the id of 1
if (isset($_GET['id'])) {
    if (!empty($_POST)) {
        // This part is similar to the create.php, but instead we update a record and not insert
        $id = isset($_POST['id']) ? $_POST['id'] : NULL;
        $code = isset($_POST['code']) ? $_POST['code'] : '';
        $libelle = isset($_POST['libelle']) ? $_POST['libelle'] : '';
        $etat = isset($_POST['etat']) ? $_POST['etat'] : '';
        $region = isset($_POST['region']) ? $_POST['region'] : '';
        // Update the record
        $stmt = $pdo->prepare('UPDATE localite SET LO_ID = ?,LO_CODE = ?,LO_LIBELLE = ?,LO_ETAT = ?, LO_PARENT=? WHERE LO_ID = ?');
        $stmt->execute([$_GET['id'], $code, $libelle, $etat, $region, $_GET['id']]);
        $msg = 'Mise à jour faite avec succès';
    }
    // Get the contact from the contacts table
    $stmt = $pdo->prepare('SELECT * FROM localite WHERE LO_ID = ?');
    $stmt->execute([$_GET['id']]);
    $localite = $stmt->fetch(PDO::FETCH_ASSOC);
    if ($localite) {
        $stmtlocalite = $pdo->prepare('SELECT * FROM localite WHERE LO_ID = ?');
        $stmtlocalite->execute([$localite['LO_PARENT']]);
        $localiteDefault = $stmtlocalite->fetch(PDO::FETCH_ASSOC);
    }
    if (!$localite) {
        exit('localite n\'existe pas');
    }
} else {
    exit('Identifiant n\'est pas specifié');
}
?>
<?= template_header('Modfication localite') ?>


<div class="content update">
    <h2>Modification de #<?= $localite['LO_CODE'] ?></h2>
    <form action="update.php?id=<?= $localite['LO_ID'] ?>" method="post">

        <div class="ligne">
            <p class="premier">
                <label for="code">Code</label>
                <input type="text" name="code" placeholder="CXX" value="<?= $localite['LO_CODE'] ?>" id="code">

                <label for="libelle">Libelle</label>
                <input type="text" name="libelle" placeholder="Commune" value="<?= $localite['LO_LIBELLE'] ?>" id="libelle">
           
         
                <?php if ($localiteDefault):
                    ?>
                    <label for="type">Region : </label><br/>
                    <select name="region" id="region"><br/><br/>

                        <option value="<?php echo $localiteDefault['LO_ID']; ?>" selected="selected" ><?php echo $localiteDefault['LO_LIBELLE']; ?></option>

                        <?php
                        $reponse = $pdo->query('SELECT * FROM localite where LO_ETAT="Y" and LO_ID !=' . $localiteDefault['LO_ID'] . ' and LO_PARENT=0 ');
                        while ($donnees = $reponse->fetch()) {
                            ?>
                            <option value="<?php echo $donnees['LO_ID']; ?>"> <?php echo $donnees['LO_LIBELLE']; ?></option><br/>
                            <?php
                        }
                        ?>

                    </select><br/>
                <?php endif; ?>
                <label for="oui">Activer</label><input style="margin-left: -28%;" type="radio" name="etat" value="Y" id="Y" checked="checked"/>
                <label for="non">Desactiver</label><input style="margin-left: -28%;" type="radio" name="etat" value="N" id="N" />
            </p>
        </div>
            <input type="submit" value="Modifier">
            <a href="liste.php">
                <input type="button" value="Annuler"></a>
    </form>
    <?php if ($msg): ?>
        <p><?= $msg ?></p>
    <?php endif; ?>
</div>

<?=
template_footer()?>