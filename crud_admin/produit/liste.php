<?php
include '../admin/fonction_include.php';

// Connect to MySQL database
$pdo = pdo_connect_mysql();
// Get the page via GET request (URL param: page), if non exists default the page to 1
$page = isset($_GET['page']) && is_numeric($_GET['page']) ? (int)$_GET['page'] : 1;
// Number of records to show on each page
$records_per_page = 10;
// Prepare the SQL statement and get records from our contacts table, LIMIT will determine the page
$stmt = $pdo->prepare('SELECT * FROM produits ORDER BY PR_ID LIMIT :current_page, :record_per_page');
$stmt->bindValue(':current_page', ($page-1)*$records_per_page, PDO::PARAM_INT);
$stmt->bindValue(':record_per_page', $records_per_page, PDO::PARAM_INT);
$stmt->execute();
// Fetch the records so we can display them in our template.
$produits = $stmt->fetchAll(PDO::FETCH_ASSOC);
// Get the total number of contacts, this is so we can determine whether there should be a next and previous button
$num_produits = $pdo->query('SELECT COUNT(*) FROM produits')->fetchColumn();

?>
<?=template_header('liste_produits')?>

<div class="content read">
	<h2>Liste des Produits</h2>
	<a href="create.php" class="create-contact">Ajouter Produit</a>
	<table>
        <thead>
            <tr>
               
                <td>Libelle</td>
                <td>Détail</td>
                <td>Photo</td>
                 <td></td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($produits as $produit): ?>
            <tr>
                <td><?=$produit['PR_LIBELLE']?></td>
                <td><?=$produit['PR_DETAIL']?>                    
                <div style="background-image:url('../../images/mesDenreesImg/<?=$produit['PR_PHOTO']?>')"></div></td>
                <td style="width: 200px;
                height: 180px;background-image:url('../../images/mesDenreesImg/<?=$produit['PR_PHOTO']?>')">
                </td>
                         
                <td class="actions" style="width: 10%;">
                    <a href="update.php?id=<?=$produit['PR_ID']?>" class="edit"><i class="fas fa-pen fa-xs"></i></a>
                    <a href="delete.php?id=<?=$produit['PR_ID']?>" class="trash"><i class="fas fa-trash fa-xs"></i></a>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
	<div class="pagination">
		<?php if ($page > 1): ?>
		<a href="liste.php?page=<?=$page-1?>"><i class="fas fa-angle-double-left fa-sm"></i></a>
		<?php endif; ?>
		<?php if ($page*$records_per_page < $num_produits): ?>
		<a href="liste.php?page=<?=$page+1?>"><i class="fas fa-angle-double-right fa-sm"></i></a>
		<?php endif; ?>
	</div>
</div>

<?=template_footer()?>