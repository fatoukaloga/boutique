<?php
include '../admin/fonction_include.php';
$pdo = pdo_connect_mysql();
$msg = '';
// Check if the contact id exists, for example update.php?id=1 will get the contact with the id of 1
if (isset($_GET['id'])) {
     $stmt1 = $pdo->prepare('SELECT * FROM produits WHERE PR_ID = ?');
     $stmt1->execute([$_GET['id']]);
    $produit = $stmt1->fetch(PDO::FETCH_ASSOC);
    $imgAncien=$produit['PR_PHOTO'];  
       
    if (!empty($_POST)) {
        // This part is similar to the create.php, but instead we update a record and not insert
        $id = isset($_POST['id']) ? $_POST['id'] : NULL;
        $libelle = isset($_POST['libelle']) ? $_POST['libelle'] : '';
        $desc = isset($_POST['desc']) ? $_POST['desc'] : '';
        $etat = isset($_POST['etat']) ? $_POST['etat'] : '';
        $categorie= isset($_POST['categorie']) ? $_POST['categorie'] : '';
        if(isset($_FILES['photo']) && $_FILES['photo']!=""){
       $img= basename($_FILES['photo']['name']);
      
        } else {
            $img=$imgAncien;
                   

        }
               // Update the record
        $stmt = $pdo->prepare('UPDATE produits SET PR_ID = ?,PR_LIBELLE = ?,PR_ETAT = ?,PR_PHOTO = ?,CA_ID = ?,PR_DETAIL = ? WHERE PR_ID = ?');
        if($img){
             $stmt->execute([$_GET['id'],$libelle,$etat,$img,$categorie,$desc, $_GET['id']]);
        }elseif ($imgAncien) {
             $stmt->execute([$_GET['id'],$libelle,$etat,$imgAncien,$categorie,$desc, $_GET['id']]);
        } else {
             $stmt->execute([$_GET['id'],$libelle,$etat,'',$categorie,$desc, $_GET['id']]);
        }
       
        $msg = 'Mise à jour faite avec succès';
    }
    // Get the contact from the contacts table
   
    if($produit){
        $stmtcat = $pdo->prepare('SELECT * FROM categorie WHERE CA_ID = ?');
        $stmtcat->execute([$produit['CA_ID']]);
        $categorieDefault=$stmtcat->fetch(PDO::FETCH_ASSOC);
    }
    if (!$produit) {
        exit('le produit n\'existe pas');
    }
} else {
    exit('Identifiant n\'est pas specifié');
}
?>
<?=template_header('Modification_produit')?>

<div class="content update">
	<h2>Modification de #<?=$produit['PR_LIBELLE']?></h2>
    
    <form action="update.php?id=<?=$produit['PR_ID']?>" method="post" enctype="multipart/form-data">
       <div class="ligne">
     <p class="premier">
        <label for="libelle">Libelle</label>
        <input type="text" name="libelle" placeholder="mil" value="<?=$produit['PR_LIBELLE']?>" id="libelle">
         <label>Description : </label></br>
         <textarea id="textDescr" name="desc" cols="30" rows="8" ><?=$produit['PR_DETAIL']?></textarea></br></br>
        <label for="categorie">Categorie : </label><br/>
            <select name="categorie" id="categorie"><br/><br/>
                <option value="<?php echo $categorieDefault['CA_ID']; ?>" selected="selected" > <?php echo $categorieDefault['CA_LIBELLE']; ?></option>
 
<?php
$reponse = $pdo->query('SELECT * FROM categorie where CA_ID !='.$categorieDefault['CA_ID']);
while ($donnees = $reponse->fetch())
{
?>
           <option value="<?php echo $donnees['CA_ID']; ?>"> <?php echo $donnees['CA_LIBELLE']; ?></option><br/>
<?php
}
 
?>
</select><br/>
         <label>Charger une image : </label><br/>
         <input type="file" name="photo" id="photo"/></br></br>
        <label >Activer</label>
        <input style="margin-left: -28%;" type="radio" name="etat" value="Y" id="Y" checked="checked"/>
        <label >Desactiver</label>
         <input style="margin-left: -27%;" type="radio" name="etat" value="N" id="N" />
            <!--</form>-->
      
<!--            <input type="submit" value="Modifier" ><a href="liste.php">
                <input type="button" value="Annuler"></a>-->
      
        </p></div>
        
     <input type="submit" value="Modifier" >
    <a href="liste.php">
                <input type="button" value="Annuler"></a>

    </form>
     
    <?php if ($msg): ?>
    <p><?=$msg?></p>
    <?php endif; ?>
</div>

<?=template_footer()?>