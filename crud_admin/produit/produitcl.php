<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of produitcl
 *
 * @author fakaloga
 */
class produitcl {
        private $libelle="";
       private $detail="";
       private $fichier="";
    public function __construct(){
     
    $num=func_num_args();
    $args=func_get_args();
    switch ($num) {
    case 0:
        $this->constructeurSansArgument();
        break;
     case 3:
        $this->constructeurAvecArgument($args[0],$args[1],$args[2]);
        break;

    default:
        break;
}

}
 private function constructeurSansArgument() {
    
}
    
    private function constructeurAvecArgument($libelle,$detail,$fichier) {
     $this-> libelle=$libelle;
     $this-> detail=$detail;
     $this-> fichier=$fichier;
}
    function getLibelle() {
        return $this->libelle;
    }

    function getDetail() {
        return $this->detail;
    }

    function getFichier() {
        return $this->fichier;
    }

    function setLibelle($libelle): void {
        $this->libelle = $libelle;
    }

    function setDetail($detail): void {
        $this->detail = $detail;
    }

    function setFichier($fichier): void {
        $this->fichier = $fichier;
    }

 
}
