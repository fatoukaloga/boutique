<?php
include '../admin/fonction_include.php';
$pdo = pdo_connect_mysql();
$msg = '';
// Check that the contact ID exists
if (isset($_GET['id'])) {
    // Select the record that is going to be deleted
    $stmt = $pdo->prepare('SELECT * FROM produits pr WHERE pr.PR_ID = ?');
    $stmt->execute([$_GET['id']]);
    $produit1 = $stmt->fetch(PDO::FETCH_ASSOC);
    if (!$produit1) {
        exit('le produit n\'existe pas!');
    }
    // Make sure the user confirms beore deletion
    if (isset($_GET['confirm'])) {
        if ($_GET['confirm'] == 'yes') {
            $stmtV = $pdo->prepare('SELECT * FROM produits pr,boutique_produits bp WHERE pr.PR_ID=bp.PR_ID and '
                    . 'pr.PR_ID = ?');
         $stmtV->execute([$_GET['id']]);
         $produitV = $stmtV->fetch(PDO::FETCH_ASSOC);
         if($produitV){
             $msg = 'Produit à supprimer est lié à une boutique!';
         } else {
              $stmt = $pdo->prepare('DELETE FROM produits WHERE PR_ID = ?');
            $stmt->execute([$_GET['id']]);
            $msg = 'Produit supprimé avec succès!';
         }
            // User clicked the "Yes" button, delete record
           
        } else {
            // User clicked the "No" button, redirect them back to the read page
            header('produit: liste.php');
            exit;
        }
    }
}

else {
    exit('identifiant non specifique!');
}
?>
<?=template_header('Suppression produit')?>
<div class="content delete">
	<h2>Suppression  #<?=$produit1['PR_LIBELLE']?>#</h2>
    <?php if ($msg): ?>
    <p><?=$msg?></p>
    <?php else: ?>
	<p>Etes vous sur de vouloir supprimer <?=$produit1['PR_LIBELLE']?>?</p>
    <div class="yesno">
        <a href="delete.php?id=<?=$produit1['PR_ID']?>&confirm=yes">Yes</a>
        <a href="liste.php">Non</a>
    </div>
    <?php endif; ?>
</div>

<?=template_footer()?>