<?php
/* LA LISTE DE TOUS LES PRODUITS DANS UN TABLEAUX AVEC 
 * PRIX EN GROS ET DETAIL?STOCK?LISTE DES LIVREURS,VENDEUR
 */
//include '../../db.php';
// Connect to MySQL database
$pdo = pdo_connect_mysql();
if (isset($_GET['id'])) {
     $stmt1 = $pdo->prepare('SELECT * FROM boutique bo,localite lo WHERE bo.LO_ID=lo.LO_ID and bo.BO_ID = ?');
     $stmt1->execute([$_GET['id']]);
 //$stmt1->execute();
  $boutiqueselect = $stmt1->fetch(PDO::FETCH_ASSOC);
  $localitebo=$boutiqueselect['LO_LIBELLE'];
// Get the page via GET request (URL param: page), if non exists default the page to 1
$page = isset($_GET['page']) && is_numeric($_GET['page']) ? (int)$_GET['page'] : 1;
// Number of records to show on each page
$records_per_page = 15;
// Prepare the SQL statement and get records from our contacts table, LIMIT will determine the page
 $stmt = $pdo->prepare('SELECT * FROM boutique_produits bp,produits pr,boutique bo,localite lo WHERE bo.BO_ID=bp.BO_ID and bo.LO_ID=lo.LO_ID and bp.PR_ID=pr.PR_ID AND'
             . ' pr.PR_ETAT="Y" AND bo.BO_ID=:id ORDER BY bp.BP_ID LIMIT :current_page, :record_per_page');
$stmt->bindValue(':id', ($_GET['id']));
$stmt->bindValue(':current_page', ($page-1)*$records_per_page, PDO::PARAM_INT);
$stmt->bindValue(':record_per_page', $records_per_page, PDO::PARAM_INT);
//     $stmt->execute(array('id'=>($_GET['id'])));

 $stmt->execute();
  $produits = $stmt->fetchAll(PDO::FETCH_ASSOC);
  $num_produits=0;
 if($produits){
// Get the total number of contacts, this is so we can determine whether there should be a next and previous button
$num_produits = count($produits);
//echo $num_produits;
 } else {
//     echo 'pas delement';
 }
 
$stmt1 = $pdo->prepare('SELECT * FROM livreur_vendeur lv,boutique bo WHERE bo.BO_ID=lv.BO_ID AND'
             . ' lv.LV_ETAT="Y" AND lv.BO_ID=:id ORDER BY lv.LV_ID LIMIT :current_page, :record_per_page');
$stmt1->bindValue(':id', ($_GET['id']));
$stmt1->bindValue(':current_page', ($page-1)*$records_per_page, PDO::PARAM_INT);
$stmt1->bindValue(':record_per_page', $records_per_page, PDO::PARAM_INT);
//     $stmt->execute(array('id'=>($_GET['id'])));

 $stmt1->execute();
  $contactsbo = $stmt1->fetchAll(PDO::FETCH_ASSOC);
  $num_contactsbo=0;
}
?>


<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>mesDenrées</title>
                <link href="../../css/style_test.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
		
	
   </head>
	<body>
 

  
<div class="content read">
    <h2 style="color: rgb(255,102,0);"><?=$boutiqueselect['BO_LIBELLE']?>  "<?=$localitebo?>"</h2>
<!--	<a href="create.php" class="create-contact">Ajouter utilisateur</a>-->
<div style="overflow-x:auto;">
    <?php if($produits){ ?>
	<table>
        <thead>
             
            <tr style="">
                <td>Nom</td>
                <td>PRIX</td>
                <td>Unité</td>
                <td>Prix en Gros</td>
                <td>Prix en Detail</td>
                <td>Stock</td>
                <td>PHOTO</td>
                <!--<td>Action</td>-->
            </tr>
        </thead>
        <tbody style="overflow-x:hidden;overflow-y: auto;height: 300px;">
           
            <?php foreach ($produits as $produit): ?>
            <tr>
                <td><a style="color: green;font-style: unset;font-family: cursive;text-transform: uppercase;" 
                       href="descriptionproduitindex.php?id=<?=$produit['BP_ID']?>?idpr=<?=$produit['PR_ID']?>"><?=$produit['PR_LIBELLE']?></a></td>
                <td><?=$produit['PR_PRIX']?></td>
                <td><?=$produit['PR_UNITE']?></td>
                <td><?=$produit['PR_PRIXGROS']?> FCFA</td>
                <td><?=$produit['PR_PRIXDETAIL']?> FCFA</td>
                <td><?=$produit['PR_QUANTITE'].$produit['PR_UNITE']?></td>
                <td><div class="img" style="background-image:url('../../images/mesDenreesImg/<?=$produit['PR_PHOTO']?>');"></div>
</td>
<!--                <td class="actions">
                    <a href="update.php?id=<?=$contact['US_ID']?>" class="edit"><i class="fas fa-pen fa-xs"></i></a>
                    <a href="delete.php?id=<?=$contact['US_ID']?>" class="trash"><i class="fas fa-trash fa-xs"></i></a>
                </td>-->
            </tr>
            <?php endforeach; ?>
           </tbody>
    </table>
    <?php } if(!$produits){?>

<p>Pas d'élément enregistré</p>
<?php
}?>
	<div class="pagination">
		<?php if ($page > 1): ?>
            <a href="boutique.php?page=<?=$page-1?>" 
   ><i class="fas fa-angle-double-left fa-sm"></i></a>
		<?php endif; ?>
		<?php if (!$num_produits and $page*$records_per_page < $num_produits): ?>
                <a href="boutique.php?page=<?=$page+1?>"><i class="fas fa-angle-double-right fa-sm"></i></a>
		<?php endif; ?>
	</div>
</div>
<h2 style="color: rgb(255,102,0);">Livreurs et Vendeurs</h2>
<div style="overflow-x:auto;">
	<table>
        <thead>
            <tr>
                <td>Nom</td>
                <td>Prénom</td>
                <td>Contact</td>
                <td></td>
                
                <!--<td>Action</td>-->
            </tr>
        </thead>
        <tbody style="overflow-x:hidden;overflow-y: auto;">
            <?php if($contactsbo){ ?>
            <?php foreach ($contactsbo as $cnt): ?>
            <tr>
                <td><?=$cnt['LV_NOM']?></td>
                <td><?=$cnt['LV_PRENOM']?></td>
                <td><?=$cnt['LV_TELEPHONE']?></td>
                <td><?php if(strcmp($cnt['LV_TYPE'],'L')==0){?>
                    <p>Livreur</p>
                <?php }else{ ?>
                    <p>Vendeur</p>
<?php } ?>
                </td>
                              
<!--                <td class="actions">
                    <a href="update.php?id=<?=$contact['US_ID']?>" class="edit"><i class="fas fa-pen fa-xs"></i></a>
                    <a href="delete.php?id=<?=$contact['US_ID']?>" class="trash"><i class="fas fa-trash fa-xs"></i></a>
                </td>-->
            </tr>
            <?php endforeach; ?>
            <?php } if(!$contactsbo){?>

<p>Pas de livreurs ni vendeurs enregistrés</p>
<?php
}?>
        </tbody>
    </table>
	
</div>
</div>
  </body>
</html>