<?php
/* DESCRIPTION DU PRODUIT SELECTIONNé
 */

$pdo = pdo_connect_mysql();
if (isset($_GET['id'])) {
// Get the page via GET request (URL param: page), if non exists default the page to 1
$page = isset($_GET['page']) && is_numeric($_GET['page']) ? (int)$_GET['page'] : 1;
// Number of records to show on each page
$records_per_page = 5;
// Prepare the SQL statement and get records from our contacts table, LIMIT will determine the page
 $stmt = $pdo->prepare('SELECT * FROM boutique_produits bp,produits pr,boutique bo WHERE bo.BO_ID=bp.BO_ID and bp.PR_ID=pr.PR_ID AND'
             . ' pr.PR_ETAT="Y" AND bp.BP_ID=:id ORDER BY bp.BP_ID LIMIT :current_page, :record_per_page');
$stmt->bindValue(':id', ($_GET['id']));
$stmt->bindValue(':current_page', ($page-1)*$records_per_page, PDO::PARAM_INT);
$stmt->bindValue(':record_per_page', $records_per_page, PDO::PARAM_INT);
//     $stmt->execute(array('id'=>($_GET['id'])));

 $stmt->execute();
  $produits = $stmt->fetchAll(PDO::FETCH_ASSOC);
  $num_produits=0;
 if($produits){
// Get the total number of contacts, this is so we can determine whether there should be a next and previous button
$num_produits = count($produits);
//echo $num_produits;
 } 
}
?>


<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>mesDenrées</title>
                <link href="../../css/style_test.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
		
	
   </head>
	<body>
 

  
<div class="content read">
    <?php foreach ($produits as $produit): ?>
    <h2 style="color: rgb(0,0,0);">Detail du produit: <?=$produit['PR_LIBELLE']?></h2>
<!--	<a href="create.php" class="create-contact">Ajouter utilisateur</a>-->
<div style="overflow-x:auto;">
            <table>
        <thead>
            <tr>
                <td></td>
                <td>Description</td>
                <td>Stock</td>
            </tr>
        </thead>
        <tbody>
            
            <tr>
                <td><div class="img" style="background-image:url('../../images/mesDenreesImg/<?=$produit['PR_PHOTO']?>');"></div></td>
                <td><p style="font-size: 18px;"><?=$produit['PR_DETAIL']?><br>
                     Disponible: <a style="font-weight: bold;color: rgb(145,100,205);" href="boutiqueindex.php?id=<?=$produit['BO_ID']?>"><?=$produit['BO_LIBELLE']?></a><br>
                     Au prix de: <?=$produit['PR_PRIX']?>/<?=$produit['PR_UNITE']?></p>
                    <?php if($produit['PR_PRIXGROS']>0){?>
                    <h6>Prix EN GROS  :</h6>  <?=$produit['PR_PRIXGROS']?> FCFA<br>
                    <?php }?>
                    <?php if($produit['PR_PRIXDETAIL']>0){?>
                    <h6>Prix DETAIL  :</h6> <?=$produit['PR_PRIXDETAIL']?> FCFA<br>
                    <?php }?>
                </td>
                    <?php if($produit['PR_QUANTITE']>0){?>
                <td style="color: rgb(255,102,0);color: green;font-weight: bold;"> Disponible
                </td>
                <?php } else{?>
                <td style="color: rgb(255,102,0); color: red;font-weight: bold;"> Non Disponible</td>
                <?php } ?>
   
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
        </div>
</div>
  </body>
</html>