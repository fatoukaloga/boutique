<?php
    include '../../crud_admin/admin/fonction_include.php';
    $pdo = pdo_connect_mysql();
    if (isset($_GET['id'])) {
//        echo $_GET['id'];
     $stmt1 = $pdo->prepare('SELECT * FROM boutique bo,localite lo WHERE bo.LO_ID=lo.LO_ID and bo.BO_ID = ?');
     $stmt1->execute([$_GET['id']]);
 //$stmt1->execute();
    $boutiqueselect = $stmt1->fetch(PDO::FETCH_ASSOC);
    $l=$boutiqueselect['BO_LATITUDE'];
    $lO=$boutiqueselect['BO_LONGITUDE'];
    $nom=$boutiqueselect['BO_LIBELLE'];
    
    }
 

// On retourne le tableau à la fonction appelante
//    echo $l.'///'.$lO;
    ?>
<!DOCTYPE html>
<html>
  <head>
    <title>MesDenrées</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
   
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDqM7WhknEvTCXc367gOU5Q5IKf8c1jcdk&callback=initMap"
    async defer>
    </script>
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
     <link rel="icon" 
      type="image/png" 
      href="../../images/mesDenreesImg/logo_1_orange.png">
  </head>
  <body>
    <div id="map"></div>
    <script>
      function initMap() {
//          var lat1=12.672847132210315;
//          var lon1=-7.897208807997309;
          //Ce bout de code permet de trier les cookies pour les stocker dans un tableau associatif.
       
        //Récupération de la variable dans le tableau
        var lat = <?php echo json_encode($l); ?>;
        var lon = <?php echo json_encode($lO); ?>;
	map = new google.maps.Map(document.getElementById("map"), {
                center: new google.maps.LatLng(lat, lon),
		zoom: 11,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		mapTypeControl: true,
		scrollwheel: false,
		mapTypeControlOptions: {
			style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
		},
		navigationControl: true,
		navigationControlOptions: {
			style: google.maps.NavigationControlStyle.ZOOM_PAN
		}
	});
    latlng = new google.maps.LatLng(lat, lon);
    marker = new google.maps.Marker({
    position: latlng,
    map: map,
    zoom:8,
    title:" " + <?php echo json_encode($nom); ?>
   
});
        
        
      
}

    
    
//    var map;
//      function initMap() {
//        map = new google.maps.Map(document.getElementById('map'), {
//          center: {lat: 12.672847132210315, lng: -7.897208807997309},
//          zoom: 8
//        });
//      }
    </script>
    
    
    
    
    
   </body>
</html>