<?php
//include '../../db.php';
// Connect to MySQL database
$pdo = pdo_connect_mysql();
if (isset($_GET['id'])) {
// Get the page via GET request (URL param: page), if non exists default the page to 1
$page = isset($_GET['page']) && is_numeric($_GET['page']) ? (int)$_GET['page'] : 1;
// Number of records to show on each page
$records_per_page = 5;
$Code='C1';
// Prepare the SQL statement and get records from our contacts table, LIMIT will determine the page
// $stmt1 = $pdo->query('SELECT * FROM localite WHERE LO_CODE = "C1"');
     $stmt1 = $pdo->prepare('SELECT * FROM localite WHERE LO_ID = ?');
     $stmt1->execute([$_GET['id']]);
 //$stmt1->execute();
  $localiteselect = $stmt1->fetch(PDO::FETCH_ASSOC);
 if($localiteselect){

$stmt = $pdo->prepare('SELECT * FROM boutique WHERE BO_ETAT="Y" AND BO_TYPE=1 AND LO_ID= ?');
$stmt->execute([$localiteselect['LO_ID']]);
// Fetch the records so we can display them in our template.
$boutiques = $stmt->fetchAll(PDO::FETCH_ASSOC);

$stmt0 = $pdo->prepare('SELECT * FROM boutique WHERE BO_ETAT="Y" AND BO_TYPE=2 AND LO_ID= ?');
$stmt0->execute([$localiteselect['LO_ID']]);
// Fetch the records so we can display them in our template.
$marches = $stmt0->fetchAll(PDO::FETCH_ASSOC);

// Get the total number of contacts, this is so we can determine whether there should be a next and previous button
$num_boutiques = $pdo->query('SELECT COUNT(*) FROM boutique')->fetchColumn();

$stmt3 = $pdo->prepare('SELECT * FROM localite WHERE LO_ETAT="Y" AND LO_PARENT= ?');
$stmt3->execute([$localiteselect['LO_ID']]);
// Fetch the records so we can display them in our template.
$communes = $stmt3->fetchAll(PDO::FETCH_ASSOC);
 }


?>
<h2 class="h2style">Marches de <?=$localiteselect['LO_LIBELLE'] ?></h2>
<?php if ($marches){ ?>
<?php foreach ($marches as $boutique): ?>

<div style="">
    <h3><a href="../boutique_marche/boutiqueindex.php?id=<?=$boutique['BO_ID']?>" class="boutiquestyle"><?=$boutique['BO_LIBELLE']?></a></h3>
<p>Quartier: <?=$boutique['BO_QUARTIER']?></p>
<p><i class='fa fa-mobile' style='font-size:35px;margin-right: 4px;'></i>Téléphone: <?=$boutique['BO_NUMERO_TELEPHONE']?></p>
<p><i class='fa fa-map-signs' style='font-size:26px;margin-right: 4px;color:red;'></i><a href="../boutique_marche/localiser.php?id=<?=$boutique['BO_ID']?>" target="true">Localiser</a></p>

</div>
<?php endforeach;
}?>
<?php 
} if(!$marches){?>

<p>Pas de marchés enregistrés</p>
<?php
}?>

<h2 class="h2style">Boutique de <?=$localiteselect['LO_LIBELLE'] ?></h2>
<?php if ($boutiques){ ?>
<?php foreach ($boutiques as $boutique): ?>

<div style="">
    <h3><a href="../boutique_marche/boutiqueindex.php?id=<?=$boutique['BO_ID']?>" class="boutiquestyle"><?=$boutique['BO_LIBELLE']?></a></h3>
<p>Quartier: <?=$boutique['BO_QUARTIER']?></p>
<p><i class='fa fa-mobile' style='font-size:35px;margin-right: 4px;'></i>Téléphone: <?=$boutique['BO_NUMERO_TELEPHONE']?></p>
<p><i class='fa fa-map-signs' style='font-size:26px;margin-right: 4px;color:red;'></i><a href="../boutique_marche/localiser.php?id=<?=$boutique['BO_ID']?>" target="true">Localiser</a></p>

</div>
<?php endforeach;
}?>
<?php 
 if(!$boutiques){?>

<p>Pas de boutiques enregistrées</p>
<?php
}?>

<h2 class="h2style">Communes-Quartiers </h2>
<?php if ($communes){ ?>
<?php foreach ($communes as $commune): ?>
<div class="boutique">
    <h3><a href="../communes/communeindex.php?id=<?=$commune['LO_ID']?>" style="margin-left: 3%;"><?=$commune['LO_LIBELLE']?></a></h3>
</div>
<?php endforeach;
}?>
<?php 
 if(!$communes){?>
<p>Pas d'élément enregistré</p>
<?php
}?>