<?php
//include './db.php';
// Connect to MySQL database
$pdo = pdo_connect_mysql();
// Get the page via GET request (URL param: page), if non exists default the page to 1
$page = isset($_GET['page']) && is_numeric($_GET['page']) ? (int)$_GET['page'] : 1;
// Number of records to show on each page
$records_per_page = 5;
// Prepare the SQL statement and get records from our contacts table, LIMIT will determine the page
$stmt = $pdo->prepare('SELECT * FROM localite WHERE LO_ETAT=\'Y\' and LO_PARENT=0 ORDER BY LO_ID');
$stmt->execute();
// Fetch the records so we can display them in our template.
$localites = $stmt->fetchAll(PDO::FETCH_ASSOC);
// Get the total number of contacts, this is so we can determine whether there should be a next and previous button
$num_localites = $pdo->query('SELECT COUNT(*) FROM localite')->fetchColumn();

?>