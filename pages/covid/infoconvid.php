<body class="html not-front not-logged-in no-sidebars page-node node-type-crise sec- page-node-crise page-node-10080 jquery-once-1-processed jquery-once-2-processed">
 
<!-- #cheader-top -->
<div id="cheader-top">
  <div class="cheader-top"><p>
    COVID-19  </p></div>
  </div>

<div id="ctitle-top">
    <h1><span>informations</span>   CORONAVIRUS</h1></div>
<!-- #cpoint-->
<a id="point" name="point"></a>
<div id="cpoint">
  <div class="c-titre">
    <h2>Point de situation</h2>    <p>#RestezChezVous</p>  </div>
  <div class="c-texte">
      <p>Une maladie inconnue est apparue en Chine, au mois de décembre. 
      Depuis, elle s’est répandue dans le monde entier. 
      Cette maladie est due à un nouveau microbe : le coronavirus Covid-19.
      Qui est-il ? Et comment se transmet-il ?

.<br>
<br>
Pour en savoir plus veuillez suivre:<br> 
<a href="https://www.orangemali.com/fr/informations-covid-19.html" target="true"><strong style="color: rgb(255,102,0);">->Orange mali Info COVID-19</strong></a> <br>  
<a href="  http://covid.nanosoftci.net/" target="true"><strong style="color: rgb(145,100,205);">->Info COVID-19</strong></a>
      <br>
<a href="https://www.who.int/fr"target="true"><strong style="color: rgb(145,100,205);">->OMS COVID-19</strong></a></p><br> 

   </div>



<!-- // #cpoint-->

<!-- #cinformations-->
<a id="informations" name="informations"></a>
<div id="cinformations" class="cinfo-block">
  <div class="c-header">
     <h2>Sinon nous vous proposons quelques informations essentielles</h2>  </div>

  <div class="c-images">
      
    <div class="cinformations-item-image">
  <h3>
      Quels sont les signes ?  </h3>
<div>
    <img src="../../images/mesDenreesImg/symptomes.png" alt="">  </div>
</div>
    <div class="cinformations-item-image">
  <h3>
      Comment se transmet-il ?  </h3>
<div>
    <img src="../../images/mesDenreesImg/transmission_moins_1_metre.png" alt="1 - Face à face pendant au moins 15 minutes, 2 - Par la projection de gouttelettes">  </div>
</div>
  
  </div>


    <div id="fb-root"></div>
 
  <div id="block-block-10" class="block block-block block-even">

    
  <div class="block-content">
        <style>
    .node-crise a {
        color: #000091;
        font-weight: 700;
        text-decoration: underline;
        text-underline-offset: 2px;
        text-decoration-thickness: 2px;
    }
    .node-crise a:hover{
        text-decoration: none;
    }
    .node-crise a span {
        font-weight: 400;
    }

    .node-crise h3 {
        font-size: 2em;
    }

    .node-crise h4 {
        font-size: 1.5em;
    }

    .node-crise u {
        text-underline-offset: 2px;
        text-decoration-thickness: 2px;
    }

    .node-crise ul {
        margin-top: 1.5em;
        margin-bottom: 2em;
        padding-left: 1.5em;
    }

    .node-crise li {
        line-height: 1.5em;
        list-style-type: disc !important;
    }

    #main {
        background-color: #F9F8F6;
        font-family: 'Marianne';
        font-size: 16px;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }

    #content  {
        padding: 0;
    }

    /* panels */
    .cinfo-block {
        background-color: #FFFFFF;
        box-shadow: 0px 4px 12px rgba(0, 0, 0, 0.15);
/*        margin-bottom: 3em;
        padding: 0 5em 3em;*/
    }

    .cinfo-block h2 {
        background-color: rgb(75,180,230) !important;
        color: #FFFFFF !important;
        font-size: 1.25em;
        padding: 0.5em 1.25em;
        padding-left: 5em;
    }

    .cinfo-block h3 {
        color: #000091;
        font-size: 1.75em;
        margin: 1em 0;
    }

    .cinfo-block > .c-header {
        margin: 0 -5em;
        text-transform: uppercase;
    }

    .c-links ul {
        padding: 0;
    }

    .tab-buttons {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
        margin-top: 2em;
    }

    .tab-buttons ~ .tab-buttons {
        display: none;
    }

    .active ~ .tab-buttons {
        margin-top: 2em;
        display: flex;
    }

    .tab-button {
        background-color: #FFFFFF !important;
        background-position: left center;
        background-repeat: no-repeat;
        background-size: 1.875em;
        border: 0;
        box-shadow: inset 0 -1px 0 0 #F0F0F0;
        color: #383838;
        cursor: pointer;
        display: block;
        flex-basis: 48%;
        font-family: 'Marianne';
        font-size: 1em;
        font-weight: 700;
        padding: 1em;
        text-align: left;
    }

    .tab-button:hover,
    .tab-button.active:hover {
        background-color: #F0F0F0 !important;
    }

    .tab-button.active {
        background-position: left center;
        background-repeat: no-repeat;
        background-size: 1.875em;
        box-shadow: inset 0 -4px 0 0 #000091;
        color: #000091;
    }

    /* accordeons */
    .items-qr {
        margin-top: 2em;
    }

    .item-qr {
        border-bottom: 1px solid #F0F0F0;
    }

    h4.item-question {
        color: #000091;
        cursor: pointer;
        font-size: 1em;
        line-height: 1.75em;
        padding: 0.5em 2em 0.5em 1em;
        position: relative;
        z-index: 1;
    }

    h4.item-question:hover,
    h4.item-question.active {
        background-color: #F0F0F0;
    }

    .item-question::before {
        content: '';
        display: block;
        width: 100%;
        height: 100%;
        position: absolute;
        right: 0;
        top: 0;
        z-index: 0;
        background: url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTYiIGhlaWdodD0iMTYiIHZpZXdCb3g9IjAgMCAxNiAxNiIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZD0iTTcuNzUgMEw3Ljc1IDE0IiBzdHJva2U9IiMwMDAwOTEiIHN0cm9rZS13aWR0aD0iMiIvPgo8cGF0aCBkPSJNMTQuNSA3LjVMNy43MzQxMSAxNC41TDEuNSA3LjUiIHN0cm9rZT0iIzAwMDA5MSIgc3Ryb2tlLXdpZHRoPSIyIi8+Cjwvc3ZnPgo=) right 0.5em center no-repeat;
    }

    .item-question.active::before {
        background-image: url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTYiIGhlaWdodD0iMTYiIHZpZXdCb3g9IjAgMCAxNiAxNiIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZD0iTTcuNjk0NTQgMTUuNUw3LjY5NDU0IDEuNSIgc3Ryb2tlPSIjMDAwMDkxIiBzdHJva2Utd2lkdGg9IjIiLz4KPHBhdGggZD0iTTE0LjUgOC41TDcuNzM0MTEgMS41TDEuNSA4LjUiIHN0cm9rZT0iIzAwMDA5MSIgc3Ryb2tlLXdpZHRoPSIyIi8+Cjwvc3ZnPgo=);
    }

    .item-depliant {
        display: none;
        height: 0;
        line-height: 1.75em;
        opacity: 0;
    }


    .item-question.active + .item-depliant {
        display: flex;
        opacity: 1;
        height: auto;
        padding: 0 1em;
        transition: opacity 0.5s ease-in;
    }

    .item-question.active + .item-depliant,
    .item-question + .item-reponse {
        margin: 1.5em 0;
    }

    .item-reponse ul {
        margin-bottom: 0;
    }

    #cmesures .c-qr-titre {
        margin-top: 2em;
    }

    #cmesures .item-depliant {
        flex-wrap: wrap;
        justify-content: space-between;
    }

    #cmesures .item-reponse,
    #cmesures .item-autre {
        width: 48%;
    }

    #cmesures .items-paragraphe .item-reponse {
        width: 100%;
    }

    #cmesures .item-question {
        order: 1;
        height: 100%;
        flex-shrink: 0;
    }

    #cmesures .item-reponse, #cprotection .item-reponse {
        order: 3;
    }

    .item-surtitre {
        order: 2;
        color: #000091;
        height: 100%;
        margin-bottom: 1em;
        flex-shrink: 0;
        width: 100%;
    }

    #cmesures .item-autre, #cprotection .item-autre{
        order: 4;
        color: #E1000F;
    }

    #cmesures .item-autre p:first-child {
        font-weight: 700;
    } 

    #cmesures .item-autre p:first-child::before {
        content: '';
        display: inline-block;
        height: 3em;
        width: 3em;
        margin-right: 1em;
        vertical-align: middle;
        background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+Cjxzdmcgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDQxIDQxIiB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbDpzcGFjZT0icHJlc2VydmUiIHhtbG5zOnNlcmlmPSJodHRwOi8vd3d3LnNlcmlmLmNvbS8iIHN0eWxlPSJmaWxsLXJ1bGU6ZXZlbm9kZDtjbGlwLXJ1bGU6ZXZlbm9kZDsiPgogICAgPHBhdGggZD0iTTQwLjUsMjAuNUM0MC41LDMxLjU0NiAzMS41NDYsNDAuNSAyMC41LDQwLjVDOS40NTQsNDAuNSAwLjUsMzEuNTQ2IDAuNSwyMC41QzAuNSw5LjQ1NCA5LjQ1NCwwLjUgMjAuNSwwLjVDMzEuNTQ2LDAuNSA0MC41LDkuNDU0IDQwLjUsMjAuNVoiIHN0eWxlPSJmaWxsOnJnYigyMjUsMCwxNSk7ZmlsbC1vcGFjaXR5OjAuMjtmaWxsLXJ1bGU6bm9uemVybztzdHJva2U6cmdiKDIyNSwwLDE1KTtzdHJva2Utd2lkdGg6MXB4OyIvPgogICAgPGcgdHJhbnNmb3JtPSJtYXRyaXgoMSwwLDAsMSw4LDE3KSI+CiAgICAgICAgPHJlY3QgeD0iMC41IiB5PSIwLjUiIHdpZHRoPSIyNCIgaGVpZ2h0PSI2IiBzdHlsZT0iZmlsbDp3aGl0ZTtzdHJva2U6cmdiKDIyNSwwLDE1KTtzdHJva2Utd2lkdGg6MXB4OyIvPgogICAgPC9nPgo8L3N2Zz4K) center center no-repeat;
    }

    .c-intro {
        border-top: 2px solid rgba(0, 0, 145, 0.2);
        font-size: 1.125em;
        margin-top: 2em;
        padding-top: 2em;
    }

    /* underlined titles */
    .item-paragraphe-titre,
    #cpoint .c-titre h2,
    .c-qr-titre h3 {
        background: 0;
       background-image: linear-gradient(to bottom, transparent 60%, rgb(255,102,0) 60%);
        color: #E1000F !important;
        display: inline;
        font-size: 1.75em;
        font-weight: 400;
        letter-spacing: 0.1em;
        line-height: 1.5em;
        position: relative;
        text-transform: uppercase;
        z-index: 0;
    }


    @supports (text-underline-offset: -0.25em) {
        .item-paragraphe-titre,
        #cpoint .c-titre h2,
        .c-qr-titre h3 {
            background: 0;
            text-decoration: underline;
            text-decoration-color: #F9CCCF;
            text-decoration-thickness: 0.5em;
            text-underline-offset: -0.25em;
            text-decoration-skip-ink: none;
        }
    }

    /* buttons */
    .button-cta,
    .cheader-buttons .button-cta,
    .boutons-partage .bouton-partage {
        background-color: #FFFFFF;
        border-color: #000091;
        color: #000091;
        font-family: 'Marianne';
        padding: 0.75em 4.5em 0.75em 1.5em;
        top: 2px;
        transform: translate(-2px, -2px);
    }

    .boutons-partage .bouton-partage {
        border: 1px solid #000091;
        border-radius: 0;
        color: #000091 !important;
        display: inline-flex;
        position: relative;
        overflow: visible;
        text-decoration: none;
    }

    .button-cta.active {
        background: #000091;
        color: #FFFFFF;
    }

    .button-cta::before,
    .button-cta::after,
    .cheader-buttons .button-cta.red::after {
        border-color: #000091;
    }

    .button-cta::before,
    .boutons-partage .bouton-partage::before {
        border: 1px solid #000091;
        content: '';
        left: auto;
        right: -1px;
        height: 100%;
        width: 41px;
        position: absolute;
        top: -1px;
        display: block;
        background: #FFFFFF url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjEiIGhlaWdodD0iMTgiIHZpZXdCb3g9IjAgMCAyMSAxOCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik0xMi4wNzUyIDE2LjVMOS43NTY3NyAxNC4xNThMMTMuMjI0NSAxMC42NTUySDAuNUwwLjUgNy4zNDMzOUwxMy4yMjQ1IDcuMzQzMzlMOS43NTY3NyAzLjg0MDUyTDEyLjA3NTIgMS41TDE5LjUgOUwxMi4wNzUyIDE2LjVaIiBzdHJva2U9IiMwMDAwOTEiLz4KPC9zdmc+Cg==) right 0.75em center no-repeat;
    }

    .button-cta::after,
    .boutons-partage .bouton-partage::after {
        border-bottom: 1px solid #000091;
        border-right: 1px solid #000091;
        content: '';
        display: inline-flex;
        height: 100%;
        right: -3px;
        top: 2px;
        position: absolute;
        width: 100%;
    }

    .button-cta:hover,
    .boutons-partage .bouton-partage:hover {
        transform: translate(0px, 0px);
    }

    .boutons-partage .bouton-partage:hover {
        background-color: transparent !important;
    }

    .button-cta:hover::before,
    .boutons-partage .bouton-partage:hover::before {
        bottom: 0;
        height: 100%;
        left: initial;
        top: -1px;
    }

    .button-cta:hover::after,
    .boutons-partage .bouton-partage:hover::after {
        top: 1px;
    }

    .boutons-partage .bouton-partage:hover::after {
        right: -2px;
    }

    .boutons-partage .bouton-partage-twitter::before {
        background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDIyLjEuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPgo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkNhbHF1ZV8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIKCSB3aWR0aD0iNDBweCIgaGVpZ2h0PSI0MHB4IiB2aWV3Qm94PSIwIDAgNDAgNDAiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDQwIDQwOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+Cgkuc3Qwe2ZpbGw6IzAwMDA5MTt9Cgkuc3Qxe2ZpbGw6bm9uZTtzdHJva2U6IzAwMDA5MTtzdHJva2UtbWl0ZXJsaW1pdDoxMDt9Cjwvc3R5bGU+CjxwYXRoIGNsYXNzPSJzdDAiIGQ9Ik0xNS40LDMwLjJjLTIuOSwwLTUuNi0wLjgtNy45LTIuM2MwLjQsMCwwLjgsMC4xLDEuMiwwLjFjMi40LDAsNC42LTAuOCw2LjQtMi4yYy0yLjIsMC00LjEtMS41LTQuOC0zLjYKCWMwLjMsMC4xLDAuNiwwLjEsMSwwLjFjMC41LDAsMC45LTAuMSwxLjQtMC4yYy0yLjMtMC41LTQuMS0yLjUtNC4xLTVjMCwwLDAsMCwwLTAuMWMwLjcsMC40LDEuNSwwLjYsMi4zLDAuNgoJYy0xLjQtMC45LTIuMy0yLjUtMi4zLTQuM2MwLTAuOSwwLjMtMS44LDAuNy0yLjZjMi41LDMuMSw2LjMsNS4xLDEwLjYsNS40Yy0wLjEtMC40LTAuMS0wLjgtMC4xLTEuMmMwLTIuOCwyLjMtNS4xLDUuMS01LjEKCWMxLjUsMCwyLjgsMC42LDMuNywxLjZjMS4yLTAuMiwyLjMtMC43LDMuMy0xLjJjLTAuNCwxLjItMS4yLDIuMi0yLjMsMi44YzEtMC4xLDItMC40LDIuOS0wLjhjLTAuNywxLTEuNiwxLjktMi42LDIuNwoJYzAsMC4yLDAsMC40LDAsMC43QzMwLDIyLjMsMjQuOCwzMC4yLDE1LjQsMzAuMiIvPgo8L3N2Zz4K) right center no-repeat;
    }

    .boutons-partage .bouton-partage-facebook::before {
        background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDIyLjEuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPgo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkNhbHF1ZV8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIKCSB3aWR0aD0iNDBweCIgaGVpZ2h0PSI0MHB4IiB2aWV3Qm94PSIwIDAgNDAgNDAiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDQwIDQwOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+Cgkuc3Qwe2ZpbGw6IzAwMDA5MTt9Cgkuc3Qxe2ZpbGw6bm9uZTtzdHJva2U6IzAwMDA5MTtzdHJva2UtbWl0ZXJsaW1pdDoxMDt9Cjwvc3R5bGU+CjxwYXRoIGNsYXNzPSJzdDAiIGQ9Ik0yMS45LDMyLjVWMjEuMWgzLjhsMC42LTQuNGgtNC40di0yLjhjMC0xLjMsMC40LTIuMiwyLjItMi4yaDIuNHYtNGMtMC40LTAuMS0xLjgtMC4yLTMuNC0wLjIKCWMtMy40LDAtNS43LDIuMS01LjcsNS45djMuM2gtMy44djQuNGgzLjh2MTEuNEgyMS45eiIvPgo8L3N2Zz4K) right center no-repeat;
    }

    .boutons-partage .bouton-partage-mail::before {
        background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDIyLjEuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPgo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkNhbHF1ZV8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIKCSB3aWR0aD0iNDBweCIgaGVpZ2h0PSI0MHB4IiB2aWV3Qm94PSIwIDAgNDAgNDAiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDQwIDQwOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+Cgkuc3Qwe2ZpbGw6IzAwMDA5MTt9Cgkuc3Qxe2ZpbGw6bm9uZTtzdHJva2U6IzAwMDA5MTtzdHJva2UtbWl0ZXJsaW1pdDoxMDt9Cjwvc3R5bGU+CjxnPgoJPHJlY3QgeD0iOS41IiB5PSIxMi4xIiBjbGFzcz0ic3QxIiB3aWR0aD0iMjEiIGhlaWdodD0iMTUuNyIvPgoJPHBvbHlsaW5lIGNsYXNzPSJzdDEiIHBvaW50cz0iOS41LDEyLjEgMjAsMjEuMiAzMC41LDEyLjEgCSIvPgoJPGxpbmUgY2xhc3M9InN0MSIgeDE9IjkuNSIgeTE9IjI3LjkiIHgyPSIxOC4yIiB5Mj0iMTkuNyIvPgoJPGxpbmUgY2xhc3M9InN0MSIgeDE9IjMwLjUiIHkxPSIyNy45IiB4Mj0iMjEuOCIgeTI9IjE5LjciLz4KPC9nPgo8L3N2Zz4K) right center no-repeat;
    }

    .cheader-buttons .button-cta.carte::before {
        background: url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjEiIHZpZXdCb3g9IjAgMCAyNCAyMSIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik0xOS42OTI5IDVMMTkuMzY2OSA0Ljk1MTk1TDE5LjYxOTUgMy40MzcxOEwxOCAzLjMxMDE0TDE4LjAyNzcgM0wyMCAzLjE1NTExTDE5LjY5MjkgNVoiIGZpbGw9IiMwMDAwOTEiLz4KPHBhdGggZD0iTTYgOEwxMC45MzE4IDMuMzU3MDJMMTUuMzE2NCA2Ljk3NTAzTDIwIDMiIHN0cm9rZT0iIzAwMDA5MSIvPgo8cGF0aCBkPSJNMjMgMTcuMDVMMC4zOTY1NTIgMTcuMzk0NSIgc3Ryb2tlPSIjMDAwMDkxIi8+CjxwYXRoIGQ9Ik0xNSAxMC43NjRMMTUgMTUuMjM2MSIgc3Ryb2tlPSIjMDAwMDkxIi8+CjxwYXRoIGQ9Ik0xOC41IDcuMzU5OUwxOC41IDE0LjY0IiBzdHJva2U9IiMwMDAwOTEiLz4KPHBhdGggZD0iTTMgMC41TDMgMjAuNSIgc3Ryb2tlPSIjMDAwMDkxIi8+CjxwYXRoIGQ9Ik0xMS41IDcuMzU5OUwxMS41IDE0LjY0IiBzdHJva2U9IiMwMDAwOTEiLz4KPHBhdGggZD0iTTguMDAwMDEgMTAuNzY0TDguMDAwMDEgMTUuMjM2MSIgc3Ryb2tlPSIjMDAwMDkxIi8+Cjwvc3ZnPgo=) right 0.625em center no-repeat
    }


    #cpoint .button-cta.linkout::before {
        background: url(data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJDYWxxdWVfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiCgkgd2lkdGg9IjI0cHgiIGhlaWdodD0iMjRweCIgdmlld0JveD0iMCAwIDI0IDI0IiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAyNCAyNDsiIHhtbDpzcGFjZT0icHJlc2VydmUiPgo8c3R5bGUgdHlwZT0idGV4dC9jc3MiPgoJLnN0MHtkaXNwbGF5Om5vbmU7fQoJLnN0MXtkaXNwbGF5OmlubGluZTt9Cgkuc3Qye2ZpbGw6IzAwMDA5MTt9Cgkuc3Qze2ZpbGw6bm9uZTtzdHJva2U6IzAwMDA5MTtzdHJva2UtbWl0ZXJsaW1pdDoxMDt9Cjwvc3R5bGU+CjxnIGlkPSJDYWxxdWVfMl8xXyIgY2xhc3M9InN0MCI+Cgk8ZyBpZD0iR3M0VWhUXzFfIiBjbGFzcz0ic3QxIj4KCQkKCQkJPGltYWdlIHN0eWxlPSJvdmVyZmxvdzp2aXNpYmxlO2VuYWJsZS1iYWNrZ3JvdW5kOm5ldyAgICA7IiB3aWR0aD0iMTU0IiBoZWlnaHQ9IjE1NCIgaWQ9IkdzNFVoVCIgeGxpbms6aHJlZj0iQjMyNzQ4N0ExRjk4NTlBRS5wbmciICB0cmFuc2Zvcm09Im1hdHJpeCg4Ljg3NDQ1OWUtMDIgMCAwIDguODc0NDU5ZS0wMiA1LjE2NjcgNS4xNjY3KSI+CgkJPC9pbWFnZT4KCTwvZz4KPC9nPgo8ZyBpZD0iQ2FscXVlXzFfMV8iPgoJPGc+CgkJPHBhdGggY2xhc3M9InN0MiIgZD0iTTE3LjYsNi4zbC00LjMsNC4zbDAuNywwLjdMMTguMyw3djMuOGgxVjUuM2gtNS41djFIMTcuNnoiLz4KCQk8cG9seWxpbmUgY2xhc3M9InN0MyIgcG9pbnRzPSIxMi43LDcuOCA1LjQsNy44IDUuNCwxOC43IDE2LjMsMTguNyAxNi4zLDExLjcgCQkiLz4KCTwvZz4KPC9nPgo8L3N2Zz4K) right 0.625em center no-repeat
    }

    .cheader-buttons .button-cta.medicale::before {
        background: url(data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iMzBweCIKCSBoZWlnaHQ9IjMwcHgiIHZpZXdCb3g9IjAgMCAzMCAzMCIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMzAgMzA7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4KPHN0eWxlIHR5cGU9InRleHQvY3NzIj4KCS5zdDB7ZGlzcGxheTpub25lO30KCS5zdDF7ZGlzcGxheTppbmxpbmU7fQoJLnN0MntmaWxsOm5vbmU7c3Ryb2tlOiMzODM4Mzg7c3Ryb2tlLW1pdGVybGltaXQ6MTA7fQoJLnN0M3tmaWxsOiMzODM4Mzg7c3Ryb2tlOiMzODM4Mzg7c3Ryb2tlLXdpZHRoOjAuNzcwODtzdHJva2UtbWl0ZXJsaW1pdDoxMDt9Cgkuc3Q0e2ZpbGw6bm9uZTtzdHJva2U6IzM4MzgzODtzdHJva2UtbGluZWpvaW46YmV2ZWw7c3Ryb2tlLW1pdGVybGltaXQ6MTA7fQoJLnN0NXtmaWxsOiMzODM4Mzg7fQoJLnN0NntmaWxsOm5vbmU7c3Ryb2tlOiMzODM4Mzg7c3Ryb2tlLXdpZHRoOjEuMTQ5ODtzdHJva2UtbWl0ZXJsaW1pdDoxMDt9Cgkuc3Q3e2ZpbGw6bm9uZTtzdHJva2U6IzAwMDA5MTtzdHJva2UtbWl0ZXJsaW1pdDoxMDt9Cgkuc3Q4e2ZpbGw6IzAwMDA5MTt9Cgkuc3Q5e2ZpbGw6bm9uZTtzdHJva2U6IzM4MzgzODtzdHJva2Utd2lkdGg6Mi41O3N0cm9rZS1taXRlcmxpbWl0OjEwO30KPC9zdHlsZT4KPGcgaWQ9IkNhcGFfMSIgY2xhc3M9InN0MCI+CjwvZz4KPGcgaWQ9IkNhbHF1ZV8yIj4KCTxwYXRoIGNsYXNzPSJzdDciIGQ9Ik0xOS43LDIzLjlsMC0yLjdjMi43LTEuNCw0LjUtNCw0LjUtN2MwLTQuNS00LjEtOC4xLTkuMi04LjFzLTkuMiwzLjYtOS4yLDguMXM0LjEsOC4xLDkuMiw4LjEKCQljMC44LDAsMS42LTAuMSwyLjMtMC4zTDE5LjcsMjMuOXoiLz4KCTxwb2x5Z29uIGNsYXNzPSJzdDgiIHBvaW50cz0iMTguNiwxMyAxNi41LDEzIDE2LjUsMTAuOCAxMy44LDEwLjggMTMuOCwxMyAxMS42LDEzIDExLjYsMTUuNyAxMy44LDE1LjcgMTMuOCwxNy44IDE2LjUsMTcuOCAKCQkxNi41LDE1LjcgMTguNiwxNS43IAkiLz4KPC9nPgo8L3N2Zz4K) right 0.5em center no-repeat
    }

    .cheader-buttons .button-cta.omedicale::before {
        background: url(data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iMzBweCIKCSBoZWlnaHQ9IjMwcHgiIHZpZXdCb3g9IjAgMCAzMCAzMCIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMzAgMzA7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4KPHN0eWxlIHR5cGU9InRleHQvY3NzIj4KCS5zdDB7ZGlzcGxheTpub25lO30KCS5zdDF7ZGlzcGxheTppbmxpbmU7fQoJLnN0MntmaWxsOm5vbmU7c3Ryb2tlOiMzODM4Mzg7c3Ryb2tlLW1pdGVybGltaXQ6MTA7fQoJLnN0M3tmaWxsOiMzODM4Mzg7c3Ryb2tlOiMzODM4Mzg7c3Ryb2tlLXdpZHRoOjAuNzcwODtzdHJva2UtbWl0ZXJsaW1pdDoxMDt9Cgkuc3Q0e2ZpbGw6bm9uZTtzdHJva2U6IzM4MzgzODtzdHJva2UtbGluZWpvaW46YmV2ZWw7c3Ryb2tlLW1pdGVybGltaXQ6MTA7fQoJLnN0NXtmaWxsOiMzODM4Mzg7fQoJLnN0NntmaWxsOm5vbmU7c3Ryb2tlOiMzODM4Mzg7c3Ryb2tlLXdpZHRoOjEuMTQ5ODtzdHJva2UtbWl0ZXJsaW1pdDoxMDt9Cgkuc3Q3e2ZpbGw6bm9uZTtzdHJva2U6IzM4MzgzODtzdHJva2Utd2lkdGg6Mi41O3N0cm9rZS1taXRlcmxpbWl0OjEwO30KCS5zdDh7ZmlsbDpub25lO3N0cm9rZTojMDAwMDkxO3N0cm9rZS1taXRlcmxpbWl0OjEwO30KCS5zdDl7ZmlsbDojMDAwMDkxO30KPC9zdHlsZT4KPGcgaWQ9IkNhcGFfMSIgY2xhc3M9InN0MCI+CjwvZz4KPGcgaWQ9IkNhbHF1ZV8yIj4KCTxnPgoJCTxwb2x5bGluZSBjbGFzcz0ic3Q4IiBwb2ludHM9IjIxLDEyLjIgMjEsMjMuMiAxOS4zLDI1IDE3LjUsMjYuNyA0LjcsMjYuNyA0LjcsNi40IDE0LjEsNi40IAkJIi8+CgkJPHBvbHlsaW5lIGNsYXNzPSJzdDgiIHBvaW50cz0iMTcuNSwyNi43IDE3LjUsMjMuMiAyMSwyMy4yIAkJIi8+CgkJPGxpbmUgY2xhc3M9InN0OCIgeDE9IjcuMyIgeTE9IjIzLjkiIHgyPSIxNC41IiB5Mj0iMjMuOSIvPgoJCTxnPgoJCQk8bGluZSBjbGFzcz0ic3Q4IiB4MT0iMTIuNSIgeTE9IjE1IiB4Mj0iMTgiIHkyPSIxNSIvPgoJCQk8cmVjdCB4PSI3LjYiIHk9IjEzLjgiIGNsYXNzPSJzdDgiIHdpZHRoPSIyLjQiIGhlaWdodD0iMi40Ii8+CgkJCTxsaW5lIGNsYXNzPSJzdDgiIHgxPSIxMi41IiB5MT0iMTkuOCIgeDI9IjE4IiB5Mj0iMTkuOCIvPgoJCQk8cmVjdCB4PSI3LjYiIHk9IjE4LjYiIGNsYXNzPSJzdDgiIHdpZHRoPSIyLjQiIGhlaWdodD0iMi40Ii8+CgkJPC9nPgoJPC9nPgoJPGxpbmUgY2xhc3M9InN0OCIgeDE9IjcuMyIgeTE9IjguOCIgeDI9IjEyLjgiIHkyPSI4LjgiLz4KCTxsaW5lIGNsYXNzPSJzdDgiIHgxPSI3LjMiIHkxPSIxMS4xIiB4Mj0iMTMuMiIgeTI9IjExLjEiLz4KCTxwYXRoIGNsYXNzPSJzdDgiIGQ9Ik0yMy4xLDEzbDAtMS44YzEuNy0wLjksMi45LTIuNiwyLjktNC41YzAtMi45LTIuNy01LjMtNi01LjNzLTYsMi40LTYsNS4zczIuNyw1LjMsNiw1LjNjMC41LDAsMS0wLjEsMS41LTAuMgoJCUwyMy4xLDEzeiIvPgoJPHBvbHlnb24gY2xhc3M9InN0OSIgcG9pbnRzPSIyMi40LDUuOCAyMSw1LjggMjEsNC40IDE5LjIsNC40IDE5LjIsNS44IDE3LjgsNS44IDE3LjgsNy42IDE5LjIsNy42IDE5LjIsOSAyMSw5IDIxLDcuNiAyMi40LDcuNiAJCgkJIi8+CjwvZz4KPC9zdmc+Cg==) right 0.5em center no-repeat
    }

    .cheader-buttons .button-cta.red::before {
        border: 1px solid #000091;
        background: url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTYiIGhlaWdodD0iMjQiIHZpZXdCb3g9IjAgMCAxNiAyNCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZD0iTTUuMDAwMTkgMTEuMDAwNEw0Ljk0ODYyIDEwLjUwM0w0LjQ1NDU4IDEwLjU1NDNMNC41MDI1MyAxMS4wNDg2TDUuMDAwMTkgMTEuMDAwNFpNNC4zMDMxNyA3LjE1MjI0TDQuMDQxMzUgNy41NzgyMUw0LjQ1MDI4IDcuODI5NTVMNC43MTgxIDcuNDMxMjJMNC4zMDMxNyA3LjE1MjI0Wk0xIDUuMTIxOTdMMC41NzY3MjYgNC44NTU4MkwwLjMwNzgzNyA1LjI4MzQ0TDAuNzM4MTgxIDUuNTQ3OTVMMSA1LjEyMTk3Wk04LjU0NTE0IDEzLjc1ODRMOC40NjE1NyAxMy4yNjU0TDguMDgzNTQgMTMuMzI5NUw4LjA0NzM3IDEzLjcxMTJMOC41NDUxNCAxMy43NTg0Wk04LjMzMjc1IDE2VjE2LjVIOC43ODc2MUw4LjgzMDUyIDE2LjA0NzJMOC4zMzI3NSAxNlpNNS40ODUwOSAxNkw0Ljk4NzQyIDE2LjA0ODNMNS4wMzEyMyAxNi41SDUuNDg1MDlWMTZaTTQuNSAyMC41MDA1QzQuNSAxOS40MjY1IDUuNDE4NDggMTguNSA2LjUxNDg2IDE4LjVWMTcuNUM0Ljg2ODQgMTcuNSAzLjUgMTguODcyIDMuNSAyMC41MDA1SDQuNVpNNi41MTQ4NiAxOC41QzcuNTUzMzMgMTguNSA4LjUgMTkuNDI2MyA4LjUgMjAuNTAwNUg5LjVDOS41IDE4Ljg3MjMgOC4xMDM4NyAxNy41IDYuNTE0ODYgMTcuNVYxOC41Wk04LjUgMjAuNTAwNUM4LjUgMjEuNTczOCA3LjU1MzQ5IDIyLjUgNi41MTQ4NiAyMi41VjIzLjVDOC4xMDM3IDIzLjUgOS41IDIyLjEyODEgOS41IDIwLjUwMDVIOC41Wk02LjUxNDg2IDIyLjVDNS40MTgzMiAyMi41IDQuNSAyMS41NzM2IDQuNSAyMC41MDA1SDMuNUMzLjUgMjIuMTI4NCA0Ljg2ODU2IDIzLjUgNi41MTQ4NiAyMy41VjIyLjVaTTUuMDUxNzYgMTEuNDk3N0M2LjgxMjU3IDExLjMxNTEgOC4zNTY0NiAxMS4wMjg5IDkuNDY3NCAxMC40MTk3QzEwLjAzMjcgMTAuMTA5NyAxMC41MDI3IDkuNzA3NDcgMTAuODI4OCA5LjE3OTEzQzExLjE1NTkgOC42NDkxMiAxMS4zMTgxIDguMDI0NiAxMS4zMTgxIDcuMzAzMTlIMTAuMzE4MUMxMC4zMTgxIDcuODY5OTYgMTAuMTkyNCA4LjMwNjEzIDkuOTc3NzkgOC42NTM5M0M5Ljc2MjEgOS4wMDM0IDkuNDM2NDcgOS4yOTYxNSA4Ljk4NjYgOS41NDI4NEM4LjA2NzEyIDEwLjA0NyA2LjcwMjA0IDEwLjMyMTIgNC45NDg2MiAxMC41MDNMNS4wNTE3NiAxMS40OTc3Wk0xMS4zMTgxIDcuMzAzMTlDMTEuMzE4MSA1LjQyMTk1IDkuODUxNjggNC4xMzU5OCA4LjAyOTUyIDQuMTM1OThWNS4xMzU5OEM5LjM1OTMyIDUuMTM1OTggMTAuMzE4MSA2LjAzMjMxIDEwLjMxODEgNy4zMDMxOUgxMS4zMTgxWk04LjAyOTUyIDQuMTM1OThDNy4xMjY4MSA0LjEzNTk4IDYuMzczMjUgNC4zNzg0OSA1LjY5NDU5IDQuODYxNzJDNS4wMzA5IDUuMzM0MzEgNC40NjA2MyA2LjAyMTkyIDMuODg4MjQgNi44NzMyNkw0LjcxODEgNy40MzEyMkM1LjI2NjQ1IDYuNjE1NjUgNS43NTY2NyA2LjA0NTEzIDYuMjc0NjMgNS42NzYzMkM2Ljc3NzYyIDUuMzE4MTUgNy4zMjY4NyA1LjEzNTk4IDguMDI5NTIgNS4xMzU5OFY0LjEzNTk4Wk00LjU2NDk5IDYuNzI2MjdMMS4yNjE4MiA0LjY5NkwwLjczODE4MSA1LjU0Nzk1TDQuMDQxMzUgNy41NzgyMUw0LjU2NDk5IDYuNzI2MjdaTTEuNDIzMjcgNS4zODgxM0MyLjIzODEgNC4wOTIzIDMuMTgzNzUgMy4xMjU3MiA0LjI4OTU3IDIuNDgxMjVDNS4zOTM0MSAxLjgzNzk0IDYuNjgzNDQgMS41IDguMjEyNTMgMS41VjAuNUM2LjUyOTI4IDAuNSA1LjA2MTI4IDAuODc0MDc4IDMuNzg2MDUgMS42MTcyN0MyLjUxMjgxIDIuMzU5MzEgMS40NTgzNyAzLjQ1MzcyIDAuNTc2NzI2IDQuODU1ODJMMS40MjMyNyA1LjM4ODEzWk04LjIxMjUzIDEuNUMxMS41NzYxIDEuNSAxNC41IDMuNzkxNDEgMTQuNSA3LjMwMzE5SDE1LjVDMTUuNSAzLjExODczIDExLjk5OTggMC41IDguMjEyNTMgMC41VjEuNVpNMTQuNSA3LjMwMzE5QzE0LjUgOC45MzczNiAxMy44ODMyIDEwLjI2MTggMTIuODM2NCAxMS4yNTk5QzExLjc4MTIgMTIuMjY2MSAxMC4yNjczIDEyLjk1OTMgOC40NjE1NyAxMy4yNjU0TDguNjI4NzEgMTQuMjUxNEMxMC41OCAxMy45MjA2IDEyLjI5MzYgMTMuMTU5MiAxMy41MjY1IDExLjk4MzZDMTQuNzY4IDEwLjc5OTkgMTUuNSA5LjIxNTIzIDE1LjUgNy4zMDMxOUgxNC41Wk04LjA0NzM3IDEzLjcxMTJMNy44MzQ5OCAxNS45NTI4TDguODMwNTIgMTYuMDQ3Mkw5LjA0MjkxIDEzLjgwNTZMOC4wNDczNyAxMy43MTEyWk04LjMzMjc1IDE1LjVINS40ODUwOVYxNi41SDguMzMyNzVWMTUuNVpNNS45ODI3NSAxNS45NTE3TDUuNDk3ODYgMTAuOTUyMUw0LjUwMjUzIDExLjA0ODZMNC45ODc0MiAxNi4wNDgzTDUuOTgyNzUgMTUuOTUxN1oiIGZpbGw9IiMwMDAwOTEiLz4KPC9zdmc+Cg==) right 0.875em center no-repeat;
    }

    .cheader-buttons .button-cta.notif {
        background-color: #E1000F;
        border-color: #E1000F;
        color: #FFFFFF;
        padding-right: 4.5em;
    }

    .notifmilieu.button-cta {
        font-weight: 400;
        letter-spacing: 0.125em;
    }

    .cheader-buttons .button-cta.notif::before,
    .notifmilieu.button-cta::before {
        border: 1px solid #E1000F;
        background: #FFFFFF url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjYiIGhlaWdodD0iMzIiIHZpZXdCb3g9IjAgMCAyNiAzMiIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik0xIDI3TDIuNzU5MjYgMjQuMDEyN0MzLjk5MjUxIDIxLjkxNzcgNC42NDQ3NSAxOS41MTkzIDQuNjQ0NzUgMTcuMDc0N1YxMy41MTY2QzQuNjQ0NzUgOC44MjAzNSA4LjM5MTQ2IDUgMTIuOTk5MiA1QzE3LjYwODUgNSAyMS4zNTUyIDguODIwMzUgMjEuMzU1MiAxMy41MTY2VjE3LjA3NDdDMjEuMzU1MiAxOS41MTkzIDIyLjAwNzUgMjEuOTE3NyAyMy4yMzkxIDI0LjAxMjdMMjUgMjdIMVoiIHN0cm9rZT0iI0UxMDAwRiIvPgo8cGF0aCBmaWxsLXJ1bGU9ImV2ZW5vZGQiIGNsaXAtcnVsZT0iZXZlbm9kZCIgZD0iTTEgMjdMMi43NTkyNiAyNC4wMTI3QzMuOTkyNTEgMjEuOTE3NyA0LjY0NDc1IDE5LjUxOTMgNC42NDQ3NSAxNy4wNzQ3VjEzLjUxNjZDNC42NDQ3NSA4LjgyMDM1IDguMzkxNDYgNSAxMi45OTkyIDVDMTcuNjA4NSA1IDIxLjM1NTIgOC44MjAzNSAyMS4zNTUyIDEzLjUxNjZWMTcuMDc0N0MyMS4zNTUyIDE5LjUxOTMgMjIuMDA3NSAyMS45MTc3IDIzLjIzOTEgMjQuMDEyN0wyNSAyN0gxWiIgc3Ryb2tlPSIjRTEwMDBGIi8+CjxwYXRoIGQ9Ik0xMS4zNDQyIDI3QzExLjEyNTYgMjcuMzk1NCAxMSAyNy44NTUxIDExIDI4LjM0NjNDMTEgMjkuODEyNCAxMi4xMTg4IDMxIDEzLjUgMzFDMTQuODgxMiAzMSAxNiAyOS44MTI0IDE2IDI4LjM0NjNDMTYgMjcuODU1MSAxNS44NzQ0IDI3LjM5NTQgMTUuNjU1OCAyNy4wMDE1IiBzdHJva2U9IiNFMTAwMEYiLz4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik0xNSA1LjIyMTQ0VjIuODkyNjZDMTUgMS44NDcwMiAxNC4xMDQ2IDEgMTIuOTk5MiAxQzExLjg5NTQgMSAxMSAxLjg0NzAyIDExIDIuODkyNjZWNS4yMjE0NEMxMSA1LjIyMTQ0IDEyLjI5OTEgNSAxMyA1QzEzLjcwMDkgNSAxNSA1LjIyMTQ0IDE1IDUuMjIxNDRaIiBzdHJva2U9IiNFMTAwMEYiLz4KPHBhdGggZD0iTTE0IDIyTDE4IDE2IiBzdHJva2U9IiNFMTAwMEYiLz4KPHBhdGggZD0iTTE1IDI0TDE5IDE4IiBzdHJva2U9IiNFMTAwMEYiLz4KPC9zdmc+Cg==) right 0.5em center no-repeat;
    }

    .cheader-buttons .button-cta.notif::after,
    #cmesures .c-links .button-cta::before,
    #cmesures .c-links .button-cta::after {
        border-color: #E1000F;
    }

    #cpoint .button-cta::before {
        background: #FFFFFF url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTciIGhlaWdodD0iMjEiIHZpZXdCb3g9IjAgMCAxNyAyMSIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik0xIDEyLjU3NTJMMy4zNDE5NSAxMC4yNTY4TDYuODQ0ODMgMTMuNzI0NVYxSDEwLjE1NjZWMTMuNzI0NUwxMy42NTk1IDEwLjI1NjhMMTYgMTIuNTc1Mkw4LjUgMjBMMSAxMi41NzUyWiIgc3Ryb2tlPSIjRTEwMDBGIi8+Cjwvc3ZnPgo=) right 0.875em center no-repeat;
    }

    #cmesures .c-links .button-cta {
        border-color: #E1000F;
        color: #E1000F;
    }

    #cmesures .c-links .button-cta.facebook::before {
        background: url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTMiIGhlaWdodD0iMjUiIHZpZXdCb3g9IjAgMCAxMyAyNSIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik0zLjg0MzM5IDI1VjEzLjU5NThIMFY5LjE1MjMxSDMuODQzMzlWNS44NzQyNkMzLjg0MzM5IDIuMDcxMTkgNi4xNjg3MSAwIDkuNTY2NTUgMEMxMS4xOTI5IDAgMTIuNTkyMyAwLjExOTc2IDEzIDAuMTczMjQ4VjQuMTUwMDFIMTAuNjQyOEM4Ljc5NTk2IDQuMTUwMDEgOC40Mzg5OSA1LjAyNjgzIDguNDM4OTkgNi4zMTQwN1Y5LjE1MjMxSDEyLjg0MzRMMTIuMjcyMSAxMy41OTU4SDguNDM4OTlWMjUiIGZpbGw9IiNFMTAwMEYiLz4KPC9zdmc+Cg==) right 1.1em center no-repeat;
    }

    #cmesures .c-links .button-cta.instagram::before {
        background: url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjIiIGhlaWdodD0iMjQiIHZpZXdCb3g9IjAgMCAyMiAyNCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPG1hc2sgaWQ9Im1hc2swIiBtYXNrLXR5cGU9ImFscGhhIiBtYXNrVW5pdHM9InVzZXJTcGFjZU9uVXNlIiB4PSIwIiB5PSIxIiB3aWR0aD0iMjIiIGhlaWdodD0iMjIiPgo8cGF0aCBmaWxsLXJ1bGU9ImV2ZW5vZGQiIGNsaXAtcnVsZT0iZXZlbm9kZCIgZD0iTTAgMS4wNzAzMUgyMS45MjczVjIzSDBWMS4wNzAzMVoiIGZpbGw9IndoaXRlIi8+CjwvbWFzaz4KPGcgbWFzaz0idXJsKCNtYXNrMCkiPgo8cGF0aCBmaWxsLXJ1bGU9ImV2ZW5vZGQiIGNsaXAtcnVsZT0iZXZlbm9kZCIgZD0iTTEwLjk2MzQgMS4wNzAzMUM3Ljk4NTI2IDEuMDcwMzEgNy42MTI3NCAxLjA4NDQ5IDYuNDQzNDYgMS4xMzcxNkM1LjI3NjA5IDEuMTkwMTUgNC40NzkzMyAxLjM3NjIgMy43ODEyNSAxLjY0Njk3QzMuMDYwODYgMS45MjY2MiAyLjQ0OTA5IDIuMzAxMDQgMS44Mzg2OSAyLjkxMTQ0QzEuMjMwMiAzLjUyMTQxIDAuODUzNjYzIDQuMTMzMTggMC41NzQyMjEgNC44NTQxQzAuMzA0MTkyIDUuNTUwMjcgMC4xMTkzMDggNi4zNDg0MSAwLjA2NDUxOTIgNy41MTQwOUMwLjAxMTg0NjIgOC42ODQ4NSAwIDkuMDU3OSAwIDEyLjAzNjRDMCAxNS4wMTI4IDAuMDExODQ2MiAxNS4zODcyIDAuMDY0NTE5MiAxNi41NTY2QzAuMTE5MzA4IDE3LjcyMzcgMC4zMDQxOTIgMTguNTIwMyAwLjU3NDIyMSAxOS4yMTgzQzAuODUzNjYzIDE5LjkzOTEgMS4yMzAyIDIwLjU1MSAxLjgzODY5IDIxLjE1OTNDMi40NDkwOSAyMS43NjkyIDMuMDYwODYgMjIuMTQ0IDMuNzgxMjUgMjIuNDI1MUM0LjQ3OTMzIDIyLjY5NjIgNS4yNzYwOSAyMi44ODA2IDYuNDQzNDYgMjIuOTM1QzcuNjEyNzQgMjIuOTg4IDcuOTg1MjYgMjMuMDAwMSAxMC45NjM0IDIzLjAwMDFDMTMuOTQxNSAyMy4wMDAxIDE0LjMxNDYgMjIuOTg4IDE1LjQ4NCAyMi45MzVDMTYuNjUxMyAyMi44ODA2IDE3LjQ0ODEgMjIuNjk2MiAxOC4xNDU2IDIyLjQyNTFDMTguODY2IDIyLjE0NCAxOS40NzggMjEuNzY5MiAyMC4wODg2IDIxLjE1OTNDMjAuNjk2NiAyMC41NTEgMjEuMDczMSAxOS45MzkxIDIxLjM1MyAxOS4yMTgzQzIxLjYyMzMgMTguNTIwMyAyMS44MDkzIDE3LjcyMzcgMjEuODYyMyAxNi41NTY2QzIxLjkxNTEgMTUuMzg3MiAyMS45MjczIDE1LjAxMjggMjEuOTI3MyAxMi4wMzY0QzIxLjkyNzMgOS4wNTc5IDIxLjkxNTEgOC42ODQ4NSAyMS44NjIzIDcuNTE0MDlDMjEuODA5MyA2LjM0ODQxIDIxLjYyMzMgNS41NTAyNyAyMS4zNTMgNC44NTQxQzIxLjA3MzEgNC4xMzMxOCAyMC42OTY2IDMuNTIxNDEgMjAuMDg4NiAyLjkxMTQ0QzE5LjQ3OCAyLjMwMTA0IDE4Ljg2NiAxLjkyNjYyIDE4LjE0NTYgMS42NDY5N0MxNy40NDgxIDEuMzc2MiAxNi42NTEzIDEuMTkwMTUgMTUuNDg0IDEuMTM3MTZDMTQuMzE0NiAxLjA4NDQ5IDEzLjk0MTUgMS4wNzAzMSAxMC45NjM0IDEuMDcwMzFaTTEwLjk2MzQgMy4wNDY4MkMxMy44OTA5IDMuMDQ2ODIgMTQuMjM5MiAzLjA1NzE5IDE1LjM5NDIgMy4xMDk4NkMxNi40NjMgMy4xNTkxNSAxNy4wNDMzIDMuMzM4NTMgMTcuNDI5OSAzLjQ4Nzk5QzE3Ljk0MTQgMy42ODY1MSAxOC4zMDc1IDMuOTIzODYgMTguNjkxMiA0LjMwOTE4QzE5LjA3NTkgNC42OTIyNyAxOS4zMTI2IDUuMDU4MDIgMTkuNTEyIDUuNTY5NjNDMTkuNjYxNCA1Ljk1NDc0IDE5Ljg0MDQgNi41MzY3OCAxOS44ODgyIDcuNjA1NjlDMTkuOTQxMiA4Ljc2MDU4IDE5Ljk1MjkgOS4xMDg3NyAxOS45NTI5IDEyLjAzNjRDMTkuOTUyOSAxNC45NjM1IDE5Ljk0MTIgMTUuMzA5OCAxOS44ODgyIDE2LjQ2NjdDMTkuODQwNCAxNy41MzU5IDE5LjY2MTQgMTguMTE2IDE5LjUxMiAxOC41MDI1QzE5LjMxMjYgMTkuMDE0MyAxOS4wNzU5IDE5LjM3OTggMTguNjkxMiAxOS43NjMxQzE4LjMwNzUgMjAuMTQ2NSAxNy45NDE0IDIwLjM4NCAxNy40Mjk5IDIwLjU4MjZDMTcuMDQzMyAyMC43MzM5IDE2LjQ2MyAyMC45MTEyIDE1LjM5NDIgMjAuOTYwNUMxNC4yMzkyIDIxLjAxMzUgMTMuODkwOSAyMS4wMjU3IDEwLjk2MzQgMjEuMDI1N0M4LjAzNjQ1IDIxLjAyNTcgNy42OTAwNiAyMS4wMTM1IDYuNTMzMjYgMjAuOTYwNUM1LjQ2MzgzIDIwLjkxMTIgNC44ODM0NyAyMC43MzM5IDQuNDk2ODggMjAuNTgyNkMzLjk4NDg2IDIwLjM4NCAzLjYxOTg1IDIwLjE0NjUgMy4yMzYyMiAxOS43NjMxQzIuODUzMTIgMTkuMzc5OCAyLjYxNTc4IDE5LjAxNDMgMi40MTU0NSAxOC41MDI1QzIuMjY1ODkgMTguMTE2IDIuMDg3MDQgMTcuNTM1OSAyLjAzOTIzIDE2LjQ2NjdDMS45ODYyNCAxNS4zMDk4IDEuOTc0MzkgMTQuOTYzNSAxLjk3NDM5IDEyLjAzNjRDMS45NzQzOSA5LjEwODc3IDEuOTg2MjQgOC43NjA1OCAyLjAzOTIzIDcuNjA1NjlDMi4wODcwNCA2LjUzNjc4IDIuMjY1ODkgNS45NTQ3NCAyLjQxNTQ1IDUuNTY5NjNDMi42MTU3OCA1LjA1ODAyIDIuODUzMTIgNC42OTIyNyAzLjIzNjIyIDQuMzA5MThDMy42MTk4NSAzLjkyMzg2IDMuOTg0ODYgMy42ODY1MSA0LjQ5Njg4IDMuNDg3OTlDNC44ODM0NyAzLjMzODUzIDUuNDYzODMgMy4xNTkxNSA2LjUzMzI2IDMuMTA5ODZDNy42OTAwNiAzLjA1NzE5IDguMDM2NDUgMy4wNDY4MiAxMC45NjM0IDMuMDQ2ODJaIiBmaWxsPSIjRTEwMDBGIi8+CjwvZz4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik0xMC40OTkxIDE2LjA2OTZDOC41MjgxNiAxNi4wNjk2IDYuOTI4NzEgMTQuNDcyNCA2LjkyODcxIDEyLjQ5OUM2LjkyODcxIDEwLjUyNzUgOC41MjgxNiA4LjkzMDMgMTAuNDk5MSA4LjkzMDNDMTIuNDcwNyA4LjkzMDMgMTQuMDY5NSAxMC41Mjc1IDE0LjA2OTUgMTIuNDk5QzE0LjA2OTUgMTQuNDcyNCAxMi40NzA3IDE2LjA2OTYgMTAuNDk5MSAxNi4wNjk2Wk0xMC40OTkxIDdDNy40NjEwNyA3IDUgOS40NjI5IDUgMTIuNDk5QzUgMTUuNTM3MyA3LjQ2MTA3IDE4IDEwLjQ5OTEgMThDMTMuNTM3MiAxOCAxNiAxNS41MzczIDE2IDEyLjQ5OUMxNiA5LjQ2MjkgMTMuNTM3MiA3IDEwLjQ5OTEgN1oiIGZpbGw9IiNFMTAwMEYiLz4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik0xOCA2LjQ5OTg4QzE4IDcuMzI4MTIgMTcuMzI4NyA4IDE2LjUwMSA4QzE1LjY3MTMgOCAxNSA3LjMyODEyIDE1IDYuNDk5ODhDMTUgNS42NzE2NCAxNS42NzEzIDUgMTYuNTAxIDVDMTcuMzI4NyA1IDE4IDUuNjcxNjQgMTggNi40OTk4OFoiIGZpbGw9IiNFMTAwMEYiLz4KPC9zdmc+Cg==) right 0.7em center no-repeat;
    }

    #cmesures .c-links .button-cta.tiktok::before {
        background: url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAiIGhlaWdodD0iMjQiIHZpZXdCb3g9IjAgMCAyMCAyNCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik0xNS4xMjQyIDkuMTE4OTZDMTUuMTI0MiA5LjExODk2IDE2LjcwNDMgMTAuMjA3NyAyMCAxMC4yMDc3VjUuMzUzNjhDMjAgNS4zNTM2OCAxNC45NDM2IDUuNTgwNSAxNC45NDM2IDAuMzE4MTc2SDEwLjExMjlWMTUuNDdDMTAuMTEyOSAxNS40NyAxMC40Mjg5IDE4LjI4MjcgNy41ODQ2NSAxOC4yODI3QzQuNzQwNDEgMTguMjgyNyA1LjAxMTI5IDE1LjI0MzIgNS4wMTEyOSAxNS4yNDMyQzUuMDExMjkgMTUuMjQzMiA1LjA1NjQzIDEyLjg4NDIgNy45OTA5NyAxMi44ODQyVjguMDc1NTdDNy45OTA5NyA4LjA3NTU3IDAgNy42NjcyOSAwIDE1LjgzM0MwIDIzLjMxODIgNy42Mjk4IDIzLjMxODIgNy42Mjk4IDIzLjMxODJDNy42Mjk4IDIzLjMxODIgMTUuMTI0MiAyMy4zNjM1IDE1LjEyNDIgMTUuOTY5MVY5LjExODk2WiIgZmlsbD0iI0UxMDAwRiIvPgo8L3N2Zz4K) right 0.75em center no-repeat;
    }

    .button-cta.back-to-topics,
    .button-cta.back-to-topics::before,
    .button-cta.back-to-topics::after {
        border-color: #6A6A6A;
        color: #6A6A6A;
    }

    .button-cta.back-to-topics::before {
        background: url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTciIGhlaWdodD0iMjEiIHZpZXdCb3g9IjAgMCAxNyAyMSIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik0xNiA4LjQyNDc2TDEzLjY1OCAxMC43NDMyTDEwLjE1NTIgNy4yNzU0OVYyMEg2Ljg0MzM5VjcuMjc1NDlMMy4zNDA1MiAxMC43NDMyTDEgOC40MjQ3Nkw4LjUgMUwxNiA4LjQyNDc2WiIgc3Ryb2tlPSIjNkE2QTZBIi8+Cjwvc3ZnPgo=) right 0.875em center no-repeat;
    }

    .button-cta.back-to-topics {
        display: none;
    }

    .item-paragraphe.active .button-cta.back-to-topics {
        display: inline-block;
        margin-top: 1em;
    }

    /* onglets */
    .item-paragraphe {
        display: none;
        margin-top: 1.5em;
    }

    .item-paragraphe.active {
        display: block;
    }

    .item-paragraphe.active .item-paragraphe-titre {
        display: none;
    }

    /* header buttons */
    #cheader-top {
        box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.1), 0px 2px 0px rgba(0, 0, 0, 0.1);
        box-sizing: border-box;
        display: flex;
        justify-content: space-between;
        margin-left: 50%;
        padding: 1.5em calc((100vw - 100%) / 2);
        position: relative;
        transform: translateX(-50%);
        width: 100vw;
        z-index: 1;
    }

    #cheader-top .cheader-top {
        display: inline;
    }

    #cheader-top .cheader-buttons ul {
        margin: 0;
    }

    #cheader-top .cheader-buttons li {
        margin-left: 2em;
    }

    #cheader-top p {
        background-color: #000091;
        color: #FFFFFF;
        display: inline-block;
        padding: 0.625em 1.5em;
    }

    .cheader-buttons li {
        display: inline;
    }

    /* update */
    #cdate-top {
        background: #FFFFFF;
        border-bottom: 2px solid #CCCCE9;
        box-sizing: border-box;
        display: flex;
        font-size: 0.75em;
        align-items: center;
        justify-content: space-between;
        padding: 0 calc((100vw - 100%) / 2);
        position: relative;
        margin-left: 50%;
        transform: translateX(-50%);
        width: 100vw;
        z-index: 0;
    }

    #cdate-top .cheader-top p {
        color: #000091;
        font-weight: 500;
        text-transform: uppercase;
    }

    #cdate-top .cheader-top p::before {
        content: '';
        display: inline-block;
        height: 0.75em;
        width: 0.75em;
        background-color: #E1000F;
        border-radius: 50%;
        margin-right: 0.5em;
        animation: blink 0.6s infinite alternate linear;
    }

    @keyframes blink {
        0% {
          opacity: 0;
        }
        100% {
          opacity: 1;
        }
    }

    /* language selector */
    ul.switch-lang {
        margin: 0;
        position: relative;
        top: 2px;
    }

    .switch-lang li {
        display: inline;
        margin-left: 3.5em;
    }

    .switch-lang li:first-child {
        margin: 0;
    }

    .switch-lang a {
        border-bottom: 4px solid transparent;
        display: inline-block;
        color: #000091;
        font-weight: 400;
        padding: 1.25em 0;
        text-decoration: none;
        text-transform: uppercase;
    }

    .switch-lang a.active {
        font-weight: 700;
    }

    .switch-lang a.active,
    .switch-lang a:hover {
        border-bottom: 4px solid #000091;
    }

    /* hero */
    #ctitle-top {
        background: #FFFFFF;
        border-bottom: 2px solid #CCCCE9;
        box-sizing: border-box;
        padding: 3.75em calc((100vw - 100%) / 2);
        position: relative;
        margin-left: 50%;
        text-transform: uppercase;
        transform: translateX(-50%);
        width: 100vw;
    }

    #ctitle-top h1 {
        font-size: 2.6em;
        color: #000091;
        letter-spacing: 0.625em;
        line-height: 1.25em;
        text-align: center;
    }

    #ctitle-top h1 span {
        color: #E1000F;
        display: block;
        letter-spacing: 0.3125em;
        margin-bottom: 0.25em;
        text-transform: lowercase;
    }

    /* texte d'information */
    #cintro,
    #cpoint {
        line-height: 1.5em;
        margin-top: 3em;
    }

    #cpoint .c-links li {
        display: inline-block;
        list-style-type: none;
    }

    #cpoint .c-titre {
        align-items: center;
        display: flex;
        justify-content: space-between;
        margin-bottom: 2em;
    }

    #cpoint .c-titre h2 {
        width: auto;
    }

    #cpoint .c-titre p {
        color: #E1000F;
        font-size: 1.5em;
        font-weight: 700;
    }

    #cpoint .c-texte {
        margin-bottom: 2em;
    }

    #cpoint .c-texte p:first-child,
    #cprotection .item-paragraphe-texte .c-enavant {
        display: block;
        font-size: 1.25em;
        line-height: 1.5em;
        margin-bottom: 1.5em;
    }

    #cpoint .attestations {
        display: none;
        flex-wrap: wrap;
        margin-bottom: 2em;
    }

    #cpoint .attestations.active {
        display: flex;
    }

    /* informations essentielles */
    #cinformations .c-images {
        display: flex;
        justify-content: space-between;
        margin-bottom: 2em;
    }

    #cinformations .cinformations-item-image {
        flex: 0 1 48%;
    }

    #cinformations .c-images img {
        width: 100%;
    }

    /* notifications */
    .conteneur-notifications {
        border: 1px solid rgba(225, 0, 15, 0.2);
        box-shadow: 0px 4px 12px rgba(0, 0, 0, 0.15);
        max-width: 508px;
        margin: 3em auto;
        padding: 1em 3em;
        position: relative;
    }

    .notifications-close {
        border: 1px solid #6A6A6A;
        height: 40px;
        margin: 0;
        padding: 0;
        position: absolute;
        right: 0.75em;
        top: 0.75em;
        width: 40px;
    }

    .notifications-close a {
        background: url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTgiIGhlaWdodD0iMTgiIHZpZXdCb3g9IjAgMCAxOCAxOCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik0xNyAxNC41NjY1TDExLjUxNDcgOS4wNTE2NUwxNyAzLjU2MTY1TDE0LjUwMTggMS4wNjM0OEw5LjAzNjM2IDYuNTI4OTFMMy40OTgxNyAwLjk5MDc4NEwxIDMuNDg4OTJMNi40ODUyOCA5LjAwMzc3TDEgMTQuNDkzOEwzLjQ5ODE3IDE2Ljk5Mkw4Ljk2MzY0IDExLjUyNjVMMTQuNTAxOCAxNy4wNjQ3TDE3IDE0LjU2NjVaIiBzdHJva2U9IiM2QTZBNkEiLz4KPC9zdmc+Cg==) center center no-repeat;
        display: block;
        height: 100%;
        text-decoration: none;
        width: 100%;
    }

    .notifications-title {
        color: #000091;
        font-size: 1.75em;
        font-weight: 700;
    }

    .notifications-texte {
        line-height: 1.5em;
        margin: 1em 0;
    }

    .notifications-aide {
        margin: 1em 0 0.5em;
    }

    .notifications-aide a {
        color: #6A6A6A;
        font-size: 0.75em;
        font-weight: 400;
        text-decoration-thickness: 1px;
    }

    /* gestes */
    #cprotection ul.c-gestes {
        align-items: center;
        display: flex;
        flex-wrap: wrap;
        font-size: 1.25em;
        justify-content: space-between;
        margin-bottom: 2em;
        padding: 0;
    }

    #cprotection .c-gestes li {
        flex: 0 1 45%;
        display: flex;
    }

    #cprotection .c-gestes img {
        height: 100px;
        margin-right: 1em;
        width: 100px;
    }

    #cprotection .c-texte {
        border-top: 2px solid #CCCCE9;
    }

    #cprotection .c-texte p {
        font-size: 1.25em;
    }

    #cprotection .c-texte img {
        width: 100%;
    }

    #cprotection .item-paragraphe-texte {
        margin-bottom: 2em;
    }

    /* misc */
    #cqr h2,
    #cdocuments h2 {
        background-color: #CCCCE9 !important;
        color: #000091 !important;
    }

    #cmesures .c-links {
        margin-top : 0;
    }

    #cmesures .c-links li {
        display: inline-block;
        list-style-type: none;
        margin: 1.5em 1.5em 0 0;
    }

    #cqr .c-intro {
        border: 0;
        padding: 0;
    }

    #cqr .item-paragraphe {
        margin-top: 0;
    }

    #cqr .item-paragraphe-texte {
        padding: 1em;
    }

    #cqr .item-paragraphe-texte:first-child {
        font-size: 1.5em;
    }

    #cqr .tab-buttons {
        border: 0;
        padding: 0;
    }

    #cqr .tab-button {
        padding-left: 3.5em;
    }

    #cqr .c-paragraphes > .items-qr > .item-qr {
        border: 0;
    }

    #cqr .c-texte p,
    #cdocuments .c-texte .item-file,
    #cdocuments .c-links {
        margin: 0;
    }

    #cdocuments .c-texte {
        margin: 2em 0;
    }

    #cdocuments .c-texte > :first-child {
        font-size: 1em;
    }

    #cdocuments .c-texte p {
        margin: 1em 0;
    }

    #cdocuments .c-texte p:first-child {
        font-size: 1.25em;
    }

    #cdocuments .tab-buttons {
        border-top: 2px solid #CCCCE9;
        margin-top: 3em;
        padding-top: 2em;
    }

    #cdocuments .c-onglet {
        display: none;
    }
    .attestations .item-file h3{
         display: none;
    }
    .attestations .item-file, #cdocuments .c-paragraphes .item-file {
        display: none;
        flex-wrap: wrap;
        margin-top: 2.5em;
        margin: 1.5em 0;
    }

    .attestations .item-file, #cdocuments .c-paragraphes .item-file.active {
        display: flex;
    }

    .attestations .file,
    #cdocuments .file {
        flex: 0 0 0;
        flex-basis: 48%;
        margin: 0.5em;
        width: auto;
    }

    #cdocuments .c-onglet {
        width: 100%;
    }

    a.download-link {
        background: url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNDUiIGhlaWdodD0iNDMiIHZpZXdCb3g9IjAgMCA0NSA0MyIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPGcgY2xpcC1wYXRoPSJ1cmwoI2NsaXAwKSI+CjxyZWN0IHdpZHRoPSI0MCIgaGVpZ2h0PSI0MCIgdHJhbnNmb3JtPSJtYXRyaXgoLTEgMCAwIDEgNDAuNSAwKSIgZmlsbD0id2hpdGUiIHN0cm9rZT0iIzAwMDA5MSIvPgo8cGF0aCBkPSJNNDQuNSAwVjQzSDAuNSIgc3Ryb2tlPSIjMDAwMDkxIi8+CjxwYXRoIGZpbGwtcnVsZT0iZXZlbm9kZCIgY2xpcC1ydWxlPSJldmVub2RkIiBkPSJNMjkuNSAzMkgxMC41VjlIMjIuODg0NkwyOS41IDE1LjU1N1YzMloiIHN0cm9rZT0iIzAwMDA5MSIvPgo8cGF0aCBkPSJNMTQuNSAyMy4wNUgyNS41IiBzdHJva2U9IiMwMDAwOTEiLz4KPHBhdGggZD0iTTE0LjUgMjcuMDVIMjUuNSIgc3Ryb2tlPSIjMDAwMDkxIi8+CjxwYXRoIGQ9Ik0xNC41IDE5LjA1SDI1LjUiIHN0cm9rZT0iIzAwMDA5MSIvPgo8cGF0aCBkPSJNMTQuNSAxNS4wNUgyMy41IiBzdHJva2U9IiMwMDAwOTEiLz4KPC9nPgo8ZGVmcz4KPGNsaXBQYXRoIGlkPSJjbGlwMCI+CjxyZWN0IHg9IjAuNSIgd2lkdGg9IjQ0IiBoZWlnaHQ9IjQzIiBmaWxsPSJ3aGl0ZSIvPgo8L2NsaXBQYXRoPgo8L2RlZnM+Cjwvc3ZnPgo=) left top no-repeat;
        color: #383838;
        display: block;
        min-height: 3em;
        padding-left: 4em;
    }
    a.download-link:hover{
        text-decoration: none;
    }
    a.download-link span {
        text-transform: lowercase;
    }

    a.download-link span::before {
        content: ' • ';
    }

    @media (max-width: 700px) {
        body {
            font-size: 14px;
        }

        #content {
            margin: 0;
            padding: 0;
        }

        .cinfo-block {
            padding: 0 1.5em 3em;
        }

        .cinfo-block > .c-header {
            margin: 0 -1.5em;
        }

        .tab-buttons {
            flex-direction: column;
        }

        .cinfo-block:not(#cqr) .items-qr {
            margin: 0;
        }

        #cdate-top,
        #cpoint .attestations,
        #cinformations .c-images,
        #cprotection ul.c-gestes,
        #cdocuments .item-file,
        .attestations .item-file {
            flex-direction: column;
        }

        #cheader-top {
            padding: 1.5em;
        }

        #cheader-top .cheader-buttons ul {
            display: flex;
            padding: 0;
        }

        .cheader-buttons li.even {
            order: 2;
        }
        
        .cheader-buttons li.even {
            order: 1;
        }
        
        #cheader-top .cheader-buttons .button-cta {
            color: transparent;
            height: 40.5px;
            padding: 0; 
            font-size: 0;
            width: 41px;
        }

        #cheader-top .cheader-buttons .button-cta::before {
            background-position: center;
            left: -1px;
            right: auto;
        }

        #cdate-top .cheader-top {
            padding: 1.5em 0;
            width: 100%;
        }

        #cdate-top .cheader-top p {
            padding: 0 2em;
        }

        ul.switch-lang {
            padding-left: 0;
        }

        .switch-lang li + li {
            margin-left: 1.5em;
        }

        #ctitle-top {
            padding: 2em 0;
        }

        #ctitle-top h1 {
            font-size: 2em;
            letter-spacing: 0.25em;
        }

        #ctitle-top h1 span {
            font-size: 0.625em;
        }

        #cintro,
        #cpoint {
            padding: 0 1.5em;
        }

        #cintro {
            margin-top: 2em;
        }

        #cpoint .c-titre {
            flex-direction: column;
            align-items: start;
        }

        #cpoint .c-titre h2 {
            display: inline-block;
            margin-bottom: 2em;
        }

        #cinformations .c-images {
            margin-bottom: 0;
        }

        #cinformations .cinformations-item-image {
            padding-bottom: 2em;
        }

        #cinformations .cinformations-item-image + .cinformations-item-image {
            border-top: 2px solid #CCCCE9;
        }

        #cprotection .c-gestes li + li {
            margin-left: 0;
            margin-top: 2em;
        }
        #cprotection .c-texte img {
            width: 100%;
        }

        #cmesures.cinfo-block .items-qr {
            margin-top: 1em;
        }

        #cmesures .item-reponse,
        #cmesures .item-autre {
            width: auto;
        }
        .red{
            color:#E1000F;
        }


    }

    </style>


<div id="block-block-15" class="block block-block block-odd">

    
  <div class="block-content">
   
  </div>
</div>

  <script async="" type="text/javascript" src="/_Incapsula_Resource?SWJIYLWA=719d34d31c8e3a6e6fffd425f7e032f3&amp;ns=1&amp;cb=2037496936"></script>
</div></body>