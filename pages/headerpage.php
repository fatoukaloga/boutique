<?php
include '../../db_local.php';
include  '../communes/communefonction.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>mesDenrées</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="mesDenrée s'est votre site de repère en question alimentaire">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="../../styles/bootstrap-4.1.3/bootstrap.css">
<link href="../../plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="../../plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="../../plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="../../plugins/OwlCarousel2-2.2.1/animate.css">
<link rel="stylesheet" type="text/css" href="../../styles/main_styles.css">
<link rel="stylesheet" type="text/css" href="../../styles/responsive.css">
<link rel="icon" 
      type="image/png" 
      href="../../images/mesDenreesImg/logo_1_orange.png">
</head>
<body>

<div class="super_container">

	<!-- Header -->

	<header class="header">
		<div class="header_content d-flex flex-row align-items-center justify-content-start">
			
			<!-- Hamburger -->
			<div class="hamburger menu_mm"><i class="fa fa-bars menu_mm" aria-hidden="true"></i></div>

			<!-- Logo -->
			<div class="header_logo">
                            <a href="../../index.php"><div>mes<span>denrées</span></div></a>
			</div>

			<!-- Navigation en inspectant -->
			<nav class="header_nav">
				<ul class="d-flex flex-row align-items-center justify-content-start">
                                    <li><a href="../../index.php">Accueil</a></li>
                                    <li><a href="../covid/infocovidindex.php">Info COVID19</a></li>
					
					<li>
                                
                                        <div class="info_languages has_children">
					<div class="dropdown_text">Localites</div>
					<div class="dropdown_arrow"><i class="fa fa-angle-down" aria-hidden="true"></i></div>
					<!-- Language Dropdown Menu -->
                                            <?php if ($localites) {
                                            ?>
                                            <ul>
                                            <?php foreach ($localites as $localite): ?>
                                                    <li><a href="../../pages/region/regionindex.php?id=<?= $localite['LO_ID'] ?>">
                                                            <div class="dropdown_text"><?= $localite['LO_LIBELLE'] ?></div>
                                                        </a></li>
                                            <?php endforeach; ?>
                                            </ul>
                                            <?php } ?>
                                        </div>
                                        </li>
                                        <li><a href="../contact/contactindex.php">Nous Contacter</a></li>
                                </ul>

                        </nav>

			<!-- Header Extra -->
			<div class="header_extra ml-auto d-flex flex-row align-items-center justify-content-start">
  			</div>

		</div>
	</header>
<!-- fin header-->
	<!-- Menu  en responsive-->

	<div class="menu d-flex flex-column align-items-start justify-content-start menu_mm trans_400">
		<div class="menu_close_container"><div class="menu_close"><div></div><div></div></div></div>
		<div class="menu_top d-flex flex-row align-items-center justify-content-start">
	
		</div>
                <!-- search -->
<!--		1<div class="menu_search">
                    <form action="../recherche/rechercheindex.php" method="post" name="formproduit" class="header_search_form menu_mm">
				<input type="search" name="motcle" class="search_input menu_mm" placeholder="Rercherche Produit" required="required">
				<button name="btsubmit" type="submit" value="recherche" class="header_search_button d-flex flex-column align-items-center justify-content-center menu_mm">
					<i class="fa fa-search menu_mm" aria-hidden="true"></i>
				</button>
			</form>
                    
		</div>-->
		<nav class="sidebar_nav">
			<ul class="menu_nav">
                            <li class="dropdown_text"><a href="../../index.php">Accueil</a></li>
				<!--<li class="menu_mm"><a href="#">Localites</a></li>-->
                                <li><a href="../covid/infocovidindex.php">Info COVID-19</a></li>
                        </ul>
                                <div class="info_languages has_children">
					<div class="dropdown_text">Localites</div>
					<div class="dropdown_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
					
					<!-- Language Dropdown Menu -->
					 <?php if($localites){
                                         ?>
                                        <ul>
                                             <?php foreach ($localites as $localite):?>
                                            <li><a href="../../pages/region/regionindex.php?id=<?=$localite['LO_ID']?>">
							<div class="dropdown_text"><?=$localite['LO_LIBELLE']?></div>
					 	</a></li>
                                                 <?php endforeach; ?>

					</ul>
                                        <?php  } ?>

				</div>            
                                <ul class="menu_nav">
                                    <li class="menu_mm"><a href="../contact/contactindex.php">nous contacter</a></li>
			</ul>
		</nav>

	</div>
	
	
        <!-- Sidebar -->
	  <div class="sidebar">
		<div class="">
                    <a href="https://www.orangemali.com" target="true"><div class=""><p class="visite">Visiter OrangeMali en cliquant sur </p> <img style="margin-left: 40%;margin-top: 5%;" src="../../images/mesDenreesImg/logo_1_orange.png" alt="https://www.orangemali.com"></div></a>
                </div>
		
		<!-- Info -->
		<!-- Logo -->
		<div class="sidebar_logo">
                    <a href="../../index.php"><div>mes<span>denrées</span></div></a>
		</div>

		<!-- Sidebar Navigation le menu a gauche-->
		<nav class="sidebar_nav">
                           
                    <ul class="menu_nav">
                        
                        <li><a href="../../index.php"><i class='fa fa-home' style='font-size:24px'></i>Accueil<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                        <li><a href="../covid/infocovidindex.php"><i class='fa fa-ambulance' style='font-size:24px;margin-right: 3px;color:red;'></i>Info COVID-19<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>

                    </ul>
                                <div class="info_languages has_children">
					<div class="dropdown_text_2"><i class='fa fa-map-marker' style='font-size:24px;margin-left: 10px;'></i>Localites</div>
					<div class="dropdown_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
					
					<!-- Language Dropdown Menu -->
					 <?php if($localites){
                                                                
                                            
                                            ?>
                                        <ul>
                                             <?php foreach ($localites as $localite):?>
                                            <li><a href="../../pages/region/regionindex.php?id=<?=$localite['LO_ID']?>">
							<div class="dropdown_text"><?=$localite['LO_LIBELLE']?></div>
					 	</a></li>
                                                 <?php endforeach; ?>
					 </ul>
<?php  } ?>
				</div>            
                                <ul class="menu_nav">
<!--				<li><a href="#">lookbook<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
				<li><a href="blog.html">blog<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>-->
                                    <li><a href="../contact/contactindex.php"><i class='fa fa-book' style='font-size:24px;'></i>nous contacter<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
			</ul>
                       
		</nav>

		<!--2 Search -->
		<div class="search">
<!--                    <form action="../recherche/rechercheindex.php" method="post" name="formproduit" class="search_form" id="sidebar_search_form">
				<input id="motcle" type="search" name="motcle" class="search_input" placeholder="Rercherche Produit" required="required">
				<button name="btsubmit" type="submit" value="recherche" class="search_button"><i class="fa fa-search" aria-hidden="true"></i></button>
			</form>-->

<!--    <img src="../../images/mesDenreesImg/indexfooter2.jpg" style="margin-left: -18px;
margin-top: -16px;" alt="cereales">-->
    <!--<div class="background_image" style="background-image:url(../../images/mesDenreesImg/cereal_grain2.jpg)"></div>-->
              <marquee>

<img src="../../images/mesDenreesImg/indexfooter2.jpg">
<img src="../../images/mesDenreesImg/indexfooter5.jpg">
<img src="../../images/mesDenreesImg/indexfooter3.jpg">
<img src="../../images/mesDenreesImg/indexfooter4.jpg">
<img src="../../images/mesDenreesImg/indexfooter6.jpg">
</marquee>  
		</div>
  
		
	</div>
<!--        <marquee id="id1"direction="up" scrollamount="2" height="100" width="150">
                  <span onmouseover="getElementById('id1').stop();" onmouseout="getElementById('id1').start();">
                     tete
              </marquee> -->

<!-- fin menu extra -->
	
<!-- Home -->
	<div class="home">
	<!-- Home Slider -->
		<div class="home_slider_container" data-c>
			<div class="owl-carousel owl-theme home_slider">
				
				<!-- Slide -->
				<div class="owl-item">
					<div class="background_image" style="background-image:url(../../images/mesDenreesImg/CEREALEslide.jpg)"></div>
			                <div class="home_content_container">
						<div class="home_content">

							<div class="home_title">CEREALES</div>
							<div class="button button_1 home_button trans_200"><a href="#">De bonne qualite!</a></div>
						</div>
					</div>
				</div>

				<!-- Slide -->
				<div class="owl-item">
					<div class="background_image" style="background-image:url(../../images/mesDenreesImg/cereal_grain2.jpg)"></div>
					<div class="home_content_container">

					</div>
				</div>

				<!-- Slide -->
				<div class="owl-item">
					<div class="background_image" style="background-image:url(../../images/home_slider_3.jpg)"></div>
					<div class="home_content_container">
						<div class="home_content">

						</div>
					</div>
				</div>
                                <!-- Slide -->
				<div class="owl-item">
					<div class="background_image" style="background-image:url(../../images/AIL.jpg)"></div>
					<div class="home_content_container">
						<div class="home_content">

						</div>
					</div>
				</div>
                                <!-- Slide -->
				<div class="owl-item">
					<div class="background_image" style="background-image:url(../../images/home_slider_4.jpg)"></div>
					<div class="home_content_container">
						<div class="home_content">							
						</div>
					</div>
				</div>

			</div>
				
			<!-- Home Slider Navigation -->
			<div class="home_slider_nav home_slider_prev trans_200">
                            <div class=" d-flex flex-column align-items-center justify-content-center">
                                <img src="../../images/prev.png" alt="">
                            </div>
                        </div>
                        <div class="home_slider_nav home_slider_next trans_200"><div class=" d-flex flex-column align-items-center justify-content-center"><img src="../../images/next.png" alt=""></div></div>

		</div>
	</div>