<?php
/* LA LISTE DE TOUS LES PRODUITS DANS UN TABLEAUX AVEC 
 * PRIX EN GROS ET DETAIL?STOCK?LISTE DES LIVREURS,VENDEUR
 */
//include '../../db.php';
// Connect to MySQL database
$pdo = pdo_connect_mysql();
$produitssecarch;
if(isset($_GET['btsubmit'])  AND $_GET["btsubmit"] == "recherche"){
    $_GET["motcle"] = htmlspecialchars($_GET["motcle"]);
    //pour sécuriser le formulaire contre les failles html
 //pour supprimer les espaces dans la requête de l'internaute
 $mc = strip_tags(trim($_GET['motcle'])); //pour supprimer les balises html dans la requête
 // Get the page via GET request (URL param: page), if non exists default the page to 1
$page = isset($_GET['page']) && is_numeric($_GET['page']) ? (int)$_GET['page'] : 1;
// Number of records to show on each page
$records_per_page = 5;
// Prepare the SQL statement and get records from our contacts table, LIMIT will determine the page


$stmt = $pdo->prepare('SELECT * FROM boutique_produits bp,produits pr,boutique bo, localite lo,boutique_type bt WHERE bo.BO_ID=bp.BO_ID and bo.BO_TYPE=bt.BT_ID AND bo.LO_ID=lo.LO_ID and bp.PR_ID=pr.PR_ID AND pr.PR_ETAT=\'Y\' AND pr.PR_LIBELLE LIKE :mc ORDER BY bp.BP_ID');
//$stmt->bindValue(':motcle', $mc);
//$stmt->bindValue(':current_page', ($page-1)*$records_per_page, PDO::PARAM_INT);
//$stmt->bindValue(':record_per_page', $records_per_page, PDO::PARAM_INT);

$stmt->execute(array(':mc'=>'%'.$mc.'%'));
$produits = $stmt->fetchAll(PDO::FETCH_ASSOC);
  $num_produits=0;
 if($produits){
// Get the total number of contacts, this is so we can determine whether there should be a next and previous button
$num_produits = count($produits);
//echo $num_produits;
 } 

}else {
 $stmt = $pdo->prepare('SELECT * FROM boutique_produits bp,produits pr,boutique bo, localite lo WHERE bo.BO_ID=bp.BO_ID AND bo.LO_ID=lo.LO_ID and bp.PR_ID=pr.PR_ID AND'
             . ' pr.PR_ETAT="Y" ORDER BY bp.BP_ID');

//$stmt->bindValue(':current_page', ($page-1)*$records_per_page, PDO::PARAM_INT);
//$stmt->bindValue(':record_per_page', $records_per_page, PDO::PARAM_INT); 
 $stmt->execute();
$produits = $stmt->fetchAll(PDO::FETCH_ASSOC);
  $num_produits=0;
 if($produits){
// Get the total number of contacts, this is so we can determine whether there should be a next and previous button
$num_produits = count($produits);
//echo $num_produits;
 }
}

?>	

<div class="products">
		<div class="section_container">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="products_container grid">
							<?php foreach ($produits as $produitsear): $produitsear['PR_PHOTO'];?>
							<div class="product grid-item hot">
								<div class="product_inner">
                                                                    <figure class="snip1487">
                                                         <div class="img" style="background-image:url('../../images/mesDenreesImg/<?=$produitsear['PR_PHOTO']?>');">
                                                          <a href="../../pages/boutique_marche/descriptionproduitindex.php?id=<?=$produitsear['BP_ID']?>?idpr=<?=$produitsear['PR_ID']?>">  </a></div>                                                                      <!--<img src="images/mesDenreesImg/RIZ.jpg" alt="sample104" />-->
                                                                <figcaption style="color: rgb(255,220,0);background-color: rgb(143, 143, 143);">
                                                                            <h3><strong><?=$produitsear['PR_LIBELLE']?></strong> <span><?=$produitsear['PR_PRIX']?>fcfa/ <?=$produitsear['PR_UNITE']?></span>
                                                                                <br><?=$produitsear['BO_LIBELLE']?></h3>
                                                                            
                                                                        </figcaption>
                                                                


                                                            </figure>
                                        <h4 style="color: rgb(0,0,0);"><?=$produitsear['PR_LIBELLE']?></h4>  
                                                                    <a style="color: rgb(255, 102, 0);font-style: unset;font-family: cursive;text-transform: uppercase;" 
                                                                    href="../../pages/boutique_marche/descriptionproduitindex.php?id=<?=$produitsear['BP_ID']?>?idpr=<?=$produitsear['PR_ID']?>"><?=$produitsear['PR_PRIX']?>fcfa/ 
                                                                <?=$produitsear['PR_UNITE']?><br><?=$produitsear['LO_LIBELLE']?></a>
                                                 
                                                                    
<!--									<div class="img" style="background-image:url('../../images/mesDenreesImg/<?= $produitsear['PR_PHOTO']?>');"></div>-->
<!--                                                                            <img src="images/mesDenreesImg/RIZ.jpg" alt="">
										<div class="product_tag">hot</div>
									</div>-->
<!--									<div class="product_content_2 text-center">
										<div class="product_title"><a href="#"><?=$produitsear['PR_LIBELLE']?></a></div>
										<div class="product_price"><?=$produitsear['PR_PRIX']?>fcfa/<?=$produitsear['PR_UNITE']?></div>
                                                                                <div class="product_price"><?=$produitsear['BO_LIBELLE']?></div>
                                                                                <div class="product_price"><?=$produitsear['BT_LIBELLE']?></div>
                                                                                <div class="product_price"><?=$produitsear['LO_LIBELLE']?></div>
										<div class="product_button ml-auto mr-auto trans_200"><a href="#">add to cart</a></div>
									</div>-->
								</div>	
							</div>
                                                     <?php endforeach; ?>
                                                                                 

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
