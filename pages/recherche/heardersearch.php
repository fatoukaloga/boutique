<?php
include '../../db_local.php';
include  '../communes/communefonction.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>mesDenrées</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="mesDenrée sest votre site de repère en question alimentaire">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="../../styles/bootstrap-4.1.3/bootstrap.css">
<link href="../../plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="../../plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="../../plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="../../plugins/OwlCarousel2-2.2.1/animate.css">
<link rel="stylesheet" type="text/css" href="../../styles/main_styles.css">
<link rel="stylesheet" type="text/css" href="../../styles/responsive.css">
</head>
<body>

<div class="super_container">

	<!-- Header -->

	<header class="header">
		<div class="header_content d-flex flex-row align-items-center justify-content-start">
			
			<!-- Hamburger -->
			<div class="hamburger menu_mm"><i class="fa fa-bars menu_mm" aria-hidden="true"></i></div>

			<!-- Logo -->
			<div class="header_logo">
				<a href="#"><div>mes<span>denrées</span></div></a>
			</div>

			<!-- Navigation en inspectant -->
			<nav class="header_nav">
				<ul class="d-flex flex-row align-items-center justify-content-start">
                                    <li><a href="../../index.php">Accueil</a></li>
                                    <li><a href="../covid/infocovidindex.php">Info COVID19</a></li>
					
					<li>
                                
                                        <div class="info_languages has_children">
					<div class="dropdown_text">Localites</div>
					<div class="dropdown_arrow"><i class="fa fa-angle-down" aria-hidden="true"></i></div>
					      
					<!-- Language Dropdown Menu -->
					<?php if($localites){
                                                                
                                            
                                            ?>
                                        <ul>
                                             <?php foreach ($localites as $localite):?>
                                            <li><a href="../communes/communeindex.php?id=<?=$localite['LO_ID']?>">
							<div class="dropdown_text"><?=$localite['LO_LIBELLE']?></div>
					 	</a></li>
                                                 <?php endforeach; ?>
					 </ul>
<?php  } ?>
                                        </div>
                                        </li>
                                        <li><a href="../contact/contactindex.php">nous Contacter</a></li>
                                </ul>
                           
			</nav>

			<!-- Header Extra -->
			<div class="header_extra ml-auto d-flex flex-row align-items-center justify-content-start">
  			</div>

		</div>
	</header>
<!-- fin header-->
	<!-- Menu  en responsive-->

	<div class="menu d-flex flex-column align-items-start justify-content-start menu_mm trans_400">
		<div class="menu_close_container"><div class="menu_close"><div></div><div></div></div></div>
		<div class="menu_top d-flex flex-row align-items-center justify-content-start">
	
		</div>
                <!-- search -->
		<div class="menu_search">
                    <form action="rechercheindex.php" method="get" name="formproduit" class="header_search_form menu_mm">
                        <input id="motcle" type="search" name="motcle" class="search_input menu_mm" placeholder="Rercherche Produit" required="required">
                        <button name="btsubmit" type="submit" value="recherche" class="header_search_button d-flex flex-column align-items-center justify-content-center menu_mm">
					<i class="fa fa-search menu_mm" aria-hidden="true"></i>
				</button>
			</form>
		</div>
		<nav class="sidebar_nav">
			<ul class="menu_nav">
                            <li class="dropdown_text"><a href="../../index.php">Accueil</a></li>
				<!--<li class="menu_mm"><a href="#">Localites</a></li>-->
                                <li><a href="../../pages/covid/infocovidindex.php">Info COVID-19</a></li>
                       </ul>
                                <div class="info_languages has_children">
					<div class="dropdown_text">Localites</div>
					<div class="dropdown_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
					
					<!-- Language Dropdown Menu -->
					 <?php if($localites){
                                                                
                                            
                                            ?>
                                        <ul>
                                             <?php foreach ($localites as $localite):?>
                                            <li><a href="../../pages/communes/communeindex.php?id=<?=$localite['LO_ID']?>">
							<div class="dropdown_text"><?=$localite['LO_LIBELLE']?></div>
					 	</a></li>
                                                 <?php endforeach; ?>

					 </ul>
                                        <?php  } ?>

				</div>            
                                <ul class="menu_nav">
                                    <li class="menu_mm"><a href="../contact/contactindex.php">nous contacter</a></li>
			</ul>
		</nav>

	</div>
	
	
        <!-- Sidebar -->
	<div class="sidebar">
		
		<!-- Info -->

		<!-- Logo -->
		<div class="sidebar_logo">
			<a href="#"><div>mes<span>denrées</span></div></a>
		</div>

		<!-- Sidebar Navigation le menu a gauche-->
		<nav class="sidebar_nav">
                           
                    <ul class="menu_nav">
                        
                        <li><a href="../../index.php">Acceuil<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                        <li><a href="../../pages/covid/infocovidindex.php">Info COVID-19<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>

                    </ul>
                                <div class="info_languages has_children">
					<div class="dropdown_text_2">Localites</div>
					<div class="dropdown_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
					
					<!-- Language Dropdown Menu -->
					 <?php if($localites){
                                                                
                                            
                                            ?>
                                        <ul>
                                             <?php foreach ($localites as $localite):?>
                                            <li><a href="../../pages/communes/communeindex.php?id=<?=$localite['LO_ID']?>">
							<div class="dropdown_text"><?=$localite['LO_LIBELLE']?></div>
					 	</a></li>
                                                 <?php endforeach; ?>
					 </ul>
<?php  } ?>
				</div>            
                                <ul class="menu_nav">
<!--				<li><a href="#">lookbook<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
				<li><a href="blog.html">blog<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>-->
                                    <li><a href="../contact/contactindex.php">nous contacter<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
			</ul>
                       
		</nav>

		<!-- Search -->
		<div class="search">
                    <form action="rechercheindex.php" method="get" name="formproduit" class="search_form" id="sidebar_search_form">
                        <input type="search" name="motcle" class="search_input" placeholder="Rercherche Produit" required="required">
                        <button name="btsubmit" type="submit"  value="recherche" class="search_button"><i class="fa fa-search" aria-hidden="true"></i></button>
			</form>
                    
		</div>

		
	</div>
<!-- fin menu extra -->
	
<!-- Home -->
	<div class="home">
	<!-- Home Slider -->
		<div class="home_slider_container" data-c>
			<div class="owl-carousel owl-theme home_slider">
				
				<!-- Slide -->
				<div class="owl-item">
					<div class="background_image" style="background-image:url(../../images/mesDenreesImg/CEREALEslide.jpg)"></div>
			                <div class="home_content_container">
						<div class="home_content">
<!--							<div class="home_discount d-flex flex-row align-items-end justify-content-start">
								<div class="home_discount_num">20</div>
								<div class="home_discount_text">Discount on the</div>
							</div>-->
							<div class="home_title">CEREALES</div>
							<div class="button button_1 home_button trans_200"><a href="#">De bonne qualite!</a></div>
						</div>
					</div>
				</div>

				<!-- Slide -->
				<div class="owl-item">
					<div class="background_image" style="background-image:url(../../images/mesDenreesImg/cereal_grain2.jpg)"></div>
					<div class="home_content_container">
<!--						
					</div>
				</div>

				<!-- Slide -->
				<div class="owl-item">
					<div class="background_image" style="background-image:url(../../images/home_slider_3.jpg)"></div>
					<div class="home_content_container">
						<div class="home_content">
							
						</div>
					</div>
				</div>
                                <!-- Slide -->
				<div class="owl-item">
					<div class="background_image" style="background-image:url(../../images/AIL.jpg)"></div>
					<div class="home_content_container">
						<div class="home_content">
<!--							<div class="home_discount d-flex flex-row align-items-end justify-content-start">
								<div class="home_discount_num">20</div>
								<div class="home_discount_text">Discount on the</div>
							</div>
							<div class="home_title">New Collection</div>
							<div class="button button_1 home_button trans_200"><a href="categories.html">Shop NOW!</a></div>-->
						</div>
					</div>
				</div>
                                <!-- Slide -->
				<div class="owl-item">
					<div class="background_image" style="background-image:url(../../images/home_slider_4.jpg)"></div>
					<div class="home_content_container">
						<div class="home_content">
<!--							<div class="home_discount d-flex flex-row align-items-end justify-content-start">
								<div class="home_discount_num">20</div>
								<div class="home_discount_text">Discount on the</div>
							</div>
							<div class="home_title">New Collection</div>
							<div class="button button_1 home_button trans_200"><a href="categories.html">Shop NOW!</a></div>-->
						</div>
					</div>
				</div>

			</div>
				
			<!-- Home Slider Navigation -->
			<div class="home_slider_nav home_slider_prev trans_200">
                            <div class=" d-flex flex-column align-items-center justify-content-center">
                                <img src="../../images/prev.png" alt="">
                            </div>
                        </div>
                        <div class="home_slider_nav home_slider_next trans_200"><div class=" d-flex flex-column align-items-center justify-content-center"><img src="../../images/next.png" alt=""></div></div>

		</div>
	</div>