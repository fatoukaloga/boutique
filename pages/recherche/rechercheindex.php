<?php
include '../../db_local.php';
include  '../communes/communefonction.php';
include  '../region/regionfonction.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>mesDenrées</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="mesDenrée sest votre site de repère en question alimentaire">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="../../styles/bootstrap-4.1.3/bootstrap.css">
<link href="../../plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="../../plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="../../plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="../../plugins/OwlCarousel2-2.2.1/animate.css">
<link rel="stylesheet" type="text/css" href="../../styles/main_styles.css">
<link rel="stylesheet" type="text/css" href="../../styles/responsive.css">
<link rel="icon" type="image/png" href="../../images/mesDenreesImg/logo_1_orange.png">
</head>
<body>

<div class="super_container">

	<!-- Header -->

	<header class="header">
		<div class="header_content d-flex flex-row align-items-center justify-content-start">
			
			<!-- Hamburger -->
			<div class="hamburger menu_mm"><i class="fa fa-bars menu_mm" aria-hidden="true"></i></div>

			<!-- Logo -->
			<div class="header_logo">
				<a href="#"><div>mes<span>denrées</span></div></a>
			</div>

			<!-- Navigation en inspectant -->
			<nav class="header_nav">
				<ul class="d-flex flex-row align-items-center justify-content-start">
                                    <li><a href="../../index.php">Accueil</a></li>
                                    <li><a href="../covid/infocovidindex.php">Info COVID19</a></li>
					
					<li>
                                
                                        <div class="info_languages has_children">
					<div class="dropdown_text">Localites</div>
					<div class="dropdown_arrow"><i class="fa fa-angle-down" aria-hidden="true"></i></div>
					      
					<!-- Language Dropdown Menu -->
					<?php if($localites){
                                                                
                                            
                                            ?>
                                        <ul>
                                             <?php foreach ($localites as $localite):?>
                                            <li><a href="../region/regionindex.php?id=<?=$localite['LO_ID']?>">
							<div class="dropdown_text"><?=$localite['LO_LIBELLE']?></div>
					 	</a></li>
                                                 <?php endforeach; ?>
					 </ul>
<?php  } ?>
                                        </div>
                                        </li>
                                        <li><a href="../contact/contactindex.php">Contact</a></li>
                                </ul>
                           
			</nav>

			<!-- Header Extra -->
			<div class="header_extra ml-auto d-flex flex-row align-items-center justify-content-start">
  			</div>

		</div>
	</header>
<!-- fin header-->
	<!-- Menu  en responsive-->

	<div class="menu d-flex flex-column align-items-start justify-content-start menu_mm trans_400">
		<div class="menu_close_container"><div class="menu_close"><div></div><div></div></div></div>
		<div class="menu_top d-flex flex-row align-items-center justify-content-start">
	
		</div>
                <!-- search -->
		<div class="menu_search">
                    <form action="rechercheindex.php" method="get" name="formproduit" class="header_search_form menu_mm">
                        <input id="motcle" type="search" name="motcle" class="search_input menu_mm" placeholder="Rercherche Produit" required="required">
                        <button name="btsubmit" type="submit" value="recherche" class="header_search_button d-flex flex-column align-items-center justify-content-center menu_mm">
					<i class="fa fa-search menu_mm" aria-hidden="true"></i>
				</button>
			</form>
		</div>
		<nav class="sidebar_nav">
			<ul class="menu_nav">
                            <li class="dropdown_text"><a href="../../index.php">Accueil</a></li>
				<!--<li class="menu_mm"><a href="#">Localites</a></li>-->
                                <li><a href="../../pages/covid/infocovidindex.php">Info COVID-19</a></li>
                       </ul>
                                <div class="info_languages has_children">
					<div class="dropdown_text">Localites</div>
					<div class="dropdown_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
					
					<!-- Language Dropdown Menu -->
					 <?php if($localites){
                                                                
                                            
                                            ?>
                                        <ul>
                                             <?php foreach ($localites as $localite):?>
                                            <li><a href="../../pages/region/regionindex.php?id=<?=$localite['LO_ID']?>">
							<div class="dropdown_text"><?=$localite['LO_LIBELLE']?></div>
					 	</a></li>
                                                 <?php endforeach; ?>

					 </ul>
                                        <?php  } ?>

				</div>            
                                <ul class="menu_nav">
                                    <li class="menu_mm"><a href="../contact/contactindex.php">contact</a></li>
			</ul>
		</nav>

	</div>
	
	
        <!-- Sidebar -->
	 <div class="sidebar">
		<div class="">
                    <a href="https://www.orangemali.com" target="true"><div class=""><p class="visite">Visiter OrangeMali en cliquant sur</p> <img style="margin-left: 40%;margin-top: 5%;" src="../../images/mesDenreesImg/logo_1_orange.png" alt="https://www.orangemali.com"></div></a>
                </div>
		<!-- Info -->

		<!-- Logo -->
		<div class="sidebar_logo">
			<a href="#"><div>mes<span>denrées</span></div></a>
		</div>

		<!-- Sidebar Navigation le menu a gauche-->
		<nav class="sidebar_nav">
                           
                    <ul class="menu_nav">
                        
                        <li><a href="../../index.php"><i class='fa fa-home' style='font-size:24px'></i>Acceuil<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                        <li><a href="../../pages/covid/infocovidindex.php"><i class='fa fa-ambulance' style='font-size:24px;margin-right: 3px;color:red;'></i>Info COVID-19<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>

                    </ul>
                                <div class="info_languages has_children">
					<div class="dropdown_text_2"><i class='fa fa-shopping-bag' style='font-size:24px;margin-left: 10px;'></i>Localites</div>
					<div class="dropdown_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
					
					<!-- Language Dropdown Menu -->
					 <?php if($localites){
                                                                
                                            
                                            ?>
                                        <ul>
                                             <?php foreach ($localites as $localite):?>
                                            <li><a href="../../pages/region/regionindex.php?id=<?=$localite['LO_ID']?>">
							<div class="dropdown_text"><?=$localite['LO_LIBELLE']?></div>
					 	</a></li>
                                                 <?php endforeach; ?>
					 </ul>
<?php  } ?>
				</div>            
                                <ul class="menu_nav">
<!--				<li><a href="#">lookbook<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
				<li><a href="blog.html">blog<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>-->
                                    <li><a href="../contact/contactindex.php"><i class='fa fa-book' style='font-size:24px;'></i>contact<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
			</ul>
                       
		</nav>

		<!-- Search -->
		<div class="search">
                    <form action="rechercheindex.php" method="get" name="formproduit" class="search_form" id="sidebar_search_form">
                        <input type="search" name="motcle" class="search_input" placeholder="Rercherche Produit" required="required">
                        <button name="btsubmit" type="submit"  value="recherche" class="search_button"><i class="fa fa-search" aria-hidden="true"></i></button>
			</form>
                    
		</div>

		
	</div>
<!-- fin menu extra -->
	
<!-- Home -->
	<div class="home">
	<!-- Home Slider -->
		<div class="home_slider_container" data-c>
			<div class="owl-carousel owl-theme home_slider">
				
				<!-- Slide -->
				<div class="owl-item">
					<div class="background_image" style="background-image:url(../../images/mesDenreesImg/CEREALEslide.jpg)"></div>
			                <div class="home_content_container">
						<div class="home_content">
<!--							<div class="home_discount d-flex flex-row align-items-end justify-content-start">
								<div class="home_discount_num">20</div>
								<div class="home_discount_text">Discount on the</div>
							</div>-->
							<div class="home_title">CEREALES</div>
							<div class="button button_1 home_button trans_200"><a href="#">De bonne qualite!</a></div>
						</div>
					</div>
				</div>

				<!-- Slide -->
				<div class="owl-item">
					<div class="background_image" style="background-image:url(../../images/mesDenreesImg/cereal_grain2.jpg)"></div>
					<div class="home_content_container">
<!--						<div class="home_content">
							<div class="home_discount d-flex flex-row align-items-end justify-content-start">
								<div class="home_discount_num">20</div>
								<div class="home_discount_text">Discount on the</div>
							</div>
							<div class="home_title">New Collection</div>
							<div class="button button_1 home_button trans_200"><a href="categories.html">Shop NOW!</a></div>
						</div>-->
					</div>
				</div>

				<!-- Slide -->
				<div class="owl-item">
					<div class="background_image" style="background-image:url(../../images/home_slider_3.jpg)"></div>
					<div class="home_content_container">
						<div class="home_content">
<!--							<div class="home_discount d-flex flex-row align-items-end justify-content-start">
								<div class="home_discount_num">20</div>
								<div class="home_discount_text">Discount on the</div>
							</div>
							<div class="home_title">New Collection</div>
							<div class="button button_1 home_button trans_200"><a href="categories.html">Shop NOW!</a></div>-->
						</div>
					</div>
				</div>
                                <!-- Slide -->
				<div class="owl-item">
					<div class="background_image" style="background-image:url(../../images/AIL.jpg)"></div>
					<div class="home_content_container">
						<div class="home_content">
<!--							<div class="home_discount d-flex flex-row align-items-end justify-content-start">
								<div class="home_discount_num">20</div>
								<div class="home_discount_text">Discount on the</div>
							</div>
							<div class="home_title">New Collection</div>
							<div class="button button_1 home_button trans_200"><a href="categories.html">Shop NOW!</a></div>-->
						</div>
					</div>
				</div>
                                <!-- Slide -->
				<div class="owl-item">
					<div class="background_image" style="background-image:url(../../images/home_slider_4.jpg)"></div>
					<div class="home_content_container">
						<div class="home_content">
<!--							<div class="home_discount d-flex flex-row align-items-end justify-content-start">
								<div class="home_discount_num">20</div>
								<div class="home_discount_text">Discount on the</div>
							</div>
							<div class="home_title">New Collection</div>
							<div class="button button_1 home_button trans_200"><a href="categories.html">Shop NOW!</a></div>-->
						</div>
					</div>
				</div>

			</div>
				
			<!-- Home Slider Navigation -->
			<div class="home_slider_nav home_slider_prev trans_200">
                            <div class=" d-flex flex-column align-items-center justify-content-center">
                                <img src="../../images/prev.png" alt="">
                            </div>
                        </div>
                        <div class="home_slider_nav home_slider_next trans_200"><div class=" d-flex flex-column align-items-center justify-content-center"><img src="../../images/next.png" alt=""></div></div>

		</div>
	</div>
<!-- fin home -->
	<!-- Boxes -->
	

	<!-- Categories -->

	<!-- Products AFFICHER-->
<?php
include '../recherche/recherche.php';
?>
	<!-- Newsletter -->

	<!-- Footer -->

	<footer class="footer">
		<div class="footer_content">
			<div class="section_container">
				<div class="container">
					<div class="row">
						
						<!-- About -->
						<div class="col-xxl-3 col-md-6 footer_col">
							<div class="footer_about">
								<!-- Logo -->
								<div class="footer_logo">
									<a href="#"><div>mes<span>Denrées</span></div></a>
								</div>
								<div class="footer_about_text">
									<p>Est un site de repère,où les aliments de première necessite sont recherchés et mis à votre disposition.
                                                                            En fonction des prix et leurs localites.</p>
								</div>

							</div>
						</div>

						

						<!-- Contact -->
						<div class="col-xxl-3 col-md-6 footer_col">
							<div class="footer_contact">
								<div class="footer_title">contact</div>
								<div class="footer_contact_list">
									<ul>
										<li class="d-flex flex-row align-items-start justify-content-start"><span>S.</span><div>Orange Mali</div></li>
										<li class="d-flex flex-row align-items-start justify-content-start"><span>A.</span><div>Hamdalaye ACI2000</div></li>
										<li class="d-flex flex-row align-items-start justify-content-start"><span>T.</span><div>+223 0000 0000</div></li>
										<li class="d-flex flex-row align-items-start justify-content-start"><span>E.</span><div>ODC@orangemali.com</div></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Social -->
		<div class="footer_social">
			<div class="section_container">
				<div class="container">
					<div class="row">
						<div class="col">								
						</div>
					</div>
				</div>
			</div>				
		</div>

		<!-- Credits -->
		<div class="credits">
			<div class="section_container">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="credits_content d-flex flex-row align-items-center justify-content-end">
								<div class="credits_text"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> 
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>

</div>

    <script src="../../js/jquery-3.2.1.min.js"></script>
<script src="../../styles/bootstrap-4.1.3/popper.js"></script>
<script src="../../styles/bootstrap-4.1.3/bootstrap.min.js"></script>
<script src="../../plugins/greensock/TweenMax.min.js"></script>
<script src="../../plugins/greensock/TimelineMax.min.js"></script>
<script src="../../plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="../../plugins/greensock/animation.gsap.min.js"></script>
<script src="../../plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="../../plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="../../plugins/easing/easing.js"></script>
<script src="../../plugins/parallax-js-master/parallax.min.js"></script>
<script src="../../plugins/Isotope/isotope.pkgd.min.js"></script>
<script src="../../plugins/Isotope/fitcolumns.js"></script>
<script src="../../js/custom.js"></script>
</body>
</html>



