<div class="menu d-flex flex-column align-items-start justify-content-start menu_mm trans_400">
		<div class="menu_close_container"><div class="menu_close"><div></div><div></div></div></div>
		<div class="menu_top d-flex flex-row align-items-center justify-content-start">
	
		</div>
                <!-- search -->
		<div class="menu_search">
			<form action="#" class="header_search_form menu_mm">
				<input type="search" class="search_input menu_mm" placeholder="Search" required="required">
				<button class="header_search_button d-flex flex-column align-items-center justify-content-center menu_mm">
					<i class="fa fa-search menu_mm" aria-hidden="true"></i>
				</button>
			</form>
		</div>
		<nav class="menu_nav">
			<ul class="menu_mm">
                            <li class="menu_mm"><a href="../index.php">Accueil</a></li>
				<li class="menu_mm"><a href="#">Localites</a></li>
                                <li class="menu_mm"><a href="../pages/infoconvid.php">Info COVID19</a></li>
                              
                                <!--
				<li class="menu_mm"><a href="#">lookbook</a></li>
				<li class="menu_mm"><a href="blog.html">blog</a></li>-->
				<li class="menu_mm"><a href="contact.php">contact</a></li>
			</ul>
		</nav>

	</div>
	
	
        <!-- Sidebar -->
	<div class="sidebar">
		
		<!-- Info -->
		<div class="info">
			<div class="info_content d-flex flex-row align-items-center justify-content-start">
				
				<!-- Language -->
				<div class="info_languages has_children">
					<div class="language_flag"><img src="images/flag_1.svg" alt="https://www.flaticon.com/authors/freepik"></div>
					<div class="dropdown_text">english</div>
					<div class="dropdown_arrow"><i class="fa fa-angle-down" aria-hidden="true"></i></div>
					
					<!-- Language Dropdown Menu -->
					 <ul>
					 	<li><a href="#">
				 			<div class="language_flag"><img src="images/flag_2.svg" alt="https://www.flaticon.com/authors/freepik"></div>
							<div class="dropdown_text">french</div>
					 	</a></li>
					 	<li><a href="#">
				 			<div class="language_flag"><img src="images/flag_3.svg" alt="https://www.flaticon.com/authors/freepik"></div>
							<div class="dropdown_text">japanese</div>
					 	</a></li>
					 	<li><a href="#">
				 			<div class="language_flag"><img src="images/flag_4.svg" alt="https://www.flaticon.com/authors/freepik"></div>
							<div class="dropdown_text">russian</div>
					 	</a></li>
					 	<li><a href="#">
				 			<div class="language_flag"><img src="images/flag_5.svg" alt="https://www.flaticon.com/authors/freepik"></div>
							<div class="dropdown_text">spanish</div>
					 	</a></li>
					 </ul>

				</div>

				<!-- Currency -->
				<div class="info_currencies has_children">
					<div class="dropdown_text">usd</div>
					<div class="dropdown_arrow"><i class="fa fa-angle-down" aria-hidden="true"></i></div>

					<!-- Currencies Dropdown Menu -->
					 <ul>
					 	<li><a href="#"><div class="dropdown_text">EUR</div></a></li>
					 	<li><a href="#"><div class="dropdown_text">YEN</div></a></li>
					 	<li><a href="#"><div class="dropdown_text">GBP</div></a></li>
					 	<li><a href="#"><div class="dropdown_text">CAD</div></a></li>
					 </ul>

				</div>

			</div>
		</div>

		<!-- Logo -->
		<div class="sidebar_logo">
			<a href="#"><div>mes<span>denrées</span></div></a>
		</div>

		<!-- Sidebar Navigation le menu a gauche-->
		<nav class="sidebar_nav">
                                 
                    <ul class="menu_nav">
                        
                        <li><a href="../index.php">Acceuil<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                        <li><a href="../index.php">Localites<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                    </ul>
                                <div class="info_languages has_children">
					<div class="dropdown_text">Localites</div>
					<div class="dropdown_arrow"><i class="fa fa-angle-down" aria-hidden="true"></i></div>
					
					<!-- Language Dropdown Menu -->
					 <ul>
					 	<li><a href="#">
							<div class="dropdown_text">Commune I</div>
					 	</a></li>
					 	<li><a href="#">
							<div class="dropdown_text">Commune I</div>
					 	</a></li>
					 	<li><a href="#">
							<div class="dropdown_text">Commune I</div>
					 	</a></li>
					 	<li><a href="#">
							<div class="dropdown_text">Commune I</div>
					 	</a></li>
					 </ul>

				</div>            
                                <ul class="menu_nav">
                                    <li><a href="../pages/infoconvid.php">Info COVID19<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
<!--				<li><a href="#">lookbook<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
				<li><a href="blog.html">blog<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>-->
                                    <li><a href="../index.php">contact<i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
			</ul>
                       
		</nav>

		<!-- Search -->
		<div class="search">
			<form action="#" class="search_form" id="sidebar_search_form">
				<input type="text" class="search_input" placeholder="Search" required="required">
				<button class="search_button"><i class="fa fa-search" aria-hidden="true"></i></button>
			</form>
		</div>

		
	</div>